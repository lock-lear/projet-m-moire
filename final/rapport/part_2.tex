\part{EM à noyau}

\section{Introduction au problème}

\paragraph{}





Dans cette seconde partie nous allons chercher à étendre la classification des mélanges à noyau, développé par Nicolas Wicker and Alejandro Murua \cite{Wicker2014}. Dans cet articles, les auteurs introduisent des noyaux afin de résoudre deux problématiques, liées à la manipulation des données :
\begin{itemize}
	\item les points ne sont pas décrits par des coordonnées (par exemple par une distance inter-point).
	
	\item La dimension est très grande (temps de calculs) et/ou plus grand que le nombre de données disponible (classification d'image par exemple).
\end{itemize}

\paragraph{}

Considérons donc $N$ points $X=(x_i)$ de $\mathbb{X}$. L'astuce du noyau consiste à envoyer $X$ dans un espace de Hilbert, noté $\mathcal{H}_k$,  de plus grande dimension (éventuellement infinie), où les données deviennent linéaire séparable (i.e séparable par un hyperplan). On sait de plus, grâce à cette astuce introduite par Aizerman \cite{Aizerman1964}, que la fonction nécessaire au changement d'espace peut être complètement caractérisée par un noyau défini positif $k(., .)$ de $\mathbb{X} \times \mathbb{X}$ où ce dernier agit comme un produit scalaire :
\begin{equation} \label{eqKernelTrick}
	<\Phi(x), \Phi(x')>_{\mathcal{H}_k} = k(x, x')
\end{equation}

On peut de cette façon travailler avec des distances dans $\mathcal{H}_k$, plutôt qu'une distance dans $\mathbb{X}$ éventuellement non définie. On considère alors le vecteur :
\begin{center}
$Y = {\Vert \Phi(X) - \mu \Vert }^2$
\end{center}
où $\mu$ représente le centre de la classe considérée. 

\paragraph{}
Nous nous concentrerons de plus dans ce mémoire sur le cas d'un noyau gaussien :
\begin{equation}
k(x, y) = \exp \left( \frac{-d(x, y)}{\sigma} \right)
\end{equation}
où 
\begin{itemize}
	\item $x$ et $y$ sont deux points de $\mathbb{X}$.
	
	\item $d$ est une distance sur $\mathbb{X}$.
	
	\item $\sigma$ est la bande passante du filtre. Si l'on veut la voir comme un paramètre de normalisation, Silverman(1986) \cite{Silverman1996} a proposé plusieurs moyens de la faire dépendre du jeu de données. Toutefois $sigma$ \textbf{sera supposé constant ici}.
\end{itemize}


\paragraph*{Étude préliminaire}
On commence cette étude par quelques expériences afin de soulever quelques problématiques. On peut voir figure~\ref{fig_Iexptest} page~\pageref{fig_Iexptest} la forte influence de la valeur de la bande passante sur la distribution de point $Y$. 

\subparagraph*{}
Par définition, on sait que \textbf{lorsque $\Phi(X)$ est distribué suivant une loi notmale, $Y$ suit une loi de Rayleigh (loi de la norme d'un vecteur gaussien).}. Bien que l'on ai aucune information sur la nature de $\Phi(X)$, esssayons d'estimer, sur le jeu de donnée Iris, la distribution du vecteur $Y$ pour chaque classe par une loi de Rayleigh. On peut également essayer d'estimer une distrition exponentielle, pour deux raisons : elle dépend également d'un seul paramètre, et est de plus monotone. Quelques résultats sont visibles figure~\ref{fig_Iexptest3} page~\pageref{fig_Iexptest3}. 



 
\begin{figure}
	\center
	\includegraphics[scale=0.60]{img/part2/sectionLoiExp/testSigma.png}
	\caption{\label{fig_Iexptest} Distribution of the distant vector Y, where X rises from a normal distribution and different values of the bandwidth.}
	\label{Référence}
\end{figure}

%\begin{figure}
%	\center
%	\includegraphics[scale=0.50]{img/part2/sectionLoiExp/testsigma5.png}
%	\caption{\label{fig_Iexptest2} Distribution à postériori du vecteur Y pour un noyau gaussien et $sigma$ = 5}
%	\label{Référence}
%\end{figure}


\begin{figure}
	\includegraphics[scale=0.45]{img/part2/sectionLoiExp/IrisComp.png}
	\caption{\label{fig_Iexptest3} Distribution of each class of the Isis dataset, for different values of the bandwidth, and its estimation by an exponential distribution (solid red line) and a Rayleigh(dashed green line).}
	\label{Référence}
\end{figure}


\clearpage





\section{EM à noyau}

\subsection{Estimation des paramètres}
\label{SectionEstimation}

Une fois encore nous allons chercher à estimer chaque paramètre par le maximum de vraisemblance. En ce qui concerne les centroids ($\mu$), nous utiliserons l'estimateur gaussien :
\begin{equation} \label{mono-simpleEst}
\hat{\mu} = \sum_{i=1}^N \Phi(x_i)
\end{equation}
Cette estimateur est toutefois pleinement justifié si l'on suppose la répartition gaussienne de $\Phi(X)$.

\subsubsection{Distribution exponentielle}

\paragraph{modèle univarié}

Supposons donc que $Y$ suit une distribution exponentielle, dont la densité est donnée par 
\begin{equation}
f_{\beta}(y) = \frac{1}{\beta} \exp \left( {-\frac{y}{\beta}} \right) \; \; \forall y \: \in \: \mathbb{R}^+
\end{equation}


La log vraisemblance vaut :
\begin{equation}
L(x, \beta) = \sum_{i=1}^n -\frac{y_i}{\beta} - n \ln(\beta)
\end{equation}

En annulant la dérivée partielle suivant $\beta$, on obtient :
\[
\begin{array}{r c l}
\hat{\beta} & = & \frac{1}{n} \sum_{i=1}^n {\Vert \Phi(x_i)- \mu \Vert}^2
\end{array}
\]
qui correspond bien à un maximum


Puis, en utilisant (\ref{mono-simpleEst}), et l'astuce du noyau :
\[
\begin{array}{r c l}
\hat{\beta} & = & \frac{1}{n} \sum_{i=1}^n \langle \Phi(x_i) - \sum_{j=1}^n \Phi(x_j), \; \Phi(x_i) - \sum_{j=1}^n \Phi(x_j) \rangle \\
& & \\
& = & \frac{1}{n} \sum_{i=1}^n \left[ k(x_i, x_i) - \frac{2}{n} \sum_{j=1}^n k(x_i, x_j) + \frac{1}{n^2} \sum_{j=1}^n \sum_{l=1}^n k(x_j, x_l)  \right]
\end{array}
\]

soit finalement :
\begin{empheq}[box=\fbox]{equation}
   \hat{\beta} = \frac{1}{n} \sum_{i=1}^n k(x_i, x_i) - \frac{1}{n^2} \sum_{i=1}^n \sum_{j=1}^n k(x_i, x_j)
\end{empheq}

\paragraph{modèle multivarié}
Supposons maintenant que $Y$ est distribué suivant un mélange à $K$ composante de loi exponentielle :
\[
\left\{
\begin{array}{c c c}
f(x) & = & \sum_{k=1}^K \pi_k \frac{1}{\beta_k} \exp \left( \frac{1}{\beta_k} {\Vert \Phi(x) - \mu_k  \Vert}^2 \right) \\
\sum_{k=1}^K \pi_k & = & 1
\end{array}
\right.
\]

Cette fois on a deux familles de paramètres à estimer
\begin{itemize}
	\item K le nombre de composantes. Nous le supposerons fixé et connu pour l'instant. Nous proposerons dans une autre section un moyen de l'estimer.
	
	\item $(\pi_k)$ les proportions du mélange
	
	\item $(\beta_k)$ les paramètres de chaque distribution.
\end{itemize}

\subparagraph*{}
En utilisant les deux familles d'hyper-paramètres introduites dans la précédente partie, $(z_{i,k})$ et $(t_{ik})$, on peut réécrire la log vraisemblance  :
\begin{center}
$L((\beta_k, \pi_k, \mu_k) | x_1...x_N) =  \sumi \sumk t_{i, k}\left(  -\log \beta_k - \frac{1}{\beta_k}y_{i, k} + \log \pi_k \right)$
\end{center}

En réintroduisant les centroids dans l'expression, on a finalement :

\begin{equation}
\boxed{
\forall k \in [1, K] \quad \left\{
\begin{array}{rcl}

\hat{y}_{i, k} & = & k(x_i, x_i) - \frac{2}{\sumt} \sumj t_{j, k} k(x_i, x_j) \\

& & + \frac{1}{{(\sumt)}^2} \sumj \suml t_{j, k} t_{l, k} k(x_j, x_l)\\

& &   \\
\hat{\beta_k} & = & \frac{1}{\sumt} \sumi t_{i, k} k(x_i, x_i) - \frac{1}{ \left( \sumt \right)^2} \sumi \sumj t_{i, k} t_{j, k} k(x_i, x_j) \\

\end{array}
\right.
}
\end{equation}





\subsubsection{Distribution de Raileigh}

Comme nous l'avons expliqué précédemment, une loi de Rayleigh peut être vu comme la loi de la norme d'un vecteur Gaussien à deux dimensions, où les coordonnées sont indépendantes centrées et de même variances. Sa densité est donnée par :

\begin{center}
$f(x; \theta) = \frac{x}{\theta^2} \exp \left( -\frac{x^2}{2\theta^2} \right)$
\end{center}

\paragraph{Modèle univarié}

On suit le même schéma
\[
\left.
\begin{array}{rcl}

L(Y; \theta) & = & \sumi \left( \log(y_i) - \frac{2 y_i^2}{2\beta^2} \right) -2N \log(\theta) \\

& &   \\

\frac{\partial L}{\partial \theta} & = & \frac{1}{\theta} \left( \sumi \frac{y_i^2}{\theta^2} - 2N \right) \\

\end{array}
\right.
\]

On obtient :
\[
\frac{\partial L}{\partial \theta}  \left\{
\begin{array}{rcl}

& > 0 & \text{ si } \theta^2 < \frac{1}{2N} \sumi y_i^2 \\

 & = 0 &  \text{ si } \theta^2 \frac{1}{2N} \sumi y_i^2\\

& < 0 & \text{ si } \theta^2 > \frac{1}{2N} \sumi y_i^2 \\

\end{array}
\right.
\]

et finalement :
\begin{equation}
\hat{\theta}^2 = \frac{1}{2N} \sumi y_i^2 
\end{equation}

\paragraph{Modèle multivarié}

Comme vu précédemment 
\[
\left.
\begin{array}{rcl}

L(Y; \theta_k, \pi_k) & = & \sumi \sumk \left( \log(\pi_k) + \log(y_{i, k}) - \frac{2 y_{i, k}^2}{2\beta_k^2} -2\log(\beta_k) \right) \\

\end{array}
\right.
\]

Et l'on obtient les estimateurs :
\begin{equation}
\forall k \in [1, K] \qquad \hat{\theta_k}^2 = \frac{1}{2\sumi t_{i, k}} \sumi y_{i, k}^2 
\end{equation}

\subsection{Choisir le nombre de composante du mélange}

\paragraph{}
Si le nombre de composante est inconnue, On se propose d'utiliser ne première approche le critère d'information d'Akaike (abrégé AIC). Celui permet de trouver un compromis entre la précision du modèle et sa complexité (i.e. le nombre de paramètre à estimer). En prenant en compte le nombre de paramètre à estimer, on obtient :
\begin{equation}
K = \underset{m \geq 1}{\text{argmax}} \left( l((\beta_k, \mu_k, \pi_k)_{k=1}^m \vert Y, Z) - 3m  \right) = 0
\end{equation}

\paragraph*{}
On rappelle rapidement les expressions de la vraisemblance pour chaque modèle, obtenue section \ref{SectionEstimation} :
\[
\left.
\begin{array}{rcl}

L_{exp}((\beta_k, \pi_k, \mu_k) | x_1...x_N) & = & \sumi \sumk t_{i, k}\left(  -\log \beta_k - \frac{1}{\beta_k}y_{i, k} + \log \pi_k \right) \\


L_{ray}(\beta_k, \pi_k, \mu_k) | x_1...x_N) & = & \sumi \sumk \left( \log(\pi_k) + \log(y_{i, k}) - \frac{2 y_{i, k}^2}{2\beta_k^2} -2\log(\beta_k) \right) \\

\end{array}
\right.
\]



\subsection{Choix de la bande passante}

\paragraph{}
Restons dans l'esprit bayésien et essayons d'estimer la bande passante par la méthode du maximum de vraisemblance. Pour des raisons de praticité, nous ne présenterons les résultats que pour un mélange univarié.
\linebreak

\subparagraph{}
Pour rappel, nous avons vu, section~\ref{SectionEstimation} :
\[
\forall i \quad \left\{
\begin{array}{rcl}

k(x_i, x_j) & = & \exp \left( -\frac{d(x_i, x_j)}{\sigma} \right) \\

& &   \\

\hat{y}_{i, k} & = & k(x_i, x_i) - \frac{2}{N} \sumj k(x_i, x_j) + \frac{1}{N^2} \sumj \suml k(x_j, x_l)\\

& &   \\

L(X, \beta, \sigma | \hat{\mu}) & = & \sumi -\frac{1}{\beta}y_i - N \log (\beta ) \\ 

\end{array}
\right.
\]

Sachant que $\frac{\partial k}{\partial \sigma}(x_i, x_j) = \frac{d(x_i, x_j)}{\sigma^2} \exp \left( -\frac{d(x_i, x_j)}{\sigma} \right)$,on obtient, en notant $h_i(X)$ la quantité $\sumj d(x_i, x_j) \exp \left( -\frac{d(x_i, x_j)}{\sigma} \right)$, l'expression du gradient de la log vraisemblance suivant $\sigma$ :

\[
\left.
\begin{array}{rcl}

 &  & \sumi \left( -\frac{2}{N} h_i(X) +\frac{1}{N^2} \sumj h_j(X) \right) \\

 & &   \\

\text{i.e.} & & \frac{\partial L}{\partial \sigma} = \sumi \sumj d(x_i, x_j) \exp \left( -\frac{d(x_i, x_j)}{\sigma} \right) \\

\end{array}
\right.
\]

\subparagraph*{Avec une loi de Raileigh} Seule la vraisemblance change; On a donc :

\[
\left.
\begin{array}{rcl}

\frac{\partial L}{\partial \sigma} & = & \sumi \left( h_i(X) \frac{1}{y_i} -\frac{h_i(X)}{\beta^2} y_i \right) \\

\end{array}
\right.
\]

Remarque : Pour les deux distribution, la bande passante ne dépend que de $h_i$ et $y_i$.

\subparagraph*{Analyse}
On peut d'abord souligner le fait que la vraisemblance, pour une distribution exponentielle, semble être une fonction croissante de la bande passante. Cependant, avec une distribution Rayleigh, la non linéarité de l'expression est trop importante pour en extraire un comportement.

\subsection{Classification}

Nous allons voir dans cette partie comment classer un nouveau point $x^*$, de deux manière différentes :
\begin{itemize}
	\item D'abord avec les probabilités conditionnelles
	
	\item Puis avec une méthode similaire à celle introduite par Nicolas Wicker and Alejandro Murua in \cite{Wicker2014}, appelée CID, et qui utilisent l'ensemble des distances inter classes.
\end{itemize}


\paragraph{Par les probabilités conditionnelles}
On attribut la classe $g^*$ à $x^*$ suivant la règle suivante :
\begin{center}
$g^* = \underset{g=1..K}{\text{argmax}} \pi_g f_{\beta_g} \left( {\Vert \Phi(x^*) - \mu_g \Vert}^2 \right)$
\end{center}

D'une point de vue pratique, on remarque que l'on a maintenant besoin de la connaissance de chaque $\mu_g$. Mais ceux-ci étant obtenir à partir de la fonction $\Phi$, on a donc pas directement accès à l'information. Cependant, on remarque en développant le calcul de $y\star$ :
\[
\left.
\begin{array}{c c c}
{\Vert \Phi(x^*) - \mu_g \Vert}^2 & = & \langle \Phi(x^*) - \mu_g, \Phi(x^*) - \mu_g \rangle \\
&  &  \\
& = & k(x^*, x^*)  - \underbrace{\frac{2}{\bar{t_k}} \sumj t_{j, k} k(x^*, x_j)}_{en \quad \theta(N)} \; + \underbrace{\frac{1}{{\bar{t_k}}^2} \sumj \suml t_{j, k} t_{l, k} k(x_j, x_l)}_{\text{Already known}} \\
\end{array}
\right.
\]

Ainsi, si l'on garde en mémoire les valeurs du vecteur $Y$ et de la famille d'hyperparamètres $t_{i, k}$, on peut diminuer la redondance dans les calculs.

\paragraph{The Bayes discriminant conditionnally-independent distance rule (CID)} \textbf{ } \\

\subsection{Résultats intermédiaires}

\paragraph{}
On teste dans un premier temps cet algorithme sur le jeu de données Iris. Pour ces premiers testes, on utilise des valeurs arbitraires de la bande passante : parmi un ensemble de valeur, on conserve celle qui fournit les meilleurs résultats.

\paragraph{}
On remarque empiriquement que l'étape E de l'algorithme est extrêmement sensible à l'initialisation, et dégénère régulièrement. Il a donc fallu plutôt insister sur l'étape S (stochastique) de l'initialisation, qui fournit de meilleurs résultats.

\paragraph{}
Pour cette première vague de tests, les résultats sont meilleurs pour une loi exponentielle. Ceux-ci sont d'ailleurs disponible figure\ref{fig_resInt1}.

\begin{figure}
	\center
	\includegraphics[scale=1]{img/part2/testsIris.png}
	\caption{\label{fig_resInt1} First results on the Iris datasets. On the left the exponential distribution is used, whereas the Rayleigh is used on the right.}
	\label{Référence}
\end{figure}


\section{Application à la classification des protéines}

\paragraph{}
Comme nous l'avons expliqué précédemment, après choisi un distance sur l'espace de départ et un Noyau, l'algorithme KEM nous permet de traiter des types de données très variés. Nous considérerons dans cette partie le jeu de données proposé par  Wicker et al (2001) \cite{Wicker2001}. Il contient un ensemble de protéines, caractérisées par leurs séquences d'acides aminés (lettres). Ces protéines peuvent être séparées classes, en fonction de leur propriétés biologiques. Nous allons donc essayer de retrouver ces classes grâce au KEM.

\paragraph{}
Comme l'espace de départ n'est pas euclidien (espace de lettre), il est nécessaire de définir une mesure de similarité entre protéines. On construit celle-ci à partir du pourcentage de d'acides aminés identiques entre deux séquences. On peut donc résumer cela, en notant $a_{i, m}$ le $m^{ième}$ acide aminé sur $M$ du gène $i$ :

\[
\left.
\begin{array}{c c c}
s_(x_i, x_j) & = & \frac{1}{M} \cdot \sum_{l}^M\delta_{a_{i, m}, a_{j, m}} \\
& & \\
d((x_i, x_j) & = & \sqrt{2 - 2s(x_i, x_j)} \\
& & \\
k(x_i, x_j) & = & \exp \left( \frac{-d(x_i, x_j)^2}{\sigma} \right)
\end{array}
\right.
\]


%\section{Application à la classification de textes}


%\section{Conclusion}
