\select@language {french}
\contentsline {part}{Introduction}{2}
\contentsline {paragraph}{}{2}
\contentsline {paragraph}{}{2}
\contentsline {paragraph}{}{2}
\contentsline {part}{I\hspace {1em}L'algorithme EM}{3}
\contentsline {section}{\numberline {0.1}Introduction aux mod\IeC {\`e}les de m\IeC {\'e}lange}{4}
\contentsline {section}{\numberline {0.2}fonction de vraisemblance}{4}
\contentsline {paragraph}{}{4}
\contentsline {paragraph}{}{4}
\contentsline {section}{\numberline {0.3}Construction de l'algorithme}{5}
\contentsline {subparagraph}{}{5}
\contentsline {paragraph}{}{6}
\contentsline {subsection}{\numberline {0.3.1}L'algorithme SEM}{6}
\contentsline {section}{\numberline {0.4}Estimation des param\IeC {\`e}tres pour quelques distributions}{7}
\contentsline {subsection}{\numberline {0.4.1}M\IeC {\'e}lange Gaussien}{7}
\contentsline {subsubsection}{M step du mod\IeC {\`e}le univari\IeC {\'e}}{7}
\contentsline {subsubsection}{M step du mod\IeC {\`e}le multivari\IeC {\'e}}{8}
\contentsline {subsubsection}{Extension au cas multivari\IeC {\'e}}{8}
\contentsline {subsection}{\numberline {0.4.2}M\IeC {\'e}lange B\IeC {\'e}ta}{8}
\contentsline {subsubsection}{M step du mod\IeC {\`e}le univari\IeC {\'e}}{8}
\contentsline {subsubsection}{M step du mod\IeC {\`e}le multivari\IeC {\'e}}{9}
\contentsline {subsubsection}{Extensions au cas multidimensionnel}{10}
\contentsline {subsection}{\numberline {0.4.3}M\IeC {\'e}lange Gamma}{10}
\contentsline {subsubsection}{M step of the univariate model}{11}
\contentsline {subsubsection}{M step of the multivariate model}{11}
\contentsline {section}{\numberline {0.5}R\IeC {\'e}sultats}{11}
\contentsline {subsection}{\numberline {0.5.1}Visualisation}{11}
\contentsline {paragraph}{}{11}
\contentsline {paragraph}{}{11}
\contentsline {subsection}{\numberline {0.5.2}M\IeC {\'e}lange Gaussien}{11}
\contentsline {paragraph}{}{11}
\contentsline {subsection}{\numberline {0.5.3}M\IeC {\'e}lange B\IeC {\'e}ta}{13}
\contentsline {section}{\numberline {0.6}Annexe : r\IeC {\'e}solution de syst\IeC {\`e}me d'\IeC {\'e}quation}{13}
\contentsline {subsection}{\numberline {0.6.1}La m\IeC {\'e}thode de Brent}{13}
\contentsline {subsection}{\numberline {0.6.2}La m\IeC {\'e}thode de Dekker}{14}
\contentsline {subsection}{\numberline {0.6.3}Les modifications de Brent}{14}
\contentsline {subsection}{\numberline {0.6.4}Impl\IeC {\'e}mentation num\IeC {\'e}rique}{15}
\contentsline {part}{II\hspace {1em}EM \IeC {\`a} noyau}{16}
\contentsline {section}{\numberline {0.7}Introduction au probl\IeC {\`e}me}{17}
\contentsline {paragraph}{}{17}
\contentsline {paragraph}{}{17}
\contentsline {paragraph}{}{17}
\contentsline {section}{\numberline {0.8}EM \IeC {\`a} noyau}{20}
\contentsline {subsection}{\numberline {0.8.1}Estimation des param\IeC {\`e}tres}{20}
\contentsline {subsubsection}{Distribution exponentielle}{20}
\contentsline {paragraph}{mod\IeC {\`e}le univari\IeC {\'e}}{20}
\contentsline {paragraph}{mod\IeC {\`e}le multivari\IeC {\'e}}{20}
\contentsline {subsubsection}{Distribution de Raileigh}{21}
\contentsline {paragraph}{Mod\IeC {\`e}le univari\IeC {\'e}}{21}
\contentsline {paragraph}{Mod\IeC {\`e}le multivari\IeC {\'e}}{21}
\contentsline {subsection}{\numberline {0.8.2}Choisir le nombre de composante du m\IeC {\'e}lange}{22}
\contentsline {paragraph}{}{22}
\contentsline {subsection}{\numberline {0.8.3}Choix de la bande passante}{22}
\contentsline {paragraph}{}{22}
\contentsline {subparagraph}{}{22}
\contentsline {subsection}{\numberline {0.8.4}Classification}{23}
\contentsline {paragraph}{Par les probabilit\IeC {\'e}s conditionnelles}{23}
\contentsline {paragraph}{The Bayes discriminant conditionnally-independent distance rule (CID)}{23}
\contentsline {subsection}{\numberline {0.8.5}R\IeC {\'e}sultats interm\IeC {\'e}diaires}{23}
\contentsline {paragraph}{}{23}
\contentsline {paragraph}{}{23}
\contentsline {paragraph}{}{23}
\contentsline {section}{\numberline {0.9}Application \IeC {\`a} la classification des prot\IeC {\'e}ines}{24}
\contentsline {paragraph}{}{24}
\contentsline {paragraph}{}{24}
\contentsline {part}{Conclusion}{25}
\contentsline {paragraph}{}{25}
\contentsline {paragraph}{}{25}
\contentsline {paragraph}{}{25}
