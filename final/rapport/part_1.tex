\part{L'algorithme EM}

\section{Introduction aux modèles de mélange}
Considérons ${\bx}=\{ {\bx}_1,...,{\bx}_n\}$ $n$ vecteurs indépendants de $\mathbb{R}^d$ tels que chaque ${\bx}_i$ est distribué suivant une loi de densité
\begin{equation}
  f({\bx}_i|\theta) = \sum_{k=1}^K p_k h({\bx}_{i}| \blambda_{k},\balpha)
\end{equation}

Où :
\begin{itemize}
	\item les $p_k$sont les proportions du modèle de mélange, ($0<p_k<1$ quelque soit $k=1,...,K$ et $\sum_1^K p_k=1$).
	
	\item $h(\cdot| \blambda_{k},\balpha)$ représente une distribution $d$-dimensionnelle  paramétrisée par $\blambda_k$ et $\balpha$. Le paramètre $\balpha$ ne dépend pas de $k$ et est commun à chaque composante du mélange.

	\item $\theta=(p_1,\ldots,p_K,\blambda_1,\ldots,\blambda_K, \balpha)$ représente le vecteur à estimer.
\end{itemize}
 

On introduit ensuite le vecteur indicateur : {\em labels}
${\bz}=\{ {\bz}_1,...,{\bz}_n\}$, avec ${\bz}_i=(z_{i1},\ldots,z_{iK})$,

\[
z_{ik}=  \left\{
\begin{array}{rcl}

& 1 & \text{ si } {\bx}_i \; \text{provient de la kième distribution} \\

 & 0 &  \text{sinon}\\

\end{array}
\right.
\]


Bien que le vecteur ${\bz}$ soit à priori inconnue, Nous allons l'utiliser pour estimer le vecteur de paramètre $\theta$ par la méthode dite de 'Espérance Maximisation\cite{Dempster97}. Dans un second temps, nous proposerons une version stochastique de l'algorithme, appelée SEM \cite{McLachlanPeel00}, puis une seconde version proche de k-mean appelée CEM


\section{fonction de vraisemblance}

En statistique, la fonction de vraisemblance (ou plus simplement notée vraisemblance), permet de mesurer l'adéquation entre une distribution observée sur un échantillon de données et une loi de probabilité censée décrire cet échantillon.

\paragraph{}
Considérons $x_1, x_2, ... x_N$ N observations d'une variable aléatoire de densité $x=f\left( \cdot \vert \theta \right)$, où $\theta$ joue le rôle du jeu de paramètres que l'on souhaite évaluer. On définie la fonction de vraisemblance au vue des observation $x$ par :

\begin{equation} 
	\label{eq:vrais}
l \left( \theta \vert x \right) = \prod_{i=1}^N f \left( x_i \vert \theta \right)
\end{equation}

Toutefois, on préférera travailler avec la fonction dite de log-vraisemblance, qui consiste à prendre le logarithme naturel de \ref{eq:vrais} 
\begin{equation} 
	\label{eq:lvrais}
L \left( \theta \vert x \right) = \sum{i=1}^N ln \left( f \left( x_i \vert \theta \right) \right)
\end{equation}

\paragraph{}
Sachant ceci, on peut donc récrire la log-vraisemblance d'un modèle de mélange par :
 
\begin{equation}
  \label{eq:vraisemblance}
  l(\theta|\bx_1,\ldots,\bx_n)=\sum_{i=1}^n \ln \left(\sum_{k=1}^K p_k h(\bx_i,\blambda_k, \balpha)\right).
\end{equation}



\section{Construction de l'algorithme}

Nous introduisons donc la deuxième famille de paramètres, évoquée lors de la section précédente, ${\bz}$, appelée "paramètre manquant". L'expression de la densité et de la log vraisemblance deviennent alors :
\begin{center}
$f(x, z, \theta) = f(z\mid x, \theta)f(x, \theta)$ \\
$ln\left[f(x, \theta)]\right]=ln\left[f(x, z, \theta)\right] - ln\left[f(z\mid x, \theta)\right]$
\end{center}

Cependant, le paramètre $\theta$ est toujours supposé inconnu. On l'initialise donc avec la valeur $\theta_0$. On peut ensuite intégrer suivant $z\mid x, \theta $ :
\begin{center}
$E_{z \mid x, \theta_0}\left[ln\left(ln(x, \theta)\right)\right]=ln\left(ln(x, \theta)\right)$ \\
$E_{z\mid x, \theta}\left[ln(f(x, z, \theta)\right]=Q(\theta, \theta_0)$ \\
$E_{z\mid x, \theta}\left[ln(f(z,\mid z, \theta)\right]=H(\theta, \theta_0)$
\end{center}

\subparagraph{}
Or $H(\theta, \theta_0)=\int ln(\left[f(z, \mid x, \theta)\right]f(z, \mid x, \theta_0)dz$ 

On construit ensuite la quantité :
\begin{equation}
-(H(\theta, \theta_0)-H(\theta_0, \theta_0))=-\int f(z\mid x, \theta)ln\frac{f(z\mid x, \theta)}{f(z\mid x, \theta_0)}dz
\end{equation}

Ce terme est appelée la divergence de Kullback Leibler (\cite{kullback1951}). On peut montrer que cette divergence est toujours positive. Elle peut alors s'interpréter comme une "distance" entre deux distribution, bien que non symétrique.

\subparagraph*{}
On choisit maintenant $\theta_1$ tel que $Q(\theta_1, \theta_0)>Q(\theta_0, \theta_0)$. Dans ce cas :
\begin{eqnarray}
  ln\left[l(\theta_1\mid x)\right])-ln\left[l(\theta_0\mid x)\right]) & = & Q(\theta_1, \theta_0)-H(\theta_1, \theta_0)-(Q(\theta_0, \theta_0)-H(\theta_0, \theta_0)) \nonumber\\
   & = & \underbrace{Q(\theta_1, \theta_0)-Q(\theta_0, \theta_0)}_{\geq0 \quad \text{par le choix de } \theta_1}-\underbrace{(H(\theta_1, \theta_0)-H(\theta_0, \theta_0))}_{\geq 0 \quad \text{la divergence}} \nonumber
\end{eqnarray}

On obtient donc :
\begin{center}
$L(\theta_1\mid x) \geq L(\theta_0\mid x)$
\end{center}
On peut donc construire de proche en proche une suite de paramètre $(\theta)_i$ qui rend la log-vraisemblance croissante. Cette dernière étant bornée, on est donc assuré de la convergence de la suite un maximum (éventuellement local) de la vraisemblance.

\begin{algorithm}
\caption{S'approcher d'un maximum de L avec la précision $\epsilon$}
\begin{algorithmic} 
\REQUIRE $\epsilon, \theta_{init}$
\STATE $\theta_{courant} \leftarrow \theta_{init}$
\STATE $L_{courant} = L(\theta_{courant}|x)$
\STATE $L_{ancien} = -1$ (par convention)
\STATE $N_{iter} \leftarrow 0$
\WHILE{$L_{courant}-L_{ancien} > \epsilon$}
	\STATE $L_{ancien} = \theta_{courant}$
	\STATE $\theta_{courant} = {argmax}_{ \begin{array}{l} \theta \end{array}} \left( Q \left( \theta, \theta_{courant} \right) \right)$
	\STATE $N_{iter} \leftarrow N_{iter}+1$
\ENDWHILE
\RETURN $\theta$
\end{algorithmic}
\end{algorithm}

Pour simplifier les notations, nous noterons dans la suite de cette section $(k_i)$ la famille d'indice définie par :
\begin{center}
$\forall i \in \{1...N\}$, $k_i = {arg}_k (z_i^k \neq 0)$
\end{center}

Il reste maintenant à obtenir une expression de $Q$. On observe d'abord que :
\[
\begin{array}{r c l}
f(x, z, \theta) & = & \prod_{i=1}^N f(x_{i}, \theta_{k_i}) \\
 & = & \prod_{i=1}^N f(x_i | \theta_{k_i} ) f(\theta_{k_i}) \\
  & = & \prod_{i=1}^N p_k f_{k_i}(x_i, \theta_{k_i}) \\
\end{array}
\]
Soit, en passant à l'espérance :
\[
\begin{array}{r c l}
Q(\theta, \theta_0)  & = & \sum_{i=1}^N \sum_{k=1}^K \mathbb{E} \left[ z_{i, k} \vert x, \theta^i \right] \cdot p_k \log \left( f(x_i \vert \theta_k) \right)
\end{array}
\]
En notant $t_{i, k}$ la quantité $\mathbb{E} \left[ z_{i, k} \vert x, \theta^i \right]$, on obtient, grâce à la forme d'inversion de Bayes :
\[
\left.
\begin{array}{r c l}
 t_{i, k}^{iter+1} & = & \frac{p(x_i|k, \theta^{iter}) p(k|\theta^{iter})}{\sum_{l=1}^K p(x_i|l, \theta^{iter}) p(l|\theta^{iter})}\\
 & = & \frac{f_k(x_i, \theta_k^{iter}) p_k^{iter}}{\sum_{l=1}^K f_l(x_i, \theta_l^{iter}) p_l^{iter}}\\
 \end{array}
\right.
\]

En dérivant selon les $p_k$, on obtient également :
\[
\left.
\begin{array}{r c l}
p_k^{iter+1} & = & \frac{1}{N} \sum_{i=1}^N p(k|x_i, \theta^{courant}) \nonumber \\
 & = & \frac{1}{N} \sum_{i=1}^N t_{i, k}^{iter+1}
\end{array}
\right.
\]

Ainsi, la maximisation de la quantité Q peut être réalisée en deux étapes :
\begin{itemize}
	\item \textbf{Étape E} : où l'on met à jour les $t_{i, k}$.
	
	\item \textbf{Étape M} : où l'on met à jour le paramètre $\theta$. On proposera dans la soirée une méthode d'estimation des paramètres via le maximum de vraisemblance.
	
	\item On met également à jour les paramètres de proportion du mélange.
\end{itemize}

\paragraph{}
On obtient alors l'algorithme final :

\begin{algorithm}
\caption{Converger vers un maximum de L avec la précision $\epsilon$}
\begin{algorithmic} 
\REQUIRE $\epsilon, \theta_{init}$
\STATE $\theta_{courant} \leftarrow \theta_{init}$
\STATE $N_{iter} \leftarrow 0$
\WHILE{$L_{courant}-L_{ancien} > \epsilon$}
	\STATE \textbf{E step} : calculer les $t_{i, k}$ via la valeur courante de $\theta$
	\STATE \textbf{M step} : Mettre à jour l'estimation du paramètre $\theta$ grâce aux probabilités conditionnelles $t_{i, k}$, et mettre à jour les proportions $\pi$
	\STATE $N_{iter} \leftarrow N_{iter}+1$
\ENDWHILE
\RETURN $\theta$
\end{algorithmic}
\end{algorithm}

\subsection{L'algorithme SEM}

L'algorithme SEM est une version stochastique de l'EM, qui consiste à incorporer entre l'étape E et M une prédiction des labels $\bz_i$, $i=1,\ldots,n,$, en les tirant suivant une loi uniforme. Ainsi, en partant d'un paramètre initial $\theta^0$, l'algorithme SEM peut être résumer en trois étapes :

\begin{itemize}
	\item \textbf{E step:} mettre à jour les probabilités conditionnelles $t^m_{ik}$ $(1 \leq i \leq n, 1 \leq k \leq
  K)$ grâce à la valeur courante du paramètre$\theta$ 
  
	\item \textbf{S step:} Générer les labels ${\bz}^m=\{ {\bz}^m_1,...,{\bz}^m_n\}$ en attribuant à chaque point ${\bx}_i$ un label tiré suivant une loi uniforme, et dont les probabilités d'appartenance à chaque classes sont les $(t^m_{ik}, 1 \leq k \leq K)$.
  
	\item \textbf{M step:} mettre à jour $\theta$ en utilisant ces labels en maximisant :
\begin{equation} \label{eq:mStepSEM}
  L(\theta| {\bx}_{1},\ldots,{\bx}_{n}, {\bt}^m)
    =\sum_{i=1}^{n}\sum_{k=1}^{K} z_{ik}^m \ln \left [p_{k} h(\textbf{x}_{i}|\blambda_{k},\balpha)\right],
\end{equation}
\end{itemize}

De par sa nature stochastique, l'algorithme SEM ne converge pas systématiquement. Il génère une chaîne de Markov dont on peut montrer que la distribution stationnaire est plus ou moins concentré autour de l'estimateur du maximum de vraisemblance. L'une des manières naturelles d'estimer le paramètre est alors de considérer, parmi toutes les itérations, celui qui a maximiser la vraisemblance.


\section{Estimation des paramètres pour quelques distributions}

\subsection{Mélange Gaussien}

Commençons par étudier un mélange gaussien, en raison de sa simplicité. Dans cette section, le schéma sera toujours le même : on commencera par étudier le modèle uni-varié, puis nous étendrons le résultat aux mélanges.

	\subsubsection{M step du modèle univarié}
Dans le cas univarié, l'hyperparamètre $z$ vaut toujours $1$. Il n'est donc plus nécessaire de le prendre en compte. Il nous suffit donc d'annuler le gradient de la log vraisemblance : 

\[
\begin{array}{r c l}
%---------------------------------------------------------------%
L \left( \theta | x_1...x_n \right) & = & \sum_{i=1}^n \ln{\frac{1}{\sqrt{2\pi \sigma^2}} \exp \left( -\frac{(x_i - \mu)^2}{2\sigma^2} \right)} \\
%---------------------------------------------------------------%
 & = &  - \frac{n}{2} \ln{\left( \pi \right)} - \frac{n}{2} \ln{\left( 2 \sigma^2 \right)} + \frac{1}{2 \sigma^2} \sum_{i=1}^n (x_i - \mu)^2
 %---------------------------------------------------------------%
\end{array}
\]


\[
\left\{
\begin{array}{r c l}
%---------------------------------------------------------------%
\frac{\partial{L}}{\partial{\mu}} & = & \sum_{i=1}^{N} \frac{(x_i-\mu)}{{\sigma}^2} \\
%---------------------------------------------------------------%
\frac{\partial{L}}{\partial{\sigma}} & = & \sum_{i=1}^{N} \left[\frac{{(x_i-\mu)}^2}{{\sigma}^3} - \frac{1}{\sigma}\right] \\
%---------------------------------------------------------------%
\end{array}
\right.
\]

En annulant ce gradient, on obtient donc directement et sans oublier de vérifier que la solution correspond bien à un maximum :

\[
\left\{
\begin{array}{r c l}
\hat{\mu}_k & = & \frac{1}{N} \sum_{i=1}^{N}x_i \\

{\hat{\sigma}_k}^2 & = & \frac{1}{N} \sum_{i=1}^{N} {\left( x_i-\mu_k \right) }^2
\end{array}
\right.
\]



	\subsubsection{M step du modèle multivarié}
	
On considère maintenant un mélange multivarié et unidimensionnel :

\begin{equation}
f(x) = \sum_{k=0}^{K} p_k \left[ \frac{1}{\sqrt{2\pi}\sigma_k} \exp{\left( -\frac{(x_i-\mu_k)^2}{2{\sigma_k}^2} \right)} \right]
\end{equation}  

Toujours en développant la log vraisemblance, on obtient :

\[
\begin{array}{r c l}
L = \sum_{i=1}^{n} \sum_{k=1}^{K} t^m_{ik} \left[ \ln{p_k} +  \left(-\frac{1}{2} \pi \ln{2} - \ln{\sigma_k} - \frac{\left(x_i - \mu_k\right)^2}{2{\sigma_k}^2}\right) \right]
\end{array}
\]

et le gradient, défini pour tout $k$  :

\[
\left\{
\begin{array}{r c l}
%---------------------------------------------------------------%
\frac{\partial{L}}{\partial{\mu_k}} & = & \sum_{i=1}^{N}t_{ik}^{m}\frac{(x_i-\mu_k)}{{\sigma_k}^2} \\
%---------------------------------------------------------------%
\frac{\partial{L}}{\partial{\sigma_k}} & = & \sum_{i=1}^{N} t_{ik}^{m}\left[\frac{{(x_i-\mu_k)}^2}{{\sigma_k}^3} - \frac{1}{\sigma_k}\right] \\
%---------------------------------------------------------------%
\end{array}
\right.
\]

Puis :

\[
\left\{
\begin{array}{r c l}
%---------------------------------------------------------------%
\frac{\partial{L}}{\partial{\mu_k}} = 0  & & \hat{\mu}_k =\left( \frac{1}{\sum_{i=1}^{N}t_{i,k}^{m}} \right) \sum_{i=1}^{N}t_{i,k}^{m} x_i\\
%---------------------------------------------------------------%
& \Leftrightarrow & \\
%---------------------------------------------------------------%
\frac{\partial{L}}{\partial{\sigma_k}} = 0  & & \hat{\sigma}_k =\left( \frac{1}{\sum_{i=1}^{N}t_{i,k}^{m}} \right) \sum_{i=1}^{N}t_{i,k}^{m} x_i\\
%---------------------------------------------------------------%
\end{array}
\right.
\]

On s'aperçoit que ces estimateurs correspondent finalement à l'estimateur des paramètres d'un gaussienne, pondérée par les probabilités conditionnelles.


	\subsubsection{Extension au cas multivarié}

On peut facilement étendre ces résultats à des mélanges de gaussiennes multidimensionnelle (et nous le ferons systématiquement dans la suite de ce mémoire). Supposons toutefois que \textbf{les distributions sont indépendantes suivant les dimensions}.


\begin{equation}
f(x) = \sum_{k=0}^{K} p_k \prod_{j=1}^{d} \left[ \frac{1}{\sqrt{2\pi}\sigma_k} \exp{\left( -\frac{(x_i^j-\mu_k)^2}{2{\sigma_k}^2} \right)} \right]
\end{equation} 

On obtient par les mêmes calculs les estimateurs :
\[
\left\{
\begin{array}{r c l}
%---------------------------------------------------------------%
\hat{\mu}_k^j & = & \left( \frac{1}{\sum_{i=1}^{N}t_{i,k}^{m}} \right) \sum_{i=1}^{N}t_{i,k}^{m} x_i^j \\
%---------------------------------------------------------------%
\\
%---------------------------------------------------------------%
\left( \hat{\sigma}_k^j \right) ^2 & = & \left( \frac{1}{\sum_{i=1}^{N}t_{i,k}^{m}} \right) \sum_{i=1}^{N}t_{i,k}^{m}{ \left(x_i^j-\mu_k^j \right)}^2
%---------------------------------------------------------------%
\end{array}
\right.{, \forall k \in \left[1, K \right] , \forall j \in \left[1, d \right]}
\]



\subsection{Mélange Béta}

	\subsubsection{M step du modèle univarié}
	
Once more, we begin with an unidimensional and univariate model.
	
	\subsubsection{M step du modèle multivarié}	
	
Considérons les deux familles de paramètres  $\alpha = (\alpha_1...\alpha_K)$ and $\beta = (\beta_1...\beta_K)$. La densité de probabilité s'écrit alors :

\begin{center}
$f(x_i, \theta) = \sum_{k=1}^K \left(\frac{(x_i)^{\alpha_k}\left(1-x_i\right)^{\beta_k-1}}{B(\alpha_k, \beta_k)} \right)$
\end{center}

La log-vraisemblance s'écrit quant à elle (\ref{eq:finalLogVraisemblance})
\begin{eqnarray} 
% ---------------------------------------------------------- 
L & = & \sum_{i=1}^{n} \sum_{k=1}^{K} \left( t^m_{ik}.\ln \left[ p_k . \frac{x_i^{\alpha_k-1}(1-{x_i}^{\beta_k-1})}{B(\alpha_k,\beta_k)}\right] \right) \nonumber %%\\
%------------------------------------------------------------ 
\end{eqnarray}

Soit
\begin{equation} \label{eq:logVraisBeta2}
L = \sum_{i=1}^{n} \sum_{k=1}^{K} t_{i, k}^m \left( ln(p_k) + (\alpha_k-1)ln(x_i) + (\beta_k-1)ln(1-x_i)-lnB(\alpha_k, \beta_k) \right)
\end{equation}
\\
En dérivant suivant chaque des paramètres, on obtient pour $k$ dans $\lbrace 1...K\rbrace$ :

\[
\left\{
\begin{array}{r c l}
% ------------------------------------------------------------------------------- %
\frac{\partial{L}}{\partial{\alpha_k}} & = & \sum_{i=1}^nt_{i,k}^{m} \left( ln\left( x_i \right) -  \frac{1}{B(\alpha_k,\beta_k)} \frac{\partial{B(\alpha_k,\beta_k)}}{\partial{\alpha_k}} \right) \nonumber \\
% ------------------------------------------------------------------------------- %
\frac{\partial{L}}{\partial{\beta_k}} & = & \sum_{i=1}^nt_{i,k}^{m} \left( ln\left( 1-x_i \right) -  \frac{1}{B(\alpha_k,\beta_k)} \frac{\partial{B(\alpha_k,\beta_k)}}{\partial{\beta_k}} \right) \nonumber
% ------------------------------------------------------------------------------- %
\end{array}
\right.
\]

En annulant le gradient, on obtient :

\[
\left\{
\begin{array}{r c l}
% ------------------------------------------------------------------------------- %
\frac{1}{B(\alpha_k,\beta_k)} \frac{\partial{B(\alpha_k,\beta_k)}}{\partial \alpha_k} & = & \frac{1}{\sum_{i=1}^n t_{i, k}^m} . \sum_{i=1}^n t_{i,k}^{m} ln \left( x_i \right) \nonumber\\
% ------------------------------------------------------------------------------- %
\frac{1}{B(\alpha_k,\beta_k)} \frac{\partial{B(\alpha_k,\beta_k)}}{\partial \beta_k} & = & \frac{1}{\sum_{i=1}^n t_{i, k}^m} . \sum_{i=1}^n t_{i,k}^{m} ln \left(1-x_i \right) \nonumber
% ------------------------------------------------------------------------------- %
\end{array}
\right.
\]

Cependant, ce résultat est probablement difficile à exploiter numériquement. On peut toutefois travailler cette expression en utilisant un résultat sur la dérivée de la fonction bêta(\cite{BetaLoi}) :
\begin{eqnarray}
\frac{\partial{B(x,y)}}{\partial{x}} = B(x,y)(\frac{\Gamma'(x)}{\Gamma(x)}-\frac{\Gamma'(x+y)}{\gamma(x+y)} = B(x,y)(\psi(x)-\psi(x+y)) \nonumber
\end{eqnarray}
En notant $\psi$ la fonction digamma. \\

Ensuite :

\[
\left\{
\begin{array}{r c l}
% ------------------------------------------------------------------------------- %
\left( \psi(\alpha_k) - \psi(\alpha_k + \beta_k) \right) & = & \frac{1}{\sum_{i=1}^n t_{i, k}^m} . \sum_{i=1}^n t_{i,k}^{m} ln \left( x_i \right) \nonumber\\
% ------------------------------------------------------------------------------- %
\left( \psi(\beta_k) - \psi(\alpha_k + \beta_k) \right) & = & \frac{1}{\sum_{i=1}^n t_{i, k}^m} . \sum_{i=1}^n t_{i,k}^{m} ln \left( 1-x_i \right) \nonumber
% ------------------------------------------------------------------------------- %
\end{array}
\right.
\]

On doit donc ensuite inverser un système non linéaire de deux équations à deux inconnues, dont la solution est dans $\left[ 0,1 \right]$. Sachant que la fonction digamma est continue sur cet intervalle, on se propose d'utiliser pour celà la méthode de Newton \cite{Dennis1996}. 

\paragraph*{}
Nous devons également vérifier que ces estimateurs sont bien des maxima. Calculons pour cela la dérivée seconde de la log vraisemblance en ces points.

\[
\left\{
\begin{array}{r c l}
% ------------------------------------------------------------------------------- %
\frac{{\partial}^2 L}{\partial{\alpha_k}^2} & = & - \left( \sum_{i=1}^N t_{i,k} \right) \frac{{\partial}^2 \log B(\alpha_k, \beta_k)}{\partial {\alpha_k}^2} \nonumber \\
% ------------------------------------------------------------------------------- %
\frac{{\partial}^2 L}{\partial{\beta_k}^2} & = & - \left( \sum_{i=1}^N t_{i,k} \right) \frac{{\partial}^2 \log B(\alpha_k, \beta_k)}{\partial {\beta_k}^2}  \nonumber \\ \nonumber
% ------------------------------------------------------------------------------- %
\end{array}
\right.
\]

En utilisant la relation précédente et en notant $\psi_1$ la fonction trigamma, on obtient :

\[
\left\{
\begin{array}{r c l}
% ------------------------------------------------------------------------------- %
\frac{{\partial}^2L}{\partial{\alpha_k}^2} & = & - \left( \sum_{i=1}^N t_{i,k} \right) \left[ \psi_1(\alpha_k) - \psi_1(\alpha_k + \beta_k) \right] < 0 \\
% ------------------------------------------------------------------------------- %
\frac{{\partial}^2L}{\partial{\beta_k}^2} & = & - \left( \sum_{i=1}^N t_{i,k} \right) \left[ \psi_1(\beta_k) - \psi_1(\alpha_k + \beta_k) \right] < 0  \\
% ------------------------------------------------------------------------------- %
\end{array}
\right.
\]

En effet, par décroissante de la fonction trigamma, on obtient : $\psi_1(\alpha) - \psi_1(\alpha + \beta) > 0$.

	\subsubsection{Extensions au cas multidimensionnel}

Comme pour la distribution gaussienne, on étend sans difficulté ces résultats à des vecteurs de loi Bêta en supposant une nouvelle fois que les lois sont indépendantes suivant les dimensions.

\begin{equation}
L = \sum_{i=1}^{n} \sum_{k=1}^{K} \left( t^m_{ik}.\ln \left[ p_k . \prod_{j=1}^{d}\frac{x_i^{\alpha_k^j-1}(1-{x_i^j}^{\beta_k^j-1})}{B(\alpha_k^j,\beta_k^j)}\right] \right)
\end{equation}

\begin{equation}
L = \sum_{i=1}^{n} \sum_{k=1}^{K} t^m_{ik}.[\ln{p_k}+\sum_{j=1}^{d}+(\alpha_k^j-1)\ln{(x_i^j)} + (\beta_k^j-1)\ln{1-x_i^j}-ln{(B(\alpha_k^j,\beta_k^j)}]
\end{equation}

Pour les mêmes raisons que précédemment, on obtient :

\[
\left \{
\begin{array}{r c l}
% ------------------------------------------------------------------------------- %
\left( \psi(\alpha_k^j) - \psi(\alpha_k^j + \beta_k^j) \right) & = & \frac{1}{\sum_{i=1}^n t_{i, k}^m} . \sum_{i=1}^n t_{i,k}^{m} ln \left( x_i^j \right) \nonumber\\
% ------------------------------------------------------------------------------- %
\nonumber \\
% ------------------------------------------------------------------------------- %
\left( \psi(\beta_k^j) - \psi(\alpha_k^j + \beta_k^j) \right) & = & \frac{1}{\sum_{i=1}^n t_{i, k}^m} . \sum_{i=1}^n t_{i,k}^{m} ln \left( 1-x_i^j \right) \nonumber
% ------------------------------------------------------------------------------- %
\end{array}
\right.{, \forall k \in \left[1, K \right] , \forall j \in \left[1, d \right]}
\]




\subsection{Mélange Gamma}

La distribution Gamma est donnée par deux paramètres : un paramètre de forme, $k$ et un paramètre d'échelle,  $\beta$ : 
\begin{center}
$g \left( x, \alpha, \beta \right) = \frac{x^{\alpha-1} e^{-\frac{x}{\beta}}}{\Gamma(\alpha) \left( \beta \right)^{\alpha}}$
\end{center}


\subsubsection{M step of the univariate model}

\subsubsection{M step of the multivariate model}



\section{Résultats}

\subsection{Visualisation}

\paragraph{}
La visualisation de résultats de classification en grande dimension (supérieure à 2) et notamment au niveau de la représentation des frontières. Nous adopterons dans ce mémoire le point de vu de [?], qui consiste à représenter dans une matrice carrée de la taille de la dimensions du problème, les caractéristiques suivante :
\begin{itemize}
	\item Sur la diagonale les densités à postériori estimée, sur une seule dimension.
	\item Ailleurs une représentation en deux dimensions des données.
\end{itemize}

\paragraph{}
Bien que pratique et suffisante ici, cette visualisation ne permet toutefois pas de totalement caractériser des variables corrélées suivant les dimensions.

\subsection{Mélange Gaussien}

\paragraph*{}
Quelques résultats sont disponible figure~(\ref{fig_gaussMix1}) page~\pageref{fig_gaussMix1}et figure~\ref{fig_gaussMix2} page~\pageref{fig_gaussMix2}


\begin{figure}
	\center
	\includegraphics{img/part1/2DSimple.png}
	\caption{\label{fig_gaussMix1} classification for simple gaussian distribution}
	\label{Référence}
\end{figure}


\begin{figure}
	\center
	\includegraphics[scale=0.5]{img/part1/Gmix3D.png}
	\caption{\label{fig_gaussMix2} Results in 3 dimensions for a Gaussian distribution.}
	\label{Référence}
\end{figure}


\paragraph{}
Appliquons également ce résultats au jeu de données Iris, qui peut être vu comme un problématique de classification à trois classes et cinq dimensions. La matrice de confusions est visible : figure~\ref{fig_gaussMix3} page~\pageref{fig_gaussMix3}.

\begin{figure}
	\center
	\includegraphics[scale=1.5]{img/part1/confusMatGIris.png}
	\caption{\label{fig_gaussMix3} Confusion matrix for the Iris dataset.}
	\label{Référence}
\end{figure}

\clearpage


\subsection{Mélange Béta}

\paragraph*{}
On adopte le même schéma d'étude : d'abord sur un jeu de données artificiel et simple (figure~\ref{fig_betaMix1} page~\pageref{fig_betaMix1}) puis sur le jeu de données Iris.

\begin{figure}
	\center
	\includegraphics[scale=0.5]{img/part1/bMix3D.png}
	\caption{\label{fig_betaMix1} results of estimations of a two components three dimensional beta mixture.}
	\label{Référence}
\end{figure}






\section{Annexe : résolution de système d'équation}

\subsection{La méthode de Brent}

d'après d'après Brent, R. P. (1973) \cite{Brent1973}.
\paragraph*{}
La méthode de Brent est un algorithme de recherche des zéros d'une fonctions.  Elle est issue de l'algorithme de Théoduros Dekker (1969), auquel Richard Brent a apporté une légère modification en 1973.

\paragraph*{}
Dans cette section, on cherchera à résoudre l'équation suivante, avec la précision $\epsilon$
\begin{eqnarray}
f(x) = 0 \nonumber
\end{eqnarray}
où $f$ est supposée être continue sur $\left[ a,b \right]$ tel que $f(a) f(b) <0$. Par hypothèse de continuité, on sait d'après le théorème des valeurs intermédiaires que la solution de cette équation existe.Si de plus $f$ est monotone sur cet intervalle, alors cette solution est unique. On fera cette hypothèse simplificatrice dans ce paragraphe.

\subsection{La méthode de Dekker}

\paragraph*{}
L'idée de l'algorithme est d'utiliser successivement les méthodes de la sécante et de dichotomie, suivant leur efficacité.

\paragraph*{}
A chaque itération, on dispose de trois points :
\begin{itemize}
	\item $b_n$ la solution courante
	\item $a_n$ le point tel que $f(a_n) f(b_n) <0$ et $|f(b_n)| < |f(a_n)|$
	\item $b_{n-1}$ la solution de l'itération précédente. Par convention $b_{-1} = a_{0}$ même si la deuxième condition n'est pas vérifiée.
\end{itemize}

\paragraph*{}
On calcule ensuite les deux candidats à la solution de l'itération suivante : \\

\begin{minipage}[c]{0.50\linewidth}
\begin{center}
$s = b_n - \frac{b_n-b_{n-1}}{f(b_n)-f(b_{n-1})} f(b_n)$ \\ \textbf{} \\
par la méthode de la sécante
\end{center}
\end{minipage} \hfill
\begin{minipage}[c]{0.50\linewidth}
\begin{center}
$\frac{a_n + b_n}{2}$ \\ \textbf{} \\
par dichotomie
\end{center}
\end{minipage}

\paragraph*{}
Si le résultat de la méthode de la sécante, $s$, tombe entre $b_n$ et $m$, alors il devient la nouvelle solution courante. Dans le cas contraire, on choisit le point issus de la dichotomie.

\paragraph*{}
Pour la mise à jour du nouveau contrepoint, on procède de la manière suivante :
\begin{itemize}
	\item Si $f(b_{n+1})$ et $f(a_n)$ sont de signe opposé, alors $a_{n+1} = a_n$
	\item Sinon $a_{n+1} = b_n$ 
\end{itemize}

\paragraph*{}
Ensuite, si $|f(a_{n+1})| < |f(b_{n+1})|$, alors $a_{n+1}$ est une meilleure approximation de la solution que $b_{n+1}$.On échange donc leur valeur.

\paragraph*{}
On répète enfin cette opération jusqu'à la précision souhaitée. On est assurée de la convergence puisque la suite des $b_n$ est encadrée par une suite construite à partir des résultats de la méthode de dichotomie, qui elle est convergente.

\subsection{Les modifications de Brent}

Il peut arriver que la méthode de Dekker converge particulièrement lentement, et en particulier nécessiter plus d'itération que la méthode de dichotomie. Brent propose alors une seconde condition à vérifier pour pour choisir $s$ lors de la nouvelle itération :
\begin{itemize}
	\item Si l'étape précédente utilisait la dichotomie alors on doit avoir $|s-b_n| < \frac{1}{2} |b_n-b_{n-1}|$
	\item Si l'étape précédente utilisait la méthode de la sécante, alors on doit avoir $|s-b_n| < \frac{1}{2} |b_{n-1}-b_{n-2}|$
\end{itemize}

\paragraph*{}
On doit alors retenir le résultats des trois itérations précédentes, mais on voit ainsi dans la deuxième proposition que la méthode de la sécante n'est plus retenue lorsqu'elle ne revient qu'à un rallongement de la dichotomie.\\
Brent a prouvé que dans ce cas, on converge au plus en $N^2$ itérations, où $N$ est le nombre d'itération nécessaire à la dichotomie.

\paragraph*{}
Prent propose également d'utiliser l'interpolation quadratique inverse plutôt que l'interpolation linéaire dans la méthode de la sécante si $f(a_n), f(b_n), f(b_{n-1)}$ sont distincts.

\paragraph*{}
Pour rappel, l'interpolation quadratique inverse consiste à approcher non plus la fonction par sa tangente, mais par une fonction quadratique par l'interpolation lagrangienne. Elle nécessite donc trois points distincts, et est définie ici par :
\begin{eqnarray}
b_{n+1} = \frac{f(a_{n})f(b_n)}{(f(b_{n-1})-f(a_{n}))(f(b_{n-1}-f(b_n))} b_{n-1} + \frac{f(b_{n-1})f(b_n)}{(f(a_{n})-f(b_{n-1}))(f(a_{n})-f(b_n))} a_{n} \nonumber
\end{eqnarray}

\paragraph*{}
De plus, dans le cas où l'interpolation quadratique inverse a été retenue à l'itération précédente, l'expression de $b_n$ est différente de celle issue de la l'interpolation linéaire. L'expression de la nouvelle condition change elle aussi :

\begin{itemize}
	\item Si l'étape précédente utilisait la dichotomie alors on doit avoir $|s-b_n| < \frac{1}{2} |b_n-b_{n-1}|$
	\item Si l'étape précédente utilisait la méthode de la sécante, alors on doit avoir $|s-b_n| < \frac{1}{4} |3a_{n}-b_{n}|$
\end{itemize}

%
%\subsection{Résolution d'un système d'équation}
%On va maintenant chercher à résoudre le système non linéaire suivant :
%\[
%\left \{
%\begin{array}{r c l}
% ------------------------------------------------------------------------------- %
%f_1(x,y) = 0 \\
%f_2(x, y) = 0
% ------------------------------------------------------------------------------- %
%\end{array}
%\right.{, \forall k \in \left[1, K \right] , \forall j \in \left[1, d ù\right]}
%\]

%L'algorithme proposé ici n'est pas propre à la méthode de Brent. Il consiste à maximiser le système successivement suivant chacune des variables :

%\begin{algorithm}
%\caption{calculer $x$ et $y$ avec la précision $\epsilon$}
%\begin{algorithmic} 
%\REQUIRE $\epsilon, x, y$
%\STATE $x \leftarrow x_0$
%\STATE $y \leftarrow y_0$
%\WHILE{$|f_1(x, y)| > \epsilon$ and $|f_2(x, y)| > \epsilon$}
%\STATE fixer $y$ et résoudre la première équation avec la méthode de Brent
%\STATE fixer $x$ et résoudre la seconde équation avec la méthode de Brent
%\ENDWHILE
%\RETURN $x, y$
%\end{algorithmic}
%\end{algorithm}

\subsection{Implémentation numérique}

Plusieurs implémentation de la méthode de Brent existent déjà. Nous avons avons utilisé dans ce mémoire :
\begin{itemize}
	\item La fonction $fsolve$ de la toolBox optimisation de Matlab,
	\item La fonction $optimize$ sur R
	\item La fonction $?$ de la bibliothèque stk++.
\end{itemize}

\clearpage
