%%
% http://stackoverflow.com/questions/7715138/using-precomputed-kernels-with-libsvm
addpath('C:\Users\Cl�ment\Desktop\M�moire Gaussian Mixture\Projet R final\matlabCode\libsvm-3.18\matlab')

%% sur prot�ine

data = textread('sm.co');
class = textread('sm.eti');
ind = 1:98;
data = data(ind, 2:end);
class = class(ind, 2);
num = size(data,1);

sigma = 1;%100;%2e-3;
simProt = @(X, Y) 2 * ( 1 - sum(X==Y) / size(X, 2));
protKernelExp = @(X,Y) exp(-sigma .* simProt(X,Y).^2);

x1 = data(1, :);
x2 = data(2, :);

%# (test,train) instances and include sample serial number as first column
K = diag(num-1, num-1);
for i=1:num
    for j=1:num
       if j >= i
          K(i, j) =  protKernelExp(data(i, :), data(j, :));
       else
           K(i, j) = K(j, i);
       end
    end
end

K =  [ (1:num)' , K ];

% -t 4 is for precomputed kernel
model = svmtrain(class, K, '-t 4');
[predClass, acc, decVals] = svmpredict(class, K, model);

confusionmat(class, predClass)

%% kmean experiment
if size(K, 1)+1 == size(K, 2)
   K = K(:, 2:end); 
end
[label, energy] = knkmeans(K, 8);

for i=0:8
   indc = find(class == i);
   label(1, indc)
end