data = textread('sm.co');
class = textread('sm.eti');
ind = 1:98;
data = data(ind, 2:end);
class = class(ind, 2);
num = size(data,1);

distProt = @(X1, X2) 2 * ( 1 - sum(X1==X2) / size(X1, 2));
distProt2 = @(X1, X2) arrayfun(@(i)distProt(X1, X2(i, :)), 1:size(X2, 1));

%%
T = clusterdata(data, 'maxclust',2, 
%%
Y = pdist(data, distProt2);
Z = linkage(Y);
d = dendrogram(Z);