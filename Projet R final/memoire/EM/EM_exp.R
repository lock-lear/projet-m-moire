# Parameters in list l
# -beta which the scale parameter, such as f(x) = (1/beta) exp(x/beta)
# pi
# N, D, K

specic_logLhd_exp <- function(X, t, l) {
  
  logLikelihood <- log(dmexp(X, l))
  logLikelihood <- sum(logLikelihood)
  
  return(logLikelihood)
}

specic_E_step_exp <- function(X, l) {
  
  K <- l$K
  D <- l$D
  beta <- l$beta
  D <- l$D
  
  N <- dim(X)[1]
  t <- matrix(0, nrow=N, ncol=K)
  
  for(k in seq(1, K)) {
    t[, k] = l$pi[k] * dmexp(X, list(beta=as.matrix(beta[, k], ncol=1), pi=c(1), D=D, K=1))
  }
  
  return(t)
}

specic_M_step_exp <- function(t, X, l) {
  
  D <- l$D
  K <- l$K
  N <- dim(t)[1]
  
  moment <- computeMoment(X, t)
  
  beta <- moment$mu
  pi <- moment$pi  
  
  pi <- pi / N
  l$beta = beta
  l$pi = pi
  return(l)
}

# transform data from I to [0+epsilon, +Inf]
reshape_exp <- function(X, epsilon = 0.01) {
  
  min = min(X)
  if (min < 0)
    return(X - min + abs(epsilon))
  else
    return(X)
}


verifyShape_exp <- function(X, l) {

  beta <- l$beta
  p <- l$pi
  
  if(!is.matrix(beta))
    stop("Please provide a D*K matrix for beta_hat")

  if (dim(beta)[1] != l$D || dim(beta)[2] != l$K)
    stop("Please provide a D*K matrix for beta_hat")
  
  if(!is.vector(p) || length(p) != K)
    stop("Please provide a K vector for p_hat")
}