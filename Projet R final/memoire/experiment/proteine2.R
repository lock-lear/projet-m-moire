# Lecture des donnees -----
nucreceptEt <- read.table("data/nucrecept.eti")
nucreceptCo <- read.table("data/nucrecept.co")


# Test sigma -----
tprot2 <- matrix(0, nrow=dim(nucreceptCo)[1], ncol=16)
for (eti in seq(0, 15))
  tprot2[nucreceptEt[, 2] == eti, eti+1] = 1

plotTestSigma(c(0.1, 1, 10, 100, 500, 1000), nucreceptCo[, -1], tprot2, dotProduct=simProt)

# Sur toutes les classes -----
res <- KEM(smCo[, -1], "exp", kernel=GaussianGrammMatrix, argKer=list(sigma=500, dotProduct=dotProductProt), K=5, 100, niter_init=1000, display=TRUE, printInfo=TRUE)
kplot(res, smCo[,- 1])

# Sur 2 class
indSemiProt <- nucreceptEt[, 2] == 0 | nucreceptEt[, 2] == 1
set.seed(11) # juste to be sure :)
res <- KEM(nucreceptCo[indSemiProt, -1], "kmean", kernel=GaussianGrammMatrix, argKer=list(sigma=1, dotProduct=dotProductProt), K=2, 1, niter_init=5, display=TRUE, printInfo=TRUE)
kplot(res, nucreceptCo[indSemiProt,- 1])

# Sur 3 class
indSemiProt3K <- smEt[, 2] == 2 | smEt[, 2] == 4 | smEt[, 2] == 5
res <- KEM(smCo[indSemiProt3K, -1], "kmean", kernel=GaussianGrammMatrix, argKer=list(sigma=1000, dotProduct=dotProductProt), K=3, 10, niter_init=10, display=TRUE, printInfo=TRUE)
kplot(res, smCo[indSemiProt3K,- 1])



simProt <- function(X1, X2) {
  
  return( sum( (X1 - X2) == 0 ) / length(X1))
}

dotProductProt <- function(X1, X2) {
  
  2 - 2 * simProt(X1, X2)
}