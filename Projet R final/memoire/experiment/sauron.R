# Classification-----
sauron <- loadSauron(150, 200)
set.seed(10) # juste to be sure :)

l <- KEM(sauron[, c(1, 2)], "exp", kernel=GaussianGrammMatrix, argKer=list(sigma=10), K=2, 400, niter_init=1, display=TRUE, printInfo=TRUE)
kplot(l, sauron[, c(1, 2)])

l <- KEM(sauron[, c(1, 2)], "kmean", kernel=GaussianGrammMatrix, argKer=list(sigma=75), K=2, 400, niter_init=1, display=TRUE, printInfo=TRUE)
kplot(l, sauron[, c(1, 2)])


# --------------
tsauron <- matrix(0, nrow=dim(sauron)[1], ncol=2)
tsauron[seq(1, 150), 1]    = 1
tsauron[seq(151, 350), 2]  = 1
plotTestSigma(c(0.1, 1, 10, 50, 100, 1000), sauron, tsauron)
plotTestSigma(c(1, 10, 15, 20, 25), sauron, tsauron)

# Construction dataset -----------------
loadSauron <- function(nb1, nb2) {
  
  set.seed(1) # juste to be sure :)
  sauron <- as.data.frame(matrix(0, ncol=2, nrow=nb1 + nb2))
  sauron["outcome"] = 0 * seq(1, nb1 + nb2)
  centrum <- seq(1, 4)
  
  ind1 <- seq(1, nb1)
  ind2 <- seq(nb1 + 1, nb1 + nb2)
  
  sauron[ind1, 1]  = rnorm(length(ind1), 0, 0.05)
  sauron[ind1, 2]  = rnorm(length(ind1), 0, 0.3)
  
  for (i in ind2) {
    
    sauron[i, 1] = 3 * cos((i-125) * 2 *pi / 125)  + rnorm(1, 0, 0.05)
    sauron[i, 2] = 3 * sin((i-125) * 2 *pi / 125)  + rnorm(1, 0, 0.05)
  }
  
  sauron$outcome[ind2] = 1 
  sauron$outcome = as.character(sauron$outcome)
  print(qplot(sauron[, 1], sauron[, 2], data=sauron, color=outcome, xlab="", ylab="") + theme(legend.position = "none"))
  
  return(sauron)
}