rmexp <- function(nb, l) {
  # needeed in l
  #   - K, D
  #   - beta: a D by K matrix, the scale parameter (such as f(x) = (1/beta) exp(x/beta))
  #   - pi: a K vector

  D <- l$D
  K <- l$K
  beta <- l$beta
  pi <- l$pi
  
  X <- matrix(0, nrow=nb,ncol=D)
  ind <- sample(seq(1, K), nb, replace=T, prob=pi)
  
  for (d in seq(1, D))
    X[, d] <- mapply(function(k){ return(rexp(1, rate=1/beta[d, k]) )}, ind)
  
  return(X)
}

dmexp <- function(x, l) {
  # needeed in l
  #   - K, D
  #   - beta: a D by K matrix, the scale parameter (such as f(x) = (1/beta) exp(x/beta))
  #   - pi: a K vector
  
  if (!is.matrix(x) & !is.data.frame(x))
    stop("Please use matrix or data frame for pdf")
  
  N <-dim(x)[1]
  K     <- l$K
  D     <- l$D
  beta     <- l$beta
  pi     <- l$pi
  
  P <- matrix(1, nrow=N, ncol=K)

  for (k in seq(1, K)) {
    for (d in seq(1, D))
      P[, k] <- P[, k] * dexp(x[, d], rate=1/beta[d, k])
  }
  
  res = t(pi %*% t(P))
  
  return(res)
}