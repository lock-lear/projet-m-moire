require(Rcpp)


source(paste(getwd(), "/kernelEM/steps/exp.R", sep=""))
source(paste(getwd(), "/kernelEM/steps/kmean.R", sep=""))
source(paste(getwd(), "/kernelEM/gramm.R", sep=""))
source(paste(getwd(), "/kernelEM/kplot.R", sep=""))

sourceCpp(paste(getwd(), "/kernelEM/Ctools.cpp", sep=""))

KEM <- function (X, distribution, kernel, argKer=NULL, K=NULL, param_convergence, niter_init=300, display=TRUE, printInfo=TRUE) {
  
  niter <- param_convergence
  
  # Is the distribution covered by the package?
  if (! distribution %in% c("exp", "kmean"))
    stop("Distribution is not recognized")
  
  # Is X a dataframe?
  if(!is.data.frame(X))
    stop("Error : X must be dataframe")
  
  # Initialisation
  l     <- NULL
  funcs <- NULL
  N     <- dim(X)[1]
  D     <- 1
  
  funcs <- getKListFunc(distribution, printInfo)
  
  if(is.null(K)) {
    stop("Rewrite the BIC calling KEM function")
  }
  else {
    gramm <- kernel(X, argKer) #GaussianGrammMatrix
    l <- list(distrib=distribution, kernel=kernel, argKer=argKer, N=N, D=D, K=K, gramm=gramm, t=matrix(0, nrow=N, ncol=K), CICRV=funcs$CICRV)
  }
  
  if (printInfo) print("debut initialisation")
  t1 <- Sys.time()
  
  l <- Kinitialise(X, l, funcs, 1)
  if (display) llk_init <- l$vecllkinit
  
  l <- fKSEM(X, l, listFunc=funcs, niter=niter_init, printInfo=printInfo)
  if (display) llk_sem <- l$vecllkinit_sem
  
  if (printInfo) print(paste("fin initialisation,", Sys.time() - t1))
  
  n=1
  
  if (printInfo) print("debut KEM")
  t1 <- Sys.time()
  
  t <- l$t
  Y <- cComputeY(l$gramm, t)
  vecLlk <- seq(0, 0, length.out=niter)
  
  logLikelihood <- KlogLikelihood(Y, l, funcs)
  l_buf <- l
  while(n <= niter) {
    # E step
    t <- KE_step(Y, l_buf, funcs)
    Y <- cComputeY(l$gramm, t)
    
    # M step
    l_buf <- KM_step(Y, t, l_buf, funcs)
    
    # compute llk
    new_ll <- KlogLikelihood(Y, l_buf, funcs)
    
    #if (new_ll > logLikelihood) {
      l <- l_buf
      logLikelihood <- new_ll
    #}
    vecLlk[n] = logLikelihood
    
    # break condition
    n <- n+1
  } 
  if (display) {
    x <- seq(1, length(llk_init) + length(llk_sem) + length(vecLlk))
    class <- seq(1, length(llk_init) + length(llk_sem) + length(vecLlk))
    class[seq(1, length(llk_init))] = 1
    class[seq(length(llk_init)+1, length(llk_init) + length(llk_sem))] = 2
    class[seq(length(llk_init) + length(llk_sem)+1, length(llk_init) + length(llk_sem) + length(vecLlk))] = 3
    class <- mapply(as.character, class)
    
    print(qplot(x, c(llk_init, llk_sem, vecLlk), color=class))
  }
  
  l <- c(l, list(llk=vecLlk[niter]))
  
  if (printInfo) print("fin KEM")
  if (printInfo) print(Sys.time() - t1) 
  
  return(l)
}

KlogLikelihood <- function(Y, l, listFunc) {
  
  llk <- listFunc$lkh(Y, l$t, l)
  return(llk)
}


Kinitialise <- function(X, l, listFunc, nbRepet) {
  
  N <- l$N
  K <- l$K
  gramm <- l$gramm
  
  t <- matrix(runif(N*K), nrow=N, ncol=K)
  t <- t / rowSums(t)
  
  Y <- cComputeY(gramm, t)
  l <- KM_step(Y, t, l, listFunc)
  l$t = t
  logLikelihood <- listFunc$lkh(Y, t, l)
  
  vecllk = seq(0, 0, length.out=nbRepet)
  vecllk[1] = logLikelihood
  for (n in seq(1, nbRepet)) {
    t <- matrix(runif(N*K), nrow=N, ncol=K)
    t <- t / rowSums(t)
    Y <- cComputeY(gramm, t)
    
    l_buf <- KM_step(Y, t, l, listFunc)
    buf_LogLikelihood <- listFunc$lkh(Y, t, l_buf)
    l_buf$t = t
    vecllk[n] <- buf_LogLikelihood
    
    if (buf_LogLikelihood > logLikelihood) {
      l <- l_buf
      logLikelihood <- buf_LogLikelihood
    }
  }
  
  return(c(l, list(vecllkinit=vecllk)))
};

fKSEM <- function (X, l, listFunc, niter, printInfo=TRUE) {
  # shortcut for the SEM algorithm
  # (not supposed to be exported)
  
  N <- l$N
  K <- l$K
  gramm <- l$gramm
  
  t <- l$t
  Y <- cComputeY(gramm, t)  
  
  # Initialisation
  l_buf <- l
  logLikelihood <- KlogLikelihood(Y, l, listFunc)
  
  # Estimation part
  if (printInfo) print('début algorithme SEM')
  vecLlk <- seq(0, 0, length.out=niter)
  vecLlk[1] = logLikelihood
  n=1
  while(n < niter) {
    # E step
    t = KE_step(Y, l, listFunc)
    Y <- cComputeY(gramm, t)
    
    # S step
    t <- S_step(t)
    t <- cleanEstep(t)
    
    # M step
    l_buf <- KM_step(Y, t, l, listFunc)
    l_buf$t = t

    # performing new modèle
    buf_logLikelihood <- listFunc$lkh(Y, t, l_buf)
    vecLlk[n+1] = buf_logLikelihood
    if (logLikelihood < buf_logLikelihood) {
      logLikelihood <- buf_logLikelihood
      l <- l_buf
    }
    
    # break condition
    n = n+1
  }
  
  # End
  if (printInfo) print('fin algorithme SEM')
  return(c(l, list(vecllkinit_sem=vecLlk)))
}

KE_step <- function(Y, l, listFunc) {
  
  N   <- dim(Y)[1]
  D   <- l$D
  K   <- l$K
  pi  <- l$pi
  pdf <- listFunc$mixtPdf

  t <- matrix(0, nrow=N, ncol=l$K)
  for (k in seq(1, K)) {

    if (l$distrib == "exp") # Pour eviter pour l instant une fonction ailleurs
      ltmp <- list(beta=as.matrix(l$beta[1, k], ncol=1), pi=c(1), D=D, K=1)
    else if (l$distrib == "kmean") # Pour eviter pour l instant une fonction ailleurs
      ltmp <- list(mu=as.matrix(l$mu[1, k], ncol=1), sig=sqrt(array(l$var[1, k], c(1, 1, 1))), pi=c(1), D=1, K=1)
    
    t[, k] = pi[k] * pdf(as.matrix(Y[, k], ncol=1), ltmp)
    
    #else if (l$distrib == "ray") # Pour eviter pour l instant une fonction ailleurs
    #  t[, k] = pi[k] * pdf(as.matrix(Y[, k], ncol=1), list(sigma=as.matrix(l$sigma[1, k], ncol=1), pi=c(1), D=D, K=1))
  }

  total <- rowSums(t)
  for(k in seq(1, l$K)) {
    t[, k] = t[, k] / total
  }
  #print(t)
  t <- cleanEstep(t)
  return(t)
}

KM_step <- function(Y, t, l, listFunc) {
  
  l = listFunc$M_step(t, Y, l)
  l$t = t
  
  return(l) 
}

getKListFunc <- function(distribution, printInfo=FALSE) {
  
  funcs <- NULL; 
  switch(distribution,

         exp={
           if (printInfo) print("Debut KEM exp")
           funcs <- list(lkh=specic_logKLhd_exp, M_step=specic_KM_step_exp, mixtPdf=dmexp, CICRV=specificCIRCV_exp)
         },
         kmean={
           if (printInfo) print("Debut KEM kmean")
           funcs <- list(lkh=specic_logKLhd_kmean, M_step=specic_KM_step_kmean, mixtPdf=dmnorm, CICRV=specificCIRCV_kmean)
         },
         #ray={
         #  if (printInfo) print("Debut EM rayleigh")
         #  funcs <- list(lkh=specic_logLhd_ray, M_step=specic_M_step_ray, mixtPdf=dmray)
         #},
         stop("Enter something that switches me!")
  )
  
  return(funcs)
}

Kclassify <- function(l, X) {
  
  if (!is.matrix(X) & !is.data.frame(X)) {
    stop("Please provide a matrix or a data frame for X")
  }
  p <- l$pi 
  K <- l$K
  t_model = l$t
  gramm <- l$gramm
  
  N <- dim(X)[1]
  
  funcs <- getKListFunc(l$distrib)
    # En theorie il faudrait distinguer dans ce calcul l echantillon dont on souhaite calculer Y
    # et l echantillon model qui sert de reference pour ce calcul (pour les gaussienne calculer les centroids)
    # ce n est pas necessaire pour l instant car les echantillon d apprentissage et de tests sont les memes pour
    # l instant
  Y <- cComputeY(gramm, t_model)
  t <- KE_step(Y, l, funcs)
  
  result <- matrix(0, nrow=N, ncol=K+1)
  result[, 1] = attributeClass(t)
  result[, seq(2, K+1)] = t
  
  names =  "attribution"
  for(k in 1:dim(t)[2])
    names = c(names, paste("classe", k, sep = "_"))
  colnames(result) = names
  
  return(result)
};