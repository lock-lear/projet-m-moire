#

  ## Compute the gramm matrix of a 
GaussianGrammMatrix <- function(X, listArg) {
  
  N <- dim(X)[1]
  
  sigma       <- listArg$sigma
  dotProduct  <- listArg$dotProduct
  
  if (is.null(dotProduct))
    dotProduct = function(X1, X2) {sum((X1-X2)^2)}
    
  X <- as.matrix(X)
  gramm <- diag(N)
  
  for (i in seq(1, N)) {
    for (j in seq(1, N)) {
      
      if (j >= i)
        gramm[i, j] = exp( -dotProduct(X[i, ], X[j, ]) / sigma )
      
      else 
        gramm[i, j] = gramm[j, i]
    }
  }
  
  return(gramm)
}

plotTestSigma <- function(sigmaRange, X, t, dotProduct=function(X1, X2) {sum((X1-X2)^2)}) {
  
  grid.newpage()
  
  nbCol = ceiling(sqrt(length(sigmaRange))) # +1 for legend
  nbRow = floor(sqrt(length(sigmaRange)))
  
  indRow = rep(seq(1, nbRow), each=nbCol)
  
  pushViewport(viewport(layout = grid.layout(nbRow, nbCol)))
  i = 0
  for (sig in sigmaRange) {
    
    Y <- cComputeY(GaussianGrammMatrix(X[, -3], list(sigma=sig, dotProduct=dotProduct)), t)
    
    g = plotHistY(Y, i!=0)
    print(g + ggtitle(paste("sigma=", sig)), vp = viewport(layout.pos.row = indRow[i+1], layout.pos.col = i %% nbCol + 1))
    
    i = i +1
  }
}


plotTestCompleteDistribSigma <- function(sigmaRange, X, t, dotProduct=function(X1, X2) {sum((X1-X2)^2)}) {
  
  pi <- colSums(t)
  N <- dim(X)[1]
  
  nbCol = ceiling(sqrt(length(sigmaRange))) # +1 for legend
  nbRow = floor(sqrt(length(sigmaRange)))
  
  indRow = rep(seq(1, nbRow), each=nbCol)
  
  pushViewport(viewport(layout = grid.layout(nbRow, nbCol)))
  i = 0
  for (sig in sigmaRange) {

    Y <- cComputeY(GaussianGrammMatrix(X[, -3], list(sigma=sig, dotProduct=dotProduct)), t)
  
    df <- data.frame(cond = factor( rep(1, each=N) ), Y = rowSums(pi * Y))
    
    g = ggplot(df, aes(x=Y, fill=cond)) + geom_density(alpha=.3) + theme(legend.position = "none")
    print(g + ggtitle(paste("sigma=", sig)), vp = viewport(layout.pos.row = indRow[i+1], layout.pos.col = i %% nbCol + 1))
    
    i = i +1
  }
}


g_legend<-function(a.gplot){ 
  tmp <- ggplot_gtable(ggplot_build(a.gplot)) 
  leg <- which(sapply(tmp$grobs, function(x) x$name) == "guide-box") 
  print(tmp$grobs)
  legend <- tmp$grobs[["leg"]] 
  return(legend)
} 

## Compute the gramm matrix of a 
GaussianGrammMatrixMinMaxAdaptativeBandwith <- function(X, listArg) {
  
  N <- dim(X)[1]
  sigma <- listArg$sigma
  X <- as.matrix(X)
  gramm <- diag(N)
  
  for (i in seq(1, N)) {
    for (j in seq(1, N)) {
      
      if (j >= i) {
        gramm[i, j] = ckGauss(X[i, ], X[j, ], sigma)
      }
      
      else 
        gramm[i, j] = gramm[j, i]
    }
  }
  
  return(gramm)
}