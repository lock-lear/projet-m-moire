\chapter{Implémentation numérique de l'algorithme EM}

Afin de tester et d'illustrer les propos tenus dans ce mémoire, un package R a été développé en parallèle. Nous discuterons donc, dans ce chapitre, qui fait office de bilan à mi parcours, de quelques considérations informatiques qui ne sont pas forcément mises en valeur lors de la construction de l'algorithme. Nous présenterons ensuite quelques résultats.

\section{Problématiques liées à l'implémentation}

\subsection{Initialisation}

\paragraph{}
La construction de l'algorithme \textbf{EM} repose sur l'existence d'une suite croissante de paramètres, qui rendent la vraisemblance du modèle croissante. La suite est implicitement définie par récurrence, c'est à dire que l'itération $n+1$ requiert le résultat de l'itération $n$, afin d'estimer la valeur de la donnée complétée $Z$. 

\paragraph{}
Plusieurs solutions sont envisageables afin d'obtenir une valeur $\theta_0$ du vecteur paramètres:
\begin{itemize}
	\item Tirer aléatoirement des valeurs pour les différents paramètres. Il faut toutefois réfléchir au type de loi à utiliser, à respecter le domaine de définition des paramètres et surtout rester cohérents vis-à-vis des facteurs d'échelle.
	
	\item Tirer aléatoirement des valeurs concernant les probabilités conditionnelles $\left(t_{i, k}\right)_{n, k}$. Ce peut être vu que un tirage aléatoire des paramètres. On évite toutefois les risques de sortir du domaine (restreint à$\left[0, 1\right]$) et le type de variable aléatoire utilisée peut s'interpréter comme un a priori sur la répartition des points. Par exemple, une loi uniforme peut s'interpréter comme une non connaissance de la répartition, tandis que certaines lois favorisant les extrêmes (loi bêta) vont plutôt encourager une séparation stricte du jeu de données.
	
	\item Tirer aléatoirement les valeurs de la donnée complétée $Z_0$.
\end{itemize}

\paragraph{}
toutefois, ce type d'initialisation, bien que nécessaire, a peu de chance d'être cohérent. C'est pour cela que l'on peut envisager des stratégies plus élaborées, comme la transformation du contexte en problème de classification non paramétrique. Ainsi on peut par exemple envisager de réaliser quelques itération d'un autre algorithme de partitionnement (par exemple \textit{plus proche voisin}, dit \textit{k-mean}\cite{duda2012pattern}), et obtenir ainsi une estimation de la variable $Z$. Toutefois, l'utilisation de k-mean présuppose une distribution gaussienne de chacune des classes. Cette assertion, bien qu'asymptotiquement vraie pour toutes les distributions considérées dans ce mémoire en vertu du théorème centrale limite, peut toutefois être problématique lorsque la quantité d'observations disponibles est faible.

\paragraph{}
La stratégie finalement envisagée dans le package développée en parallèle de ce mémoire est la suivante:
\begin{itemize}
	\item Plusieurs tirages aléatoires des probabilités conditionnelles suivant une loi uniforme.
	\item A partir de chaque valeur initiale obtenue à partir des tirages précédents, réaliser quelques itérations de l'algorithme SEM, et conserver le paramètre $\theta_{nbIter}$ maximisant la vraisemblance.
	\item Lancer l'algorithme EM à partir de cette valeur du vecteur paramètre.
\end{itemize}

\subsection{Choix du nombre de composantes du mélange, AIC, BIC}

\paragraph{}
Dans le cas où l'on ne connait pas à l'avance le nombre de classe à estimer, on ne peut pas simplement se baser le valeur de la vraisemblance pour évaluer la qualité d'un modèle. En effet, on a de forte chance d'augmenter cette vraisemblance en rajoutant toujours plus de paramètres. Ce principe peut s'apparenter au problème de l'overfitting que l'on peut rencontrer dans d'autres branche de l'analyse statistique, comme la régression. C'est donc en toute logique que l'on mettre en place en place une pénalisation du modèle, tenant compte du nombre de paramètre à estimer.

\paragraph{Le critère AIC}
Akaike propose, en 1974, \textbf{le critère d'information d'Akaike} \cite{akaike1974new} (noté AIC), en se basant sur des travaux issues de la théorie de l'information. Il est donné par:
\begin{equation}
AIC = 2k - 2 \ln(L)
\end{equation}
où : 
\begin{itemize}
	\item $k$ est le nombre de paramètres du modèle.
	\item $L$ est la vraisemblance du modèle.
\end{itemize}

\paragraph{}
En pratique, Akaike montre que si l'on dispose de $m$ modèles candidats $AIC_1...AIC_m$, où $AIC_{min}=\min(AIC_1...AIC_m)$, alors les quantités $\exp^{\frac{AIC_{min}-AIC_i}{2}}$ peuvent s'interpréter comme la probabilité que le modèle $i$  minimise la perte d'information parmi les $m$ modèles proposés.

\paragraph{Le critère BIC}
Schwarz propose quand à lui un critère de pénalisation basé sur des arguments bayésiens \cite{schwarz1978estimating} en 1978. Celui-ci s'écrit:
\begin{equation}
AIC = k \cdot \ln(n) - 2 \ln(L)
\end{equation}
où : 
\begin{itemize}
	\item $k$ est le nombre de paramètres du modèle.
	\item $L$ est la vraisemblance du modèle.
	\item $n$ est le nombre d'observation disponible.
\end{itemize}

\paragraph{}
Discuter et comparer les deux critères.

\paragraph{}
Il est important de noter que \textbf{que ces critère ne permettent pas d'évaluer la pertinence du modèle}, c'est à dire ici si le type de loi utilisée est approprié ou non.

\subsection{Correction}

\paragraph{}
Bien que l'on soit assuré de la convergence théorique de l'algorithme \textbf{EM}, son implémentation numérique peut toutefois diverger, et l'une des classes peut se retrouver totalement vide, engendrant des problèmes numériques. Ceci peut provenir du fait que le maximum atteint ne soit que local (et non global), que le nombre de classe ait mal été dimensionné, ou simplement en raison de problème de converge des solveurs dans le cas de résolutions de système d'équation.

\paragraph{}
Dans la mesure où l'on travaille dans ce mémoire sur des modèles paramétriques, on n'envisagera de faire disparaître une classe vide. La stratégie adoptée dans ce cas est de tirer aléatoirement quelques points dans les autres classes afin de les réinjecter dans celle qui a disparu. Cette méthode, qui fait chuter la vraisemblance, peut toutefois être grandement amélioré dans la mesure où le tirage des points se fait ici de manière totalement incohérentes. Nous ne chercherons toutefois pas à l'améliorer.

\section{Résultats}

\subsection{Visualisation}

\paragraph{}
La visualisation de résultats de classification en grande dimension (supérieure à 2) n'est pas toujours évidente. Nous adopterons dans ce mémoire le point de vu qui consiste à représenter dans une matrice carrée de la taille de la dimensions du problème, les caractéristiques suivantes :
\begin{itemize}
	\item Sur la diagonale les densités à postériori et la fonction de densité estimée, sur une seule dimension correspondant au numéro de ligne.
	\item Ailleurs une représentation en deux dimensions des données.
\end{itemize}

\paragraph{}
Bien que pratique et suffisante ici, cette visualisation ne permet toutefois pas de totalement caractériser des variables corrélées suivant les dimensions.

\subsection{Jeux de données artificiels}

\paragraph{Xor}
On commence avec une jeu de données artificiel, \textit{Xor}, de dimension 2. Celui-ci consiste en quatre classes gaussiennes relativement bien séparées. La valeur des paramètres importent peu. Un aperçu du jeu de données est disponible figure~(\ref{fig_xor_plot}) page~\pageref{fig_xor_plot}.

\begin{figure}[!htbp]
	\center
	\includegraphics[scale=0.4]{img/chap_implementation/xor_plot.jpeg}
	\caption{\label{fig_xor_plot} Représentation du jeu de données artificiel Xor, consistant ici en quatre classes dans les mêmes proportions et issues de gaussiennes de variance 0.3 sur les deux dimensions, et de moyenne dans $\left\{1, 3\right\}$.}
	\label{Référence}
\end{figure}


\paragraph{}
Ce jeu de données est intéressant pour mettre en valeur et interpréter les résultats du critère BIC figure~(\ref{fig_xor_bic}) page~\pageref{fig_xor_bic}. On remarque alors que la loi bêta ne parvient à recommander exactement le bon nombre de classe, contrairement à un mélange de lois normales. On remarque également sur ces graphiques que l'échelle de valeur du critère BIC est complètement différentes entre les deux lois. Ainsi, bien que la loi normales soit la plus appropriée, ici, la valeur du BIC est environ $100$ fois plus faible pour la loi bêta. On joint également les résultats issues des classifications. figure~(\ref{fig_xor_plot}) page~\pageref{fig_xor_plot}.

\begin{figure}[!htbp]
\includegraphics[width=6cm]{img/chap_implementation/xor_betabic.jpeg}\hfill
\includegraphics[width=6cm]{img/chap_implementation/xor_normbic.jpeg}
\caption{Critère BIC pour les lois beta et normales}\label{fig_xor_bic} On calcule le critère BIC pour différentes valeurs du nombre de composantes du mélange. On constate que la loi bêta, à gauche ne parvient pas à séparer le modèle tandis que la loi normale, recommande bien de choisir 4 classes.
\end{figure}


\begin{figure}[!htbp]
\includegraphics[width=6cm]{img/chap_implementation/xor_plotbeta2.jpeg}\hfill
\includegraphics[width=6cm]{img/chap_implementation/xor_plotnormal.jpeg}
\caption{Résultats de l'EM pour les lois beta et normales}\label{fig_xor_plot} On présente ici les résultats de classification pour l'algorithme EM, pour la loi bêta (à gauche, 2 classes) et normale (à droite, 4 classes). Tandis que la loi normale parvient à parfaitement séparer les classes, la loi bêta ne réussit à trouver que deux groupes, gauche/droite ou haut/bas en raison de la symétrie du problème. On notera que pour la loi bêta, les données ont dû être transformées pour correspondre à l'ensemble de définition($[0, 1]$).
\end{figure}


\subsection{Jeux de données réels}

\paragraph{Iris}
Bien que le critère BIC recommandent pour une loi normale de différencier quatre classes, la classification en utilisant 3 clusters (dont on connait la constitution) offre 6\% d'erreur pour une loi normale. La matrice de confusion est d'ailleurs disponible table \ref{fig_irismatConfEM}.

\begin{figure}[!htbp]
\includegraphics[width=6cm]{img/chap_implementation/iris_BicNorm.jpeg}\hfill
\includegraphics[width=6cm]{img/chap_implementation/iris_plotnormal4.jpeg}
\caption{Traitement du cas Iris }\label{fig_iris_bic} On calcule le critère BIC pour différentes valeur du nombre de composantes du mélange. Bien que ce critère recommande de choisir 4 classes, on représente sur la figure de droite la partitionnement en utilisant 3 groupes avec un mélange de lois normales.
\end{figure}

\begin{table}[h!]
\begin{tabular}{l|l|c|c|c|} 
		\multicolumn{2}{c}{} & \multicolumn{3}{c}{\textbf{Classe estimée}} \\
		\cline{3-5}
		\multicolumn{2}{c|}{} & Classe 1 & Classe 2 & Classe 3 \\
		\cline{2-5}
		\multirow{3}{*}{\textbf{Classe réelle}} & Classe 1 & 50 & 0 & 0   \\
		\cline{2-5}
		& Classe 2 & 0 & 44 & 6  \\
		\cline{2-5}
		& Classe 3 & 0 & 3 & 47  \\
		 \cline{2-5}
\end{tabular}
\caption{Matrice de confusion Iris}\label{fig_irismatConfEM}
Matrice de confusion pour une classification en utilisant l'algorithme EM pour estimer un mélange de lois normales sur le jeu de données Iris.
\end{table}

\section{Annexe : résolution de système d'équation}

\subsection{La méthode de Brent}

d'après d'après Brent, R. P. (1973) \cite{Brent1973}.
\paragraph*{}
La méthode de Brent est un algorithme de recherche des zéros d'une fonctions.  Elle est issue de l'algorithme de Théoduros Dekker (1969), auquel Richard Brent a apporté une légère modification en 1973.

\paragraph*{}
Dans cette section, on cherchera à résoudre l'équation suivante, avec la précision $\epsilon$
\begin{eqnarray}
f(x) = 0 \nonumber
\end{eqnarray}
où $f$ est supposée être continue sur $\left[ a,b \right]$ tel que $f(a) f(b) <0$. Par hypothèse de continuité, on sait d'après le théorème des valeurs intermédiaires que la solution de cette équation existe.Si de plus $f$ est monotone sur cet intervalle, alors cette solution est unique. On fera cette hypothèse simplificatrice dans ce paragraphe.

\subsection{La méthode de Dekker}

\paragraph*{}
L'idée de l'algorithme est d'utiliser successivement les méthodes de la sécante et de dichotomie, suivant leur efficacité.

\paragraph*{}
A chaque itération, on dispose de trois points :
\begin{itemize}
	\item $b_n$ la solution courante
	\item $a_n$ le point tel que $f(a_n) f(b_n) <0$ et $|f(b_n)| < |f(a_n)|$
	\item $b_{n-1}$ la solution de l'itération précédente. Par convention $b_{-1} = a_{0}$ même si la deuxième condition n'est pas vérifiée.
\end{itemize}

\paragraph*{}
On calcule ensuite les deux candidats à la solution de l'itération suivante : \\

\begin{minipage}[c]{0.50\linewidth}
\begin{center}
$s = b_n - \frac{b_n-b_{n-1}}{f(b_n)-f(b_{n-1})} f(b_n)$ \\ \textbf{} \\
par la méthode de la sécante
\end{center}
\end{minipage} \hfill
\begin{minipage}[c]{0.50\linewidth}
\begin{center}
$\frac{a_n + b_n}{2}$ \\ \textbf{} \\
par dichotomie
\end{center}
\end{minipage}

\paragraph*{}
Si le résultat de la méthode de la sécante, $s$, tombe entre $b_n$ et $m$, alors il devient la nouvelle solution courante. Dans le cas contraire, on choisit le point issus de la dichotomie.

\paragraph*{}
Pour la mise à jour du nouveau contrepoint, on procède de la manière suivante :
\begin{itemize}
	\item Si $f(b_{n+1})$ et $f(a_n)$ sont de signe opposé, alors $a_{n+1} = a_n$
	\item Sinon $a_{n+1} = b_n$ 
\end{itemize}

\paragraph*{}
Ensuite, si $|f(a_{n+1})| < |f(b_{n+1})|$, alors $a_{n+1}$ est une meilleure approximation de la solution que $b_{n+1}$.On échange donc leur valeur.

\paragraph*{}
On répète enfin cette opération jusqu'à la précision souhaitée. On est assurée de la convergence puisque la suite des $b_n$ est encadrée par une suite construite à partir des résultats de la méthode de dichotomie, qui elle est convergente.

\subsection{Les modifications de Brent}

Il peut arriver que la méthode de Dekker converge particulièrement lentement, et en particulier nécessiter plus d'itération que la méthode de dichotomie. Brent propose alors une seconde condition à vérifier pour pour choisir $s$ lors de la nouvelle itération :
\begin{itemize}
	\item Si l'étape précédente utilisait la dichotomie alors on doit avoir $|s-b_n| < \frac{1}{2} |b_n-b_{n-1}|$
	\item Si l'étape précédente utilisait la méthode de la sécante, alors on doit avoir $|s-b_n| < \frac{1}{2} |b_{n-1}-b_{n-2}|$
\end{itemize}

\paragraph*{}
On doit alors retenir le résultats des trois itérations précédentes, mais on voit ainsi dans la deuxième proposition que la méthode de la sécante n'est plus retenue lorsqu'elle ne revient qu'à un rallongement de la dichotomie.\\
Brent a prouvé que dans ce cas, on converge au plus en $N^2$ itérations, où $N$ est le nombre d'itération nécessaire à la dichotomie.

\paragraph*{}
Prent propose également d'utiliser l'interpolation quadratique inverse plutôt que l'interpolation linéaire dans la méthode de la sécante si $f(a_n), f(b_n), f(b_{n-1)}$ sont distincts.

\paragraph*{}
Pour rappel, l'interpolation quadratique inverse consiste à approcher non plus la fonction par sa tangente, mais par une fonction quadratique par l'interpolation lagrangienne. Elle nécessite donc trois points distincts, et est définie ici par :
\begin{eqnarray}
b_{n+1} = \frac{f(a_{n})f(b_n)}{(f(b_{n-1})-f(a_{n}))(f(b_{n-1}-f(b_n))} b_{n-1} + \frac{f(b_{n-1})f(b_n)}{(f(a_{n})-f(b_{n-1}))(f(a_{n})-f(b_n))} a_{n} \nonumber
\end{eqnarray}

\paragraph*{}
De plus, dans le cas où l'interpolation quadratique inverse a été retenue à l'itération précédente, l'expression de $b_n$ est différente de celle issue de la l'interpolation linéaire. L'expression de la nouvelle condition change elle aussi :

\begin{itemize}
	\item Si l'étape précédente utilisait la dichotomie alors on doit avoir $|s-b_n| < \frac{1}{2} |b_n-b_{n-1}|$
	\item Si l'étape précédente utilisait la méthode de la sécante, alors on doit avoir $|s-b_n| < \frac{1}{4} |3a_{n}-b_{n}|$
\end{itemize}

%
%\subsection{Résolution d'un système d'équation}
%On va maintenant chercher à résoudre le système non linéaire suivant :
%\[
%\left \{
%\begin{array}{r c l}
% ------------------------------------------------------------------------------- %
%f_1(x,y) = 0 \\
%f_2(x, y) = 0
% ------------------------------------------------------------------------------- %
%\end{array}
%\right.{, \forall k \in \left[1, K \right] , \forall j \in \left[1, d ù\right]}
%\]

%L'algorithme proposé ici n'est pas propre à la méthode de Brent. Il consiste à maximiser le système successivement suivant chacune des variables :

%\begin{algorithm}
%\caption{calculer $x$ et $y$ avec la précision $\epsilon$}
%\begin{algorithmic} 
%\REQUIRE $\epsilon, x, y$
%\STATE $x \leftarrow x_0$
%\STATE $y \leftarrow y_0$
%\WHILE{$|f_1(x, y)| > \epsilon$ and $|f_2(x, y)| > \epsilon$}
%\STATE fixer $y$ et résoudre la première équation avec la méthode de Brent
%\STATE fixer $x$ et résoudre la seconde équation avec la méthode de Brent
%\ENDWHILE
%\RETURN $x, y$
%\end{algorithmic}
%\end{algorithm}

\subsection{Implémentation numérique}

Plusieurs implémentation de la méthode de Brent existent déjà. Nous avons avons utilisé dans ce mémoire :
\begin{itemize}
	\item La fonction $fsolve$ de la toolBox optimisation de Matlab,
	\item La fonction $optimize$ sur R
	\item La fonction $?$ de la bibliothèque stk++.
\end{itemize}

\clearpage