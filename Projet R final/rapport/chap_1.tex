\chapter{Construction de l'algorithme EM}

\section{Généralités}

\subsection{Introduction aux modèles de mélange}

\paragraph{}
En statistique, un modèle de mélange, ou loi mélange est une variable aléatoire dont la fonction densité est une combinaison convexe de de plusieurs fonctions densités. Concrètement, chaque réalisation de cette variable peut s'interpréter comme un tirage suivant l'une des fonctions densités du mélange.

Ainsi, si l'on considère $K$ distributions \textit{D-dimensionnelles} $h_k(\cdot)$, définie sur $I$ paramétrées par $\blambda_{k},\balpha$, la densité $f$ du mélange issues de ces distributions est définie par:
\begin{equation}
 \forall x \in I, \quad f({\bx}|\theta) = \sum_{k=1}^K p_k h({\bx}| \blambda_{k},\balpha)
\end{equation}

Où :
\begin{itemize}
	\item les $p_k$sont les proportions du modèle de mélange, ($0<p_k<1$ quelque soit $k=1,...,K$ et $\sum_1^K p_k=1$).

	\item $\theta=(p_1,\ldots,p_K,\blambda_1,\ldots,\blambda_K, \balpha)$ représente le vecteur de paramètres du mélange.
\end{itemize}
 
 \paragraph{}
On peut également introduire le vecteur indicateur : {\em labels}
${\bz}=\{ {\bz}_1,...,{\bz}_n\}$, avec ${\bz}_i=(z_{i1},\ldots,z_{iK})$,

\[
z_{ik}=  \left\{
\begin{array}{rcl}

& 1 & \text{ si } {\bx}_i \; \text{provient de la kième distribution} \\

 & 0 &  \text{sinon}\\

\end{array}
\right.
\]

Ce vecteur est cependant inconnue à priori. I est néanmoins l'élément centrale de l'algorithme \textit{Espérance Maximisation}\cite{Dempster97}. En effet, une estimation de ce paramètre va permettre d'estimer à chaque itération les paramètres du mélange.

%Bien que le vecteur ${\bz}$ soit à priori inconnue, Nous allons l'utiliser pour estimer le vecteur de paramètre $\theta$ par la méthode dite de 'Espérance Maximisation\cite{Dempster97}. Dans un second temps, nous proposerons une version stochastique de l'algorithme, appelée SEM \cite{McLachlanPeel00}, puis une seconde version proche de k-mean appelée CEM


\subsection{fonction de vraisemblance}

\subsubsection{Définition}

En statistique, la fonction de vraisemblance (ou plus simplement notée vraisemblance), permet de mesurer l'adéquation entre une distribution observée sur un échantillon de données et une loi de probabilité censée décrire cet échantillon.

\paragraph{}
Considérons $x_1, x_2, ... x_N$ N observations d'une variable aléatoire de densité $x=f\left( \cdot \vert \theta \right)$, où $\theta$ joue le rôle du jeu de paramètres que l'on souhaite évaluer. On définie la fonction de vraisemblance au vue des observations $x$ par :

\begin{equation} 
	\label{eq:vrais}
l \left( \theta \vert x \right) = \prod_{i=1}^N f \left( x_i \vert \theta \right)
\end{equation}

Toutefois, on préférera travailler avec la fonction dite de log-vraisemblance, qui consiste à prendre le logarithme naturel de \ref{eq:vrais} 
\begin{equation} 
	\label{eq:lvrais}
L \left( \theta \vert x \right) = \sum{i=1}^N ln \left( f \left( x_i \vert \theta \right) \right)
\end{equation}

\paragraph{}
Sachant ceci, on peut donc réécrire la log-vraisemblance d'un modèle de mélange par :
 
\begin{equation}
  \label{eq:vraisemblance}
  l(\theta|\bx_1,\ldots,\bx_n)=\sum_{i=1}^n \ln \left(\sum_{k=1}^K p_k h(\bx_i,\blambda_k, \balpha)\right).
\end{equation}


\subsubsection{Maximum de vraisemblance}

\paragraph{}
Cette méthode, introduite par Ronald Aylmer Fisher en 1922 \cite{aldrich1997ra} est utilisée pour déterminer la valeur à postériori des paramètres d'une densité de probabilité, Connaissant $N$ observations. I s'agit donc d'un problème d'optimisation.

\paragraph{}
Dans ce mémoire, nous ne considérerons que des densités de probabilité continues et deux fois dérivables presque partout (ou des combinaisons convexes de telles densités possédant donc les mêmes propriétés). On alors le fait que s'il existe un maximum global $\hat{\theta}$ de la vraisemblance, alors sa dérivée s'annule forcément en ce point. Ainsi on peut se limiter à la recherche de maximum locaux en recherchant les zéros de la dérivés puis en s'assurant qu'il s'agit bien de maximums.

\paragraph{}
En pratique, on préférera travailler encore une fois avec la log vraisemblance, car la transformation des produits en somme rendant la manipulation des dérivés plus simple Ainsi, les conditions nécessaires suivantes
\begin{itemize}
	\item $\frac{\partial \log L(xi; \theta)}{\partial \theta} = 0$
	\item $\frac{\partial^2 \ln L(x_1, \ldots, x_i, \ldots,x_n;\theta)}{\partial \theta^2} \leq 0$
\end{itemize}
permettre de localiser les maxima locaux.

\paragraph{}
Cette estimateur possède quelques propriétés intéressantes, que nous ne démontrerons pas ici \cite{wasserman2004all}:
\begin{itemize}
	\item Convergence.
	\item Il atteint la borne de Cramér Rao (qui est une limite inférieure de la précision maximale que l'on peut obtenir avec un estimateur).
	\item Il est toutefois biaisé lorsque obtenu avec un échangé de taille finie.
\end{itemize}

\paragraph{} 
Il est également possible de définir des intervalles de confiances. Toutefois, nous ne nous y intéresserons pas dans ce mémoire.

\section{Construction de l'algorithme}

\subsection{Introduction}

\paragraph{}
L'algorithme \textit{Espérance Maximisation} a été introduit par  Arthur Dempster, Nan Laird, and Donald Rubin en 1977\cite{Dempster97}. On l'utilise par exemple pour la classification de données, l'apprentissage automatique, ou la vision artificielle dans le cas où les équations d'estimation des paramètres du modèle statistique ne sont pas analytiquement solubles, en raison, par exemple, de la présence d'une variable latente. L'algorithme a été également initialement conçu afin d'estimer les composantes du mélange de manière à maximiser la vraisemblance statistique du modèle, ce qui sera abordé dans le chapitre 2.

\paragraph{}
Le cas des modèles de mélange peut donc être traité en considérant simplement que pour chaque tirage $x_i$ de la variable aléatoire, il existe une observation supplémentaire $z_i$, dont la valeur correspond à la composante du mélange suivant laquelle $x_i$ a été tiré. Nous avons précédemment introduit cette variable sous le nom d'\textit{indicateur}. On crée ainsi un un nouveau modèle probabiliste $P_{\theta}(X, Z)$ , dont la vraisemblance sera beaucoup plus simple à maximiser par rapport au modèle initial $P_{\theta}(X)$.


\paragraph{}

\subsection{Construction}

Supposons que l'on dispose d'une valeur $\theta_t$ du vecteur de paramètres. Comme l'on cherche à maximiser la vraisemblance du modèle, on va choisir la valeur $\theta$ du vecteur de paramètres de manière à rendre la quantité $L(X, \theta) - L(X, \theta_t)$. La seconde quantité peut s'exprime simplement puisque l'on connait la valeurs des paramètres, tandis que l'on va utiliser la donnée manquante afin d'exprimer la première :

\begin{eqnarray}
  L(X, \theta) - L(X, \theta_t) & = & \sum_{n=1}^N \log \left( P_{\theta} \left( X=x_n, Z=z_n \right)  \right) - \sum_{n=1}^N \log \left( P_{\theta_t} \left( X=x_n \right) \right) \nonumber \\
   & = & \sum_{n=1}^N \log \left(\sum_{k=1}^K P_{\theta} \left( X=x_n, Z=k \right)  \right) - \sum_{n=1}^N \log \left( P_{\theta_t} \left( X=x_n \right) \right) \nonumber \\
   & = & \sum_{n=1}^N \log \left( \frac{\sum_{k=1}^K P_{\theta} \left( X=x_n, Z=k \right)}{P_{\theta_t} \left( X=x_n \right)}   \right) \nonumber \\
   & = & \sum_{n=1}^N \log \left( \sum_{k=1}^K P_{\theta_t} \left( Z=k \mid X=x_n \right) \cdot \frac{P_{\theta} \left( X=x_n, Z=k \right)}{P_{\theta_t} \left( X=x_n \right) \cdot P_{\theta_t} \left( Z=k \mid X=x_n \right) }   \right) \nonumber \\
   & = & \log \left( \mathbb{E}_{Z \mid x, \theta_t} \left[ \frac{P_{\theta} \left( X=x, Z \right) }{P_{\theta_t} \left( X=x \right) \cdot P_{\theta_t} \left( Z \mid X=x \right) } \right]  \right) \nonumber \\
 (\text{Inégalité de Jensen})  & \geq & \mathbb{E}_{Z \mid x, \theta_t} \left[ \log \left( \frac{P_{\theta} \left( x, Z \right) }{P_{\theta_t} \left( x \right) \cdot P_{\theta_t} \left( Z \mid x \right) } \right) \right] \nonumber \\
\end{eqnarray}

\paragraph{}
On a donc en résumé :
\begin{equation}
L(X, \theta) - L(X, \theta_t) = \underbrace{\mathbb{E}_{Z \mid x, \theta_t} \left[ \log \left( P_{\theta} \left( x, Z \right) \right) \right]}_{Q(\theta, \theta_t)} - \mathbb{E}_{Z \mid x, \theta_t} \left[ \log \left( P_{\theta_t} \left( x \right) \cdot P_{\theta_t} \left( Z \mid x \right) \right) \right]
\end{equation}

\paragraph{}
Sachant que le terme de droite est toujours positif en tant qu'opposé de la somme de log vraisemblance, il suffit \textbf{de s'assurer de l'existence d'une valeur du paramètre $\theta$ rendant le terme de gauche positif} afin de construire une suite croissante de valeurs du vecteur paramètres. Sachant que cette suite est croissante, et que la log vraisemblance possède une borne supérieur (0), on est assuré de la convergence vers un maximum de vraisemblance, éventuellement local.

\paragraph{}
Sachant que le terme de droite ne dépend pas du paramètre $\theta$, une valeur convenable de ce paramètre peut être obtenue en maximum $Q(\theta)$, et en s'assurant de la positivité en cette valeur.


\subsection{Mise en place de l'algorithme}

\paragraph{}
En retravaillant l'expression de $Q$ on remarque que:
\begin{eqnarray}
  Q(\theta, \theta_t & = & \mathbb{E}_{Z \mid x, \theta_t} \left[ \log \left( P_{\theta} \left( x, Z \right) \right) \right] \nonumber \\
  	& = & \mathbb{E}_{Z \mid x, \theta_t} \left[ L \left( x, z; \theta \mid \theta_t \right) \right] \nonumber \\
  	& = & \mathbb{E}_{Z \mid x, \theta_t} \left[ \sum_{n=1}^N \sum_{k=1}^K z_{n, k} \log \left(  p_k f(x_n, \theta) \right) \right] \nonumber \\
    & = & \sum_{n=1}^N \sum_{k=1}^K \underbrace{\mathbb{E}_{Z \mid x, \theta_t} \left[ z_{n, k } \right]}_{t_{n,k}} \log \left(  p_k f(x_n, \theta) \right) \nonumber \\
\end{eqnarray}

Les $t_{n, k}$ peuvent s'interpréter comme la probabilité à postériori d'appartenance du point $x_n$ à la classe $k$.

\subsubsection{Mise à jour des probabilités d'appartenance}

Or, la formule d'inversion de Bayes nous fournit le résultat suivant:
\begin{eqnarray}
t_{n,k} & = & \mathbb{E}_{Z \mid x, \theta_t} \left[ z_{n, k } \right] \nonumber \\
	& = & \mathbb{P} \left( z_{n}=k \mid x_n, \theta_t \right) \nonumber \\
	& = & \frac{\mathbb{P} \left(z_{n}=k \right) \cdot \mathbb{P} \left(x_n, \theta_t \mid z_n=k \right)}{\mathbb{P} \left(x_n, \theta_t \right)} \nonumber \\
	& = & \frac{p_k^{(t)} \cdot f_k\left(x_n, \theta_t \right)}{ \sum_{k=1}^K p_k^{(t)} f_k\left(x_n, \theta_t \right)} \nonumber \\
\end{eqnarray}

\subsubsection{Mise à jour des proportions du mélange}

La mise à jours des proportions du mélange est elle aussi indépendante du type de mélange considéré. Compte tenues des propriétés de transformation des produits en somme de la fonction logarithme, la mise à jour des proportions peut être \textit{vu comme la solution du problème d'optimisation concave sous contraintes suivant:
\[
\left\{
\begin{array}{r c l}
%---------------------------------------------------------------%
\left( p_k \right)_{k\in[1, k]} & = & \underset{(p_k)}{argmax} \sum_k C_k \log p_k \\
%---------------------------------------------------------------%
\sum_{k=1}^K p_k & = & 1 \\
%---------------------------------------------------------------%
%---------------------------------------------------------------%
 \forall k, &  & p_k \in \left[ 0, 1 \right] \\
%---------------------------------------------------------------%
\end{array}
\right.
\]}

En notant $C_k$ la quantité $\sum_{n=1}^N t_{n, k}$.

\paragraph{}
\textbf{Sous l'hypothèse de non nullité de toutes les composantes du mélange} ($\forall k, p_k \in \left] 0, 1 \right[$), la concavité de la fonction objectif (en tant que somme de fonctions concaves) nous permet de trouver son maximum en utilisant la \textbf{contrainte d'optimalité générale} (wikipedia optimisation convexe pour l'instant) afin d'en cherche un maximum. Cherchons donc dans un premier temps les $0$ du gradient. En utilisant le fait que $p_K = 1 - \sum_{k=1}^{K-1} p_k$, la fonction objectif peut se réécrire de la manière suivante:

\[
\begin{array}{r c l}
%---------------------------------------------------------------%
\left( p_k \right)_{k\in[1, k]} & = & \underset{(p_k)}{argmax} \sum_{k=1}^{K-1} C_k \log p_k + C_K \log \left( 1 - \sum_{k=1}^{K-1} p_k \right) \\
%---------------------------------------------------------------%
\end{array}
\]

On obtient ensuite, en dérivant suivant chacune des composantes et en annulant le gradient:

\[
\begin{array}{r c l}
%---------------------------------------------------------------%
\forall j \in \left[ 1, K-1 \right] \frac{\partial}{\partial p_j} & = & \frac{C_j}{p_j} - \frac{C_K}{1 - \sum_{k=1}^{K-1} p_k} = 0 \\
%---------------------------------------------------------------%
\end{array}
\]
On en déduit rapidement, par combinaison linéaire de chacune des équations avec la  première que:
\[
\begin{array}{r c l}
%---------------------------------------------------------------%
\forall j \in \left[ 2, K-1 \right] p_j & = & \frac{C_j}{C_1} \cdot p_1 \\
%---------------------------------------------------------------%
\end{array}
\]


D'où, en réinjectant ces résultats dans la première équation:
\begin{eqnarray}
\frac{C_1}{p_1} & = & \frac{C_K}{ 1 - \sum_{k=1}^{K-1} p_k} \nonumber \\
p_1 \cdot C_K & = & C_1 \left( 1 - \sum_{k=1}^{K-1} \frac{C_k}{C_1} p_1 \right) \nonumber \\
\end{eqnarray}

On a obtient donc successivement:
\begin{eqnarray}
p_1 & = & \frac{C_1}{\sum_k C_k} \nonumber \\
\forall j \in \left[ 2, K-1 \right] p_j & = & \frac{C_j}{\sum_k C_k} \nonumber \\
 p_K & = & \frac{C_K}{\sum_k C_k} \nonumber \\
\end{eqnarray}

Soit, en se souvenant que $\sum_n \sum_k t_{n, k} = N$:
\begin{equation}
p_k = \frac{1}{N} \cdot \sum_n t_{n, k}
\end{equation}

\subsubsection{Algorithme final}

On peut finalement résumer la maximisation de la quantité $Q$ en deux étapes, qui donnent leur nom à l'algorithme :
\begin{itemize}
	\item \textbf{Étape E}, dite d'espérance : où l'on met à jour les $t_{i, k}$.
	
	\item \textbf{Étape M}, dite de maximisation : où l'on met à jour le vecteur paramètre $\theta$ grâce à la méthode du maximum de vraisemblance.
\end{itemize}

\paragraph{}
On obtient alors l'algorithme final :

\begin{algorithm}
\caption{Converger vers un maximum de L avec la précision $\epsilon$}
\begin{algorithmic} 
\REQUIRE $\epsilon, \theta_{init}$
\STATE $\theta_{courant} \leftarrow \theta_{init}$
\STATE $N_{iter} \leftarrow 0$
\WHILE{$L_{courant}-L_{ancien} > \epsilon$}
	\STATE \textbf{E step} : calculer les $t_{i, k}$ via la valeur courante de $\theta$
	\STATE \textbf{M step} : Mettre à jour l'estimation du paramètre $\theta$ grâce aux probabilités conditionnelles $t_{i, k}$, et mettre à jour les proportions $\pi$
	\STATE $N_{iter} \leftarrow N_{iter}+1$
\ENDWHILE
\RETURN $\theta$
\end{algorithmic}
\end{algorithm}


\section{Quelques variantes de l'algorithme}

\subsection{L'algorithme SEM}

L'algorithme SEM, présenté par Celeux et Diebolt \cite{celeux1985stochastic} est une version stochastique de l'EM, qui consiste à incorporer entre l'étape E et M une prédiction des labels $\bz_i$, $i=1,\ldots,n,$, en les tirant suivant une loi uniforme. Ainsi, en partant d'un paramètre initial $\theta^0$, l'algorithme SEM peut être résumer en trois étapes :

\begin{itemize}
	\item \textbf{E step:} mettre à jour les probabilités conditionnelles $t^m_{ik}$ $(1 \leq i \leq n, 1 \leq k \leq
  K)$ grâce à la valeur courante du paramètre$\theta$ 
  
	\item \textbf{S step:} Générer les labels ${\bz}^m=\{ {\bz}^m_1,...,{\bz}^m_n\}$ en attribuant à chaque point ${\bx}_i$ un label tiré suivant une loi uniforme, et dont les probabilités d'appartenance à chaque classe sont les $(t^m_{ik}, 1 \leq k \leq K)$.
  
	\item \textbf{M step:} mettre à jour $\theta$.
\end{itemize}

\paragraph{}
De par sa nature stochastique, l'algorithme SEM ne converge pas systématiquement. Il génère une chaîne de Markov dont on peut montrer que la \textbf{distribution stationnaire est  concentrée autour de l'estimateur du maximum de vraisemblance}. L'une des manières naturelles d'estimer le paramètre est alors de considérer, parmi toutes les itérations, celui qui a maximiser la vraisemblance.

\subsection{L'algorithme CEM}

Proposé par \cite{celeux1992classification}, l'algorithme CEM consiste à optimiser, non pas la vraisemblance du paramètre mis directement la vraisemblance complétée. C'est à dire, dans le cas présent la quantité:
\begin{center}
$L(x,z;\theta) = \sum_{n=1}^N \sum_{k=1}^K z_{n, k} \log \left( p_k f(x_n, \theta_k) \right)$
\end{center}

\paragraph{}
En pratique on vient insérer une étape \textbf{C} de détermination du vecteur $z$:
\begin{itemize}
	\item \textbf{E step:} mettre à jour les probabilités conditionnelles $t^m_{ik}$ $(1 \leq i \leq n, 1 \leq k \leq
  K)$ grâce à la valeur courante du paramètre$\theta$ 
  
	\item \textbf{C step:} Déterminer les $z_{n, k}$ tels quel
	\[ z_{n, k} = \left\{
  \begin{array}{l l}
    1 & \quad \text{ if } t_{n, k}=\underset{j}{max} t_{n, j}\\
    0 & \quad \text{sinon}
  \end{array} \right.\]
  
	\item \textbf{M step:} mettre à jour $\theta$.
\end{itemize}

\clearpage