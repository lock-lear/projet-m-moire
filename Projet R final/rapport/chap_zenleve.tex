\paragraph{}
On peut de cette façon travailler avec des distances dans $\mathcal{H}_k$, plutôt qu'une distance dans $\mathbb{X}$ éventuellement non définie. On considère alors le vecteur :
\begin{center}
$Y = {\Vert \Phi(X) - \mu \Vert }^2$
\end{center}
où $\mu$ représente le centre de la classe considérée. 

\paragraph{}
Nous nous concentrerons de plus dans ce mémoire sur le cas d'un noyau gaussien :
\begin{equation}
k(x, y) = \exp \left( \frac{-d(x, y)}{\sigma} \right)
\end{equation}
où 
\begin{itemize}
	\item $x$ et $y$ sont deux points de $\mathbb{X}$.
	
	\item $d$ est une distance sur $\mathbb{X}$.
	
	\item $\sigma$ est la bande passante du filtre. Si l'on veut la voir comme un paramètre de normalisation, Silverman(1986) \cite{Silverman1996} a proposé plusieurs moyens de la faire dépendre du jeu de données. Toutefois $sigma$ \textbf{sera supposé constant ici}.
\end{itemize}


\subsection{Estimation des paramètres}
\label{SectionEstimation}

Une fois encore nous allons chercher à estimer chaque paramètre par le maximum de vraisemblance. En ce qui concerne les centroids ($\mu$), nous utiliserons l'estimateur gaussien :
\begin{equation} \label{mono-simpleEst}
\hat{\mu} = \sum_{i=1}^N \Phi(x_i)
\end{equation}
Cette estimateur est toutefois pleinement justifié si l'on suppose la répartition gaussienne de $\Phi(X)$.

\subsubsection{Distribution exponentielle}

\paragraph{modèle univarié}

Supposons donc que $Y$ suit une distribution exponentielle, dont la densité est donnée par 
\begin{equation}
f_{\beta}(y) = \frac{1}{\beta} \exp \left( {-\frac{y}{\beta}} \right) \; \; \forall y \: \in \: \mathbb{R}^+
\end{equation}


La log vraisemblance vaut :
\begin{equation}
L(x, \beta) = \sum_{i=1}^n -\frac{y_i}{\beta} - n \ln(\beta)
\end{equation}

En annulant la dérivée partielle suivant $\beta$, on obtient :
\[
\begin{array}{r c l}
\hat{\beta} & = & \frac{1}{n} \sum_{i=1}^n {\Vert \Phi(x_i)- \mu \Vert}^2
\end{array}
\]
qui correspond bien à un maximum


Puis, en utilisant (\ref{mono-simpleEst}), et l'astuce du noyau :
\[
\begin{array}{r c l}
\hat{\beta} & = & \frac{1}{n} \sum_{i=1}^n \langle \Phi(x_i) - \sum_{j=1}^n \Phi(x_j), \; \Phi(x_i) - \sum_{j=1}^n \Phi(x_j) \rangle \\
& & \\
& = & \frac{1}{n} \sum_{i=1}^n \left[ k(x_i, x_i) - \frac{2}{n} \sum_{j=1}^n k(x_i, x_j) + \frac{1}{n^2} \sum_{j=1}^n \sum_{l=1}^n k(x_j, x_l)  \right]
\end{array}
\]

soit finalement :
\begin{empheq}[box=\fbox]{equation}
   \hat{\beta} = \frac{1}{n} \sum_{i=1}^n k(x_i, x_i) - \frac{1}{n^2} \sum_{i=1}^n \sum_{j=1}^n k(x_i, x_j)
\end{empheq}

\paragraph{modèle multivarié}
Supposons maintenant que $Y$ est distribué suivant un mélange à $K$ composante de loi exponentielle :
\[
\left\{
\begin{array}{c c c}
f(x) & = & \sum_{k=1}^K \pi_k \frac{1}{\beta_k} \exp \left( \frac{1}{\beta_k} {\Vert \Phi(x) - \mu_k  \Vert}^2 \right) \\
\sum_{k=1}^K \pi_k & = & 1
\end{array}
\right.
\]

Cette fois on a deux familles de paramètres à estimer
\begin{itemize}
	\item K le nombre de composantes. Nous le supposerons fixé et connu pour l'instant. Nous proposerons dans une autre section un moyen de l'estimer.
	
	\item $(\pi_k)$ les proportions du mélange
	
	\item $(\beta_k)$ les paramètres de chaque distribution.
\end{itemize}

\subparagraph*{}
En utilisant les deux familles d'hyper-paramètres introduites dans la précédente partie, $(z_{i,k})$ et $(t_{ik})$, on peut réécrire la log vraisemblance  :
\begin{center}
$L((\beta_k, \pi_k, \mu_k) | x_1...x_N) =  \sumi \sumk t_{i, k}\left(  -\log \beta_k - \frac{1}{\beta_k}y_{i, k} + \log \pi_k \right)$
\end{center}

En réintroduisant les centroids dans l'expression, on a finalement :

\begin{equation}
\boxed{
\forall k \in [1, K] \quad \left\{
\begin{array}{rcl}

\hat{y}_{i, k} & = & k(x_i, x_i) - \frac{2}{\sumt} \sumj t_{j, k} k(x_i, x_j) \\

& & + \frac{1}{{(\sumt)}^2} \sumj \suml t_{j, k} t_{l, k} k(x_j, x_l)\\

& &   \\
\hat{\beta_k} & = & \frac{1}{\sumt} \sumi t_{i, k} k(x_i, x_i) - \frac{1}{ \left( \sumt \right)^2} \sumi \sumj t_{i, k} t_{j, k} k(x_i, x_j) \\

\end{array}
\right.
}
\end{equation}





\subsubsection{Distribution de Raileigh}

Comme nous l'avons expliqué précédemment, une loi de Rayleigh peut être vu comme la loi de la norme d'un vecteur Gaussien à deux dimensions, où les coordonnées sont indépendantes centrées et de même variances. Sa densité est donnée par :

\begin{center}
$f(x; \theta) = \frac{x}{\theta^2} \exp \left( -\frac{x^2}{2\theta^2} \right)$
\end{center}

\paragraph{Modèle univarié}

On suit le même schéma
\[
\left.
\begin{array}{rcl}

L(Y; \theta) & = & \sumi \left( \log(y_i) - \frac{2 y_i^2}{2\beta^2} \right) -2N \log(\theta) \\

& &   \\

\frac{\partial L}{\partial \theta} & = & \frac{1}{\theta} \left( \sumi \frac{y_i^2}{\theta^2} - 2N \right) \\

\end{array}
\right.
\]

On obtient :
\[
\frac{\partial L}{\partial \theta}  \left\{
\begin{array}{rcl}

& > 0 & \text{ si } \theta^2 < \frac{1}{2N} \sumi y_i^2 \\

 & = 0 &  \text{ si } \theta^2 \frac{1}{2N} \sumi y_i^2\\

& < 0 & \text{ si } \theta^2 > \frac{1}{2N} \sumi y_i^2 \\

\end{array}
\right.
\]

et finalement :
\begin{equation}
\hat{\theta}^2 = \frac{1}{2N} \sumi y_i^2 
\end{equation}

\paragraph{Modèle multivarié}

Comme vu précédemment 
\[
\left.
\begin{array}{rcl}

L(Y; \theta_k, \pi_k) & = & \sumi \sumk \left( \log(\pi_k) + \log(y_{i, k}) - \frac{2 y_{i, k}^2}{2\beta_k^2} -2\log(\beta_k) \right) \\

\end{array}
\right.
\]

Et l'on obtient les estimateurs :
\begin{equation}
\forall k \in [1, K] \qquad \hat{\theta_k}^2 = \frac{1}{2\sumi t_{i, k}} \sumi y_{i, k}^2 
\end{equation}

\subsection{Choisir le nombre de composante du mélange}

\paragraph{}
Si le nombre de composante est inconnue, On se propose d'utiliser ne première approche le critère d'information d'Akaike (abrégé AIC). Celui permet de trouver un compromis entre la précision du modèle et sa complexité (i.e. le nombre de paramètre à estimer). En prenant en compte le nombre de paramètre à estimer, on obtient :
\begin{equation}
K = \underset{m \geq 1}{\text{argmax}} \left( l((\beta_k, \mu_k, \pi_k)_{k=1}^m \vert Y, Z) - 3m  \right) = 0
\end{equation}

\paragraph*{}
On rappelle rapidement les expressions de la vraisemblance pour chaque modèle, obtenue section \ref{SectionEstimation} :
\[
\left.
\begin{array}{rcl}

L_{exp}((\beta_k, \pi_k, \mu_k) | x_1...x_N) & = & \sumi \sumk t_{i, k}\left(  -\log \beta_k - \frac{1}{\beta_k}y_{i, k} + \log \pi_k \right) \\


L_{ray}(\beta_k, \pi_k, \mu_k) | x_1...x_N) & = & \sumi \sumk \left( \log(\pi_k) + \log(y_{i, k}) - \frac{2 y_{i, k}^2}{2\beta_k^2} -2\log(\beta_k) \right) \\

\end{array}
\right.
\]



\subsection{Choix de la bande passante}

\paragraph{}
Restons dans l'esprit bayésien et essayons d'estimer la bande passante par la méthode du maximum de vraisemblance. Pour des raisons de praticité, nous ne présenterons les résultats que pour un mélange univarié.
\linebreak

\subparagraph{}
Pour rappel, nous avons vu, section~\ref{SectionEstimation} :
\[
\forall i \quad \left\{
\begin{array}{rcl}

k(x_i, x_j) & = & \exp \left( -\frac{d(x_i, x_j)}{\sigma} \right) \\

& &   \\

\hat{y}_{i, k} & = & k(x_i, x_i) - \frac{2}{N} \sumj k(x_i, x_j) + \frac{1}{N^2} \sumj \suml k(x_j, x_l)\\

& &   \\

L(X, \beta, \sigma | \hat{\mu}) & = & \sumi -\frac{1}{\beta}y_i - N \log (\beta ) \\ 

\end{array}
\right.
\]

Sachant que $\frac{\partial k}{\partial \sigma}(x_i, x_j) = \frac{d(x_i, x_j)}{\sigma^2} \exp \left( -\frac{d(x_i, x_j)}{\sigma} \right)$,on obtient, en notant $h_i(X)$ la quantité $\sumj d(x_i, x_j) \exp \left( -\frac{d(x_i, x_j)}{\sigma} \right)$, l'expression du gradient de la log vraisemblance suivant $\sigma$ :

\[
\left.
\begin{array}{rcl}

 &  & \sumi \left( -\frac{2}{N} h_i(X) +\frac{1}{N^2} \sumj h_j(X) \right) \\

 & &   \\

\text{i.e.} & & \frac{\partial L}{\partial \sigma} = \sumi \sumj d(x_i, x_j) \exp \left( -\frac{d(x_i, x_j)}{\sigma} \right) \\

\end{array}
\right.
\]

\subparagraph*{Avec une loi de Raileigh} Seule la vraisemblance change; On a donc :

\[
\left.
\begin{array}{rcl}

\frac{\partial L}{\partial \sigma} & = & \sumi \left( h_i(X) \frac{1}{y_i} -\frac{h_i(X)}{\beta^2} y_i \right) \\

\end{array}
\right.
\]

Remarque : Pour les deux distribution, la bande passante ne dépend que de $h_i$ et $y_i$.

\subparagraph*{Analyse}
On peut d'abord souligner le fait que la vraisemblance, pour une distribution exponentielle, semble être une fonction croissante de la bande passante. Cependant, avec une distribution Rayleigh, la non linéarité de l'expression est trop importante pour en extraire un comportement.


%\section{Conclusion}