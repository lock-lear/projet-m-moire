\chapter{EM à noyau}

\paragraph{}

Dans cette quatrième partie nous allons chercher à introduire des méthodes à noyau dans l'algorithme EM, comme cela été présenté par Nicolas Wicker et Alejandro Murua \cite{Wicker2014} et \cite{murua2014kernel}. En effet, l'utilisation de telles méthodes en classification a le potentiel théorique de:
\begin{itemize}
	\item Rendre certains problèmes linéaires, en transportant les données dans un nouvel espace, et où les méthodes classiques de classification peuvent fonctionner.

	\item Envisager l'application des méthodes usuelles sur des données au format plus exotique (texte, distance inter-point etc...)
	
	\item Traiter des cas où la dimension est problématique (classification d'image par exemple).
\end{itemize}

\section{Vers des méthodes à noyaux}

\paragraph{}
Il existe plusieurs manières d'introduire et d'interpréter les méthodes à noyaux. Nous les interpréterons ici comme \textbf{un passage par un espace de redescription}. Ceci nous amènera à ce qu'on appelle \textbf{l'astuce du noyau}.

\subsection{Un produit scalaire dans la loi normale}

\paragraph{}
Avant de travailler sur des modèles de mélange, considérons des distribution simples, et en particulier une distribution gaussienne, notée $f$. Considérons un espace vectoriel $\mathbb{X}$ et supposons dans un premier temps que $\mathbb{X}=\mathbb{R}^d$. Soit $x$ un point de $\mathbb{X}$. Notons $\mu$ le vecteur de moyenne de la loi gaussienne, $\sigma$ sa variance, identique sur toutes les dimensions.La valeur de la densité de probabilité en $x$ est alors donnée par :
\begin{equation}
f(x) = K^d \cdot \exp^{-\frac{1}{2\sigma^2}(x-\mu)^T (x-\mu)}
\end{equation}

où $K=(2\pi){-\frac{1}{2}} \cdot \sigma^{-1}$ la constance de normalisation.

\paragraph{}
On s'aperçoit que $(x-\mu)^T(x-\mu)$ définit en réalité un produit scalaire et implicitement une norme sur un espace. Ainsi, rien ne nous empêche de généraliser, du moins théoriquement, cette loi à des espaces plus généraux en changeant de produit scalaire.

\subsection{Vers un espace de redescription : l'astuce du noyau}

\paragraph{}
Considérons maintenant que $\mathbb{X}$ est un espace quelconque. Nous allons chercher à généraliser la loi normale simplifiée, évoquée à la section précédente, à cet espace. On commence par donner la définition suivante :
\begin{def_noyau}
la fonction $k \qquad \mathbb{X} \times \mathbb{X} \rightarrow \mathbb{R}$ est un noyau si et seulement si elle satisfait la la propriété suivante:
\begin{equation}
\forall x, x' \in \mathbb{X}, k(x, x') = \langle \Phi(x), \Phi(x') \rangle
\end{equation}
où $\Phi$ est une fonction de $\mathbb{X}$ dans \textbf{F} doté d'un produit scalaire.
\end{def_noyau}

\paragraph{}
Aizermana a montré, en 1964, que l'on peut caractériser cette fonction de transfert par la donnée d'un noyau \cite{Aizerman1964}:
\begin{th_ktrick}
Si $k \qquad \mathbb{X} \times \mathbb{X} \rightarrow \mathbb{R}$ est symétrique et définie positive, Alors:
\begin{itemize}
	\item On est assuré d'existence d'un fonction de transfert $\Phi$.
	\item L'espace d'arrivée \textbf{F} est un espace Hilbertien. On le ne notera $\mathbb{H}_k$.
	\item k agit comme un produit scalaire sur $\mathbb{H}_k$. On a alors $\forall x, x' \in \mathbb{X}, k(x, x') = \langle \Phi(x), \Phi(x') \rangle_{\mathbb{R}_k}$.
	\item $\mathbb{R}_k$ est de dimension au moins égale à celle de $\mathbb{X}_k$.
\end{itemize}
\end{th_ktrick}

\paragraph{}
L'astuce du noyau consiste à, connaissant un point $x$ de $\mathbb{X}$, être capable de calculer la quantité $\Vert \Phi(x) \Vert$. La fonction $\Phi$ est en effet implicitement définie par la connaissant d'une fonction symétrique définie positive, que l'on choisi. On peut déterminer dans certain cas particulier (noyau polynomiaux etc...) la dimension de $\mathbb{H}_k$ \cite{scholkopf2001learning}, mais aucun résultat systématique n'est disponible.

\section{Considérations liées aux noyaux}

\paragraph{}
Nous allons dans cette section présenter un argument légitimant le recourt aux noyaux dans la classification.

\subsection{Capacité théorique de pulvérisation}

\paragraph{}
Commençons par introduire la notion de \textit{pulvérisation d'un classificateur} et de \textit{dimension de Vapnik-Chervonenkis} :
\begin{def_pulverisation}
Soit $f$ un modèle de classification paramétrisé par $\theta$, n un entier quelconque et $x=\left( x_i \right)_{i\in [1;n]}$ $n$ points quelconques..\\
On dit que $f$ pulvérise $x$ si pour tout étiquetage $z$ de x, il existe une valeur du paramètre $\theta$ qui rend nulle l'erreur de classification sur $x$.\\
On appellera alors dimension de Vapnik-Chervonenkis (ou dimension VC) de $f$ le cardinal du plus grand ensemble pulvérisé par $f$. Cette dimension peut être éventuellement infinie. Formellement, 
\begin{center}
$Dim_{VC} = max \{\quad k  \quad | \quad card(S)=k \quad et \quad f \quad pulvérise \quad S\quad\}$
\end{center}
\end{def_pulverisation}

\paragraph{}
Illustrons cette notion avec un cas simple. Considérons un classificateur linéaire $f$ de $\mathbb{R}^2$ (c'est à dire séparant les données avec juste une simple droite). On montre facilement qu'alors $Dim_{VC}(f) = 3$. Cela se voit sur l'exemple suivant:
\begin{figure}[!htbp]
\includegraphics[scale=1]{img/chap_kem/pulverisation.png}
\caption{Illustration de la pulvérisation et de la notion de dimension VC}\label{fig_illustrationPulverisation} On constate sur les trois schémas de gauche que quelque soit l'étiquetage, on parvient à séparer parfaitement le jeu de donnée avec une droite. La dimensions VC de ce classificateur est donc d'au moins 3. La figure de droite montre en revanche un exemple avec quatre points où la séparation parfaite est impossible. La dimension VC est donc finalement exactement 3.
\end{figure}

\paragraph{}
On introduit maintenant la \textbf{notion de noyau universel}:
\begin{def_noyauUniversel}
Une fonction noyau continue sur un espace compact métrique $\mathbb{X}$ est \textbf{dite universelle} si et seulement si l'espace de Hilbert à noyau reproduisant (RKHS)  $\mathbb{H}_k$ est dans dans l'espace des fonction continues de $\mathbb{X}$ dans $\mathbb{R}$ (noté $\mathfrak{C}\left(\mathbb{X} \right)$)
\end{def_noyauUniversel}

La densité peut se traduire par le fait que pour toute fonction $g$ de $\mathfrak{C}\left(\mathbb{X} \right)$ et tout  $\epsilon$ strictement positif, il existe une fonction $f$ de $\mathbb{H}_k$ telle que $\Vert f - g \Vert_{\infty} \leq \epsilon$.

\paragraph{}
On peut alors montrer que si un noyau est universel, alors sa dimension VC est infinie, c'est à dire qu'il est capable de pulvériser tout ensemble de $\mathbb{X}$, et donc capable de séparer le en n'importe quel sous ensemble.\\

\subparagraph{Élément de démonstration} 
En effet, si l'on fixe un ensemble de $n$ points $\left( x_i \right)_{i\in [1;n]}$, munie d'un étiquetage $z$.\\
Notons $f_i$ une fonction continue de $\mathbb{X} \rightarrow \mathbb{R}$ permettant de caractériser l'appartenance à la classe. La difficulté réside dans la construction d'une telle fonction. Si on l'admet, la densité de $\mathbb{H}_k$ dans $\mathfrak{C}\left(\mathbb{X} \right)$ nous assure alors de l'existence d'une fonction permettant d'estimer $f_i$ avec la précision souhaité en norme infinie, et donc de rendre l'erreur de classification nulle sur x pour la classe $i$.\\
Ainsi, en répétant cette étape pour chaque classe, on peut construire par assemblage un classificateur pulvérisant $x$.

\subparagraph{}
Ainsi, si l'on est capable de construire un noyau universelle, on est assurée de la qualité théorique du classificateur. Nous allons donc essayer de construire de telle noyau dans la partie suivante.

\subsection{Présentation de quelques noyaux}

\paragraph{Noyaux gaussiens}
Défini par:
\begin{equation}
k(x, y) = \exp(\frac{\Vert x - y \Vert^2}{\sigma^2}
\end{equation}

où $\Vert \cdot \Vert$ est une norme quelconque, et $\sigma$ un paramètre de normalisation des données.

\paragraph{}
On admet que ce noyau est universel. C'est pour sa simplicité et cette propriété que nous nous y intéresserons dans la partie application.

\section{EM noyau pour une loi exponentielle}

Dans toute cette partie, $k$ sera un noyau arbitrairement choisi et respectant les propriétés évoquées dans la partie précédente. On définie alors la loi de densité $f$ du modèle paramétrique $P_{k, \theta}$ par, en reprenant les notations du chapitre précédent:
\begin{equation}
\forall x \in \mathbb{X}, \qquad f(x) = \sum_{k=1}^K \pi_k f_k(\Vert \Phi(x) - \mu \Vert^2, \theta)
\end{equation}
 On utilisera parfois la notation $y = \Vert \Phi(x) - \mu \Vert^2$.

\subsection{Le problème de la dimension}

\paragraph{}
On dispose maintenant de suffisamment d'outil pour généraliser la distribution gaussienne à l'espace $\mathbb{X}$. Soit donc $k \qquad \mathbb{X} \times \mathbb{X} \rightarrow \mathbb{H}_k$ une fonction symétrique définie positive, arbitrairement définie. $k$ est donc un noyau d'après le théorème précédent. Sachant que la dimensions de l'espace $\mathbb{H}_k$ est au moins égale à celle de X, on peut espérer rendre un problème de classification linéaire dans cette espace. La tentative d'y reproduire l'algorithme \textbf{EM} est donc pleinement justifiée. Reprenons donc la généralisation de la gaussienne simplifiée. On obtient alors la densité de probabilité:
\begin{equation}
f(x) = K^d \cdot \exp^{-\frac{1}{2\sigma} \cdot \Vert \Phi(x) - \mu \Vert^2}
\end{equation}
où $\mu$ est le centre de la moyenne de la loi dans l'espace $\mathbb{H}_k$

\paragraph{}
On remarque cependant que le calcul de la densité nécessite la connaissance de la dimension de l'espace $\mathbb{H}_k$, que l'on n'est pas forcément capable de fournir. On peut alors choisir de se restreindre à des noyaux dont la dimension a été calculé, tels que les noyaux polynomiaux, évoqués précédemment (de la forme $k(x, y) = \left( x' \Sigma^-1 y \right)^n$, où $\mathbb{X} = \mathbb{R}^p$ et $\Sigma$ une matrice symétrique définie positive). On sait alors que la dimension vaut $p'=\begin{pmatrix}
   n + p - 1\\
   n - 1
\end{pmatrix}$.

\paragraph{}
Si en revanche l'on souhaite utiliser des noyaux plus généraux, N. Wicker et A. Murua proposent, \cite{murua2014kernel} de se restreindre à modéliser la distribution des distances au centre de la classe $\mu$. On se retrouve alors avec un nouveau vecteur d'observations, donnée par $Y_{\mu}=\Vert \Phi(x) - \mu \Vert^2$, positif, et la dimension du problème vaut à nouveau 1.
 
\subsection{Estimation des paramètres}

\paragraph{Estimation du centroide}
On se retrouve néanmoins ici avec un nouveau paramètre à estimer, le centre $\mu$ d'un groupe de point. La principale différence avec les autres paramètres est qu'ici $\mu$ est un élément de l'espace $\mathbb{H}_k$, où les règles de dérivation classiques ne s'appliquent plus. Toutefois, puisque l'on a la chance de travailler dans un espace de Hilbert, on peut toutefois proposer un estimateur de ce paramètre. Pour $x=(x_i)$ un ensemble de points, la log vraisemblance du modèle évoquée précédemment vaut: $f(x, \theta) = Const - \frac{1}{\sigma} \cdot \sum_{i=1}^N \Vert \phi(x_i) - \mu \Vert^2$. Soit $h$ un vecteur de $\mathbb{H}_k$. On peut alors écrire:
\begin{eqnarray}
f(x, \theta, \mu) - f(x, \theta, \mu + h) & = & \sum_{i=1}^N \Vert \phi(x_i) - \mu - h \Vert^2 - \Vert \phi(x_i) - \mu \Vert^2 \nonumber \\
& = & -2 \langle \sum_{i=1}^N \phi(x_i) - \mu, h \rangle + \Vert h \Vert^2 \nonumber \\
\end{eqnarray}

Ainsi, on obtient l'application linéaire, en faisant tendre $\Vert h \Vert$ vers 0:
\begin{equation}
\frac{\partial f_{\beta} (x)}{\partial \mu} = \langle \sum_{i=1}^N \phi(x_i) - \mu, \cdot \rangle
\end{equation}

En cherchant à maximiser suivant cette dérivée partielle on obtient l'estimateur : $\hat{\mu} = \frac{\sum_{i=1}^N x_i}{n}$, qui est finalement une extension des estimateurs de moyenne sur $\mathbb{R}^p$ à des espaces Hilbertiens plus généraux.

\paragraph{}
On ne détaillera pas les calculs, mais on peut généraliser cet estimateurs aux modèles de mélange via:
\begin{equation}
\hat{\mu_k} = \frac{\sum_{i=1}^N z_i x_i}{\sum_{i=1}^N z_i}
\end{equation}

\paragraph{}
On est donc maintenant en mesure de fournir une formule pour le calcul pour le vecteur $y$. On obtient, pour tout $k $ dans $[1, K]$:
\begin{eqnarray}
\hat{y}_{i, k} & = & k(x_i, x_i) - \frac{2}{\sumt} \sumj t_{j, k} k(x_i, x_j) \nonumber \\
& & + \frac{1}{{(\sumt)}^2} \sumj \suml t_{j, k} t_{l, k} k(x_j, x_l) \nonumber \\
\end{eqnarray}

\subsection{Règle de classification}

\paragraph{Par les probabilités conditionnelles}
On peut naturellement envisager la règle de classification traditionnelle, utilisée pour l'agorithme EM. On attribut la classe $g^*$ à $x^*$ suivant la règle suivante :
\begin{center}
$g^* = \underset{g=1..K}{\text{argmax}} \pi_g f_{\beta_g} \left( {\Vert \Phi(x^*) - \mu_g \Vert}^2 \right)$
\end{center}

D'une point de vue pratique, on remarque que l'on a maintenant besoin de la connaissance de chaque $\mu_g$. Mais ceux-ci étant obtenir à partir de la fonction $\Phi$, on a donc pas directement accès à l'information. Cependant, on remarque en développant le calcul de $y\star$ :
\[
\left.
\begin{array}{c c c}
{\Vert \Phi(x^*) - \mu_g \Vert}^2 & = & \langle \Phi(x^*) - \mu_g, \Phi(x^*) - \mu_g \rangle \\
&  &  \\
& = & k(x^*, x^*)  - \underbrace{\frac{2}{\bar{t_k}} \sumj t_{j, k} k(x^*, x_j)}_{en \quad \theta(N)} \; + \underbrace{\frac{1}{{\bar{t_k}}^2} \sumj \suml t_{j, k} t_{l, k} k(x_j, x_l)}_{\text{Already known}} \\
\end{array}
\right.
\]

\paragraph{La règle du CICRV}
Cependant, Nicolas Wicker and Alejandro Murua propose dans leur article \cite{Wicker2014}, une nouvelle règle tenant compte de la connaissance des distances d'un point à chacun des centres, c'est à dire les distances $d_k=\Vert \phi(x^*) - \mu_k \Vert$. On note d'ailleurs $D(x^*)$ le vecteur de $\mathbb{R}^K$ des distances de $x^*$ à chacun des centroides. La règle traditionnelle suppose que, en notant $g$ l'évènement "$x^*$ appartient à la classe $g$": $P \left( g \mid D(x^*) \right) = P \left( g \mid d_g(x^*) \right) \rtimes \hat{p_g}f_g(x^*)$.

\paragraph{}
Mais les auteurs proposent plutôt de considérer que:
\begin{center}
$P \left( g \mid D(x^*) \right) \rtimes \hat{p_g} P \left( D(x^*) \mid g \right)$
\end{center}

\paragraph{}
Cependant, on ne sait pas calculer cette quantité. On peut alors faire les hypothèses suivantes:
\begin{itemize}
	\item les distances sont indépendantes selon les classes. c'est à dire que $P \left( g \mid D(x^*) \right) = \prod_k P \left( d_k^2(x^*) \mid g \right)$
	\item Pour chaque classe $k$, $P \left( \cdot \mid g \right)$ suit une loi une loi gamma, dont les deux paramètres sont ceux provenant des résultats de l'algorithme $EM$.
\end{itemize}
	
\paragraph{}
On obtient donc la règle appelée \textit{Conditionally independant class-repeated-variable rule}, noté CICRV:
\begin{center}
$g^* = \underset{g=1..K}{\text{argmax}} \qquad \pi_g f_{\beta_g} \prod_k P \left( d_k^2(x^*) \mid g \right)$
\end{center}

\clearpage