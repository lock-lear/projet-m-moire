EM_gaussien <- function (X, K_hat, whichStop, param_stop, mu_init=NULL, sigma_init=NULL, p_init=NULL) {
  # X must be a N * D matrix
  # K_hat the number of estimated cluster
  # whichStop is an integer for which parameter you want to stop the EM
  #     - 0 for nbIteration
  #     - 1 one for log likelihood
  # param_stop the parameter
  # initialise : 1 T if mu, signa and p are alreay initialise. Else they are with the M step with
  # random value of t
  # mu_init, sigma_init and p_init the optional parameter for initialisation
  
  N <- 0
  D <- 0
  computeDimension(X, N, D)
  
  
  if (is.null(mu_init) || is.null(sigma_init) || is.null(p_init)) {
    print("début initialisation")
    t1 <- Sys.time()
    
    l <- initiase_gaussien(X, N, K_hat, D, 20)
    l <- SEM_gaussien(X, K_hat, 0, 50, l_init=l)    
    
    print("fin initialisation")
    print(Sys.time() - t1)
  }
  
  else {
    l <- list(mu=mu_init, sigma=sigma_init, pi=pi_init)
    if(!verifyShape_gaussien(X, N, D, l))
      return(NULL)
  }
  
  logLikelihood <- logLikelihood_gaussien(X, l)
  
  print("début EM")
  t1 <- Sys.time()
  n=0
  while(whichStop==0 & n < param_stop | whichStop==1 & logLikelihood >10) {
    
    # E step
    t = E_step_gaussien(X, N, K_hat, D, l)
  
    # M step
    l <- M_step_gaussien(X, t)
    
    # break condition
    n <- n+1
  }
 
  print(Sys.time() - t1)
  print("fin EM")
  
  return(c(l, list(type="norm"))
};






SEM_gaussien <- function (X, K_hat, whichStop, param_stop, mu_init=NULL, sigma_init=NULL, p_init=NULL, l_init=NULL) {
  # X must be a N * D matrix
  # K_hat the number of estimated cluster
  # whichStop is an integer for which parameter you want to stop the EM
  #     - 0 for nbIteration
  #     - 1 one for log likelihood
  # param_stop the parameter
  # initialise : 1 T if mu, signa and p are alreay initialise. Else they are with the M step with
  # random value of t
  # mu_init, sigma_init and p_init the optional parameter for initialisation
  
  N <- 0
  D <- 0
  l <- NULL
  computeDimension(X, N, D)
  
  if (( is.null(mu_init) || is.null(sigma_init) || is.null(p_init) ) && is.null(l_init) ) { 
    # Initialisation
    t <- matrix(runif(N*K_hat), nrow=N, ncol=K_hat)
    t <- t / rowSums(t)
    l = M_step_gaussien(X, t)
  }
  else {
    if (verifyShape_gaussien(X, N, D, K_hat, l_init))
      l <- l_init
    else
      l <- list(mu=alpha_init, sigma=beta_init, pi=p_init)
  }
  
  n <- 0
  logLikelihood <- logLikelihood_gaussien(X, l)
  l_buf <- l
  
  print("début SEM")
  t1 <- Sys.time()  
  while(whichStop==0 & n < param_stop | whichStop==1 & logLikelihood >10) {
    
    # E step
    t = E_step_gaussien(X, N, K_hat, D, l)
    
    # S step
    t <- S_step(t)
    
    # M step
    l_buf <- M_step_gaussien(X, t)
    
    buf_LogLikelihood <- logLikelihood_gaussien(X, l_buf)
    
    if (buf_LogLikelihood > logLikelihood) {
      l <- l_buf
      logLikelihood <- buf_LogLikelihood
    }
    
    n <- n+1
  }
  
  t2 <- Sys.time()
  print(t2-t1)
  print('fin algorithme SEM')
  return(l)
};








E_step_gaussien <- function(X, N, K, D, l) {
  mu <- l$mu
  sigma <- l$sigma
  p <- l$pi
  
  t <- matrix(0, nrow=N, ncol=K)
  for(k in seq(1, K)) {
    # Protection
    if ( sum(sigma[, , k]) < 10^-10 ) {
      sigma[, , k] = diag(vector(1, D))
    }

    t[, k] = p[k] * gaussianMixture_pdf(X, mu[, k], array(sigma[, , k], c(D, D, 1)), 1)
  }

  total <- rowSums(t)
for(k in seq(1, K)) {
  t[, k] = t[, k] / total
}

  return(t)
};







M_step_gaussien <- function(X, t) {
  # call by reference : http://stackoverflow.com/questions/2603184/r-pass-by-reference
  # and simply : http://www.r-bloggers.com/call-by-reference-in-r/
 
  N <- dim(t)[1]
  K <- dim(t)[2]
  if (is.matrix(X) || is.array(X))
    D <- dim(X)[2]
  else 
    D <- 1
  
  #*********************************************#
  #          gaussian custom stuff              #
  
  m <- matrix(0, nrow=D, ncol=K)
  s <- array(0, c(D, D, K))
  pi <- colSums(t)
  
  m <- t(X) %*% t
  
  for (k in seq(1, K)) {
    m[, k] <-  m[, k] / pi[k]
    
      # dx is X - m where m is an estimation of the mean
    dx <- as.matrix(X, nrow=N) - repmat(matrix(m[, k], ncol=D, nrow=1), N, 1)
    
    buf <- t(t[, k]) %*% (dx^2)
    
    if (length(buf) == 1)
      s[, , k] <- diag(as.matrix(buf)) / pi[k]
    else 
      s[, , k] <- diag(as.vector(buf)) / pi[k]
  }
  
  #*********************************************#
  
  pi <- pi / N
  
  # End of the function
  return(list(mu=m, sigma=s, pi=pi))
};






initiase_gaussien <- function(X, N, K, D, nbRepet, l) {
  # initialise gauss
  # mu, sigma and p are going to change
  
  t <- matrix(runif(N*K), nrow=N, ncol=K)
  t <- t / rowSums(t)
  
  l <- M_step_gaussien(X, t)
  logLikelihood <- logLikelihood_gaussien(X, l)
  
  l_buf <- l
  for (n in seq(1, nbRepet)) {
    t <- matrix(runif(N*K), nrow=N, ncol=K)
    t <- t / rowSums(t)
    
    l_buf <- M_step_gaussien(X, t)
    buf_LogLikelihood <- logLikelihood_gaussien(X, l_buf)
      
    if (buf_LogLikelihood > logLikelihood) {
      l <- l_buf
      logLikelihood <- buf_LogLikelihood
    }
  }
  
  # End of the function
  return(l)
};





verifyShape_gaussien <- function(X, N, D, K, l) {
  
  if(!is.matrix(l$mu)) {
    print("Please provide a D*K matrix for mu_hat")
    return(FALSE)
  }
  else if (dim(l$mu)[1] != D || dim(l$mu)[2] != K) {
    print("Please provide a D*K matrix for mu_hat")
    return(FALSE)
  }
  
  
  
  if(!is.array(l$sigma)) {
    print("Please provide a D*D*K array for sigma_hat")
    return(FALSE)
  }
  else if (dim(l$sigma)[1] != D || dim(l$sigma)[2] != D || dim(l$sigma)[3] != K) {
    print("Please provide a D*D*K matrix for sigma_hat")
    return(FALSE)
  }
  
  
  
  if(!is.vector(l$pi) || length(l$pi) != K) {
    print("Please provide a K vector for p_hat")
    return(FALSE)
  }
  
  
  return(TRUE)
};







logLikelihood_gaussien <- function(X, l) {
  logLikelihood <- log(gaussianMixture_pdf(X, l$mu, l$sigma, l$pi))
  logLikelihood <- sum(logLikelihood)
  
  return(logLikelihood)
};



classify_gaussien <- function(objectEM, X) {
  if (!is.matrix(X)) {
    print("Please provide a matrix for X")
    return(NULL);
  }
  
  p <- l$pi
  N <- dim(X)[1]
  D <- dim(X)[2]
  K <- dim(p)[1]
  if (is.null(K))
    K <- 1
  
  #****   Gaussian stuff  ****/
  mu <- l$mu
  sigma <- l$sigma
   
  t <- E_step_gaussien(X, p, N, K, l)
  #***************************/
  
  result <- Matrix(0, nrow=N, ncol=K+1)
  result[, 1] = attributeClass(t)
  result[, seq(2, K+1)] = t

  return(result)
};


#*************************************************#
#                    Objet retour                 #
#*************************************************#

# # beauté du modèle objet de R
# # http://cran.r-project.org/doc/contrib/Genolini-PetitManuelDeS4.pdf
# setClass(
#   Class = "Gaussian_EM",
#   representation = representation(
#     type="character",
#     K = "numeric",
#     D = "numeric",
#     mu_hat = "matrix",
#     sigma_hat = "array",
#     p_hat = "vector"
#   ),
#   
#   prototype=prototype(
#     K = 1,
#     type="norm"
#   )
# )
# 
# setGeneric("getType",
#            function(object){standardGeneric("getType")}
# )
# setMethod("getType", "Gaussian_EM",
#           function(object){
#             return(object@type)
#           }
# )
# 
# setGeneric("getK",
#   function(object){standardGeneric("getK")}
# )
# setMethod("getK", "Gaussian_EM",
#   function(object){
#     return(object@K)
#   }
# )
# 
# setGeneric("getD",
#   function(object){standardGeneric("getD")}
# )
# setMethod("getD", "Gaussian_EM",
#   function(object){
#     return(object@D)
#   }
# )
# 
# setGeneric("getMu",
#            function(object){standardGeneric("getMu")}
# )
# setMethod("getMu", "Gaussian_EM",
#   function(object){
#     return(object@mu_hat)
#   }
# )
# 
# setGeneric("getSigma",
#            function(object){standardGeneric("getSigma")}
# )
# setMethod("getSigma", "Gaussian_EM",
#           function(object){
#             return(object@sigma_hat)
#           }
# )
# 
# setGeneric("getP",
#            function(object){standardGeneric("getP")}
# )
# setMethod("getP", "Gaussian_EM",
#           function(object){
#             return(object@p_hat)
#           }
# )
