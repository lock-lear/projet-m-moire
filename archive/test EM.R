#********************************#
#         TESTS INTEGRATION  #####
#********************************#

#******************#
#      D1 K1   -----
#******************#

D <- 1
K <- 1
a <- matrix(c(4), nrow=1)
b <- matrix(c(10), nrow=1)
p <- c(1)
x <- betaMixture_random(200, a, b, p)

l <- EM("beta", x, K, niter=100)
plotEM(l, x)




D <- 1
K <- 1
N <- 100
m <- matrix(c(0), nrow=1)
s <- array(1, c(1, 1, 2))
p <- c(1)
x = matrix(rnorm(N, mean=0, sd=1))

l <- EM("norm", x, K, niter=100)
plotEM(l, x)


#******************#
#      D2 K2   -----
#******************#

D <- 2
K <- 2
N <- 200
a <- matrix(c(1, 1, 3, 3), nrow=D)
b <- matrix(c(5, 5, 3, 3), nrow=D)
p <- c(0.5, 0.5)
x <- betaMixture_random(N, a, b, p)

l <- EM("beta", x, K, niter=100, niter_init=10)
plotEM(l, x)





#******************#
#      D1 K2    ----
#******************#

D <- 1
K <- 2
N <- 300
a <- matrix(c(2, 10), nrow=1)
b <- matrix(c(10, 2), nrow=1)
p <- c(0.5, 0.5)
x <- matrix(betaMixture_random(N, a, b, p), nrow=N, ncol=1)

l <- EM("beta", x, K, niter=100, niter_init=100)
plotEM(l, x)




D <- 1
K <- 2
N <- 200
m <- matrix(c(0, 10), nrow=D, ncol=K)
s <- array(1, c(D, D, K))
p <- c(0.5, 0.5)
x <- gaussianMixture_random(N, D, m, s, p)

l <- EM("norm", x, K, niter=100, niter_init=20)
plotEM(l, x)

#******************#
#      D2 K1   -----
#******************#

D <- 2
K <- 1
N <- 200
a <- matrix(c(1, 5), nrow=D)
b <- matrix(c(5, 1), nrow=D)
p <- c(1)
x <- betaMixture_random(N, a, b, p)

l <- EM("beta", x, K, niter=100, niter_init=1)
l <- EM("beta", matrix(x[, 2]), K, niter=100, niter_init=1)
plotEM(l, x)



D <- 3
K <- 1
N <- 100
m <- matrix(c(0, 5, 10), nrow=D, ncol=K)
s <- array(1, c(D, D, K))
s[, , 1] = as.matrix(Diagonal(D))
p <- c(1)
x <- gaussianMixture_random(N, D, m, s, p)

l <- EM("norm", x, K, niter=200)
plotEM(l, x)



#******************#
#       IRIS       #
#******************#

x <- iris[, seq(1, 4)]
for (k in seq(1, 4))
  x[, k] = x[, k] / max(x[, k])
N <- dim(x)[1]
D <- dim(x)[2]
K <- 3

l <- EM("beta", x, K, niter=100)
plotEM(l, x)



x <- iris[, seq(1, 4)]
N <- dim(x)[1]
D <- dim(x)[2]
K <- 3
l <- EM("norm", x, K, niter=100)
plotEM(l, x)
