# Pour rajouter une distribution :
#   -> Dans EM
#   -> Dans SEM
#   -> Dans E step
#   -> Dans M step
#   -> Dans floglikelihood
#   -> Dans classify plus tard
folder = "C:\\Users\\Cl�ment\\Desktop\\M�moire Gaussian Mixture\\v3\\"
source(paste(folder, "EM gaussien.R", sep=""))
source(paste(folder, "EM gamma.R", sep=""))
source(paste(folder, "EM beta.R", sep=""))

EM <- function (distribution, X, K_hat, niter, niter_init=300) {
  ## Meta fonction
  
  ## Preprocessing steps
  
    # Is the distribution covered by the package?
  if ! distribution %in% c("normal", "beta", "gamma") {
    print("Distribution is not recognized")
    return
  }
  
    # Is X a matrix?
  if(!is.matrix(X)) {
    print("Error : X must be dataframe or matrix")
    return
  }

# Initialisation
  l     <- NULL
  func  <- NULL
  N     <- 0
  D     <- 0
  computeDimension(X, N, D)
  
  switch(distribution,
         normal={
            l <- list(lkh=, E_step=specic_E_step_gaussien, M_step=specic_M_step_gaussien)
         },
         beta={
            X <- colMeans(mtcars)
            outer(X, X)
         },
         gamma={
            X <- colMeans(mtcars)
            outer(X, X)
         },
         stop("Enter something that switches me!")
         return;
  )
  
  
  print("debut initialisation")
  t1 <- Sys.time()
  l <- initialise(distribution, X, N, K_hat, D, niter_init)
  
  l <- SEM(distribution, X, K_hat, niter_init, linit=l)    
  print("fin initialisation")
  print(Sys.time() - t1)
  
  
  n=0
  logLikelihood <- flogLikelihood(distribution, X, l)

  print("debut EM")
  t1 <- Sys.time()
  while(n < niter) {
    # E step
    t = E_step(distribution, X, N, K_hat, D, l)
    
    # M step
    l <-  M_step(distribution, X, t)

    # break condition
    n <- n+1
  } 
  
  print("fin EM")
  print(Sys.time() - t1) 
  
  return(c(l, list(type=distribution, D=D, K=K_hat, N=N)))
}




SEM <- function (distribution, X, K_hat, niter, linit) {
  # X must be a N * D matrix
  # K_hat the number of estimated cluster
  
  l <- NULL
  N <- 0  
  D <- 0
  computeDimension(X, N, D)
  
  # Initialisation
  l <- linit
  
  logLikelihood <- flogLikelihood(distribution, X, l)
  l_buf <- l
  
  
  # Estimation part
  print('début algorithme SEM')
  n=0
  while(n < niter) {
    # E step
    t = E_step(distribution, X, N, K_hat, D, l)

    # S step
    t <- S_step(t)
    
    # M step
    l_buf <- M_step(distribution, X, t)

    # performing new modèle
    buf_logLikelihood <- flogLikelihood(distribution, X, l_buf)
    if (logLikelihood < buf_logLikelihood) {
      logLikelihood <- buf_logLikelihood
      l <- l_buf
    }
    
    # break condition
    n <- n+1
  }
  
  
  # End
  ('fin algorithme SEM')
  return(l)
};





flogLikelihood <- function(distribution, X, l) {
  
  logLikelihood <- 0
  
  if (distribution == "beta") {
    logLikelihood <- log(mixture_pdf("beta", X, l$pi, l$a, l$b))
  }
    
  else if (distribution == "norm") {
    #logLikelihood <- log(mixture_pdf("norm", X, l$pi, l$mu, l$sigma))
    
    sig = matrix(0, nrow=dim(X)[2], ncol=length(l$pi))
    for (k in seq(1, length(l$pi))){
      sig[, k] = diag( as.matrix(l$sigma[, , k]) )
    }
    logLikelihood <- log(cppGMixture_pdf(X, l$mu, sig, l$pi))
  }
  
  else if (distribution == "gamma") {
    logLikelihood <- log(gammaMixture_pdf(X, l$alpha, l$beta, l$p))
  }
  
  logLikelihood <- sum(logLikelihood)
  return(logLikelihood)
};



initialise <- function(distribution, X, N, K, D, nbRepet) {
  
  t <- matrix(runif(N*K), nrow=N, ncol=K)
  t <- t / rowSums(t)
  
  l <- M_step(distribution, X, t)
  
  logLikelihood <- flogLikelihood(distribution, X, l)

  for (n in seq(1, nbRepet)) {
    t <- matrix(runif(N*K), nrow=N, ncol=K)
    t <- t / rowSums(t)
    
    l_buf <- M_step(distribution, X, t)
    buf_LogLikelihood <- flogLikelihood(distribution, X, l_buf)
    
    if (buf_LogLikelihood > logLikelihood) {
      l <- l_buf
      logLikelihood <- buf_LogLikelihood
    }
  }
  
  return(l)
};




E_step <- function(func, X, l) {
  
  t <- matrix(0, nrow=l$N, ncol=l$K)
  t <- func$E_step(t, X, l)
  
  total <- rowSums(t)
  for(k in seq(1, l$K)) {
    t[, k] = t[, k] / total
  }
  
  return(t)
};




M_step <- function(func, X, t) {

  l = func$M_step(t, X, l)
  return(l) 
}





S_step <- function(t) {
  
  N <- dim(t)[1]
  K <- dim(t)[2]

  #return(matrix(apply(t, 1, function(x) sample(1:K, 1, prob=x)), nrow=N))
  if (K==1)
    return(matrix(1, ncol=1, nrow=N))
  
  
  s <- matrix(runif(N), nrow=N, ncol=1)  
  return(cS_step(t, s))
};



C_step <- function(t) {
  
  N <- dim(t)[1]
  K <- dim(t)[2]
  
  buf_t <- matrix(0, nrow=N, ncol=K)
  
  for (n in seq(1, N)) {
    buf_t[n, which.max(t[n, ])] = 1
  }
  
  return(buf_t);
};


repmat = function(X,m,n){
  ##R equivalent of repmat (matlab)
  mx = dim(X)[1]
  nx = dim(X)[2]
  matrix(t(matrix(X,mx,nx*n)),mx*m,nx*n,byrow=T)
};


attributeClass <- function(t) {
  # t is a N by K matrix
  
  N <- dim(t)[1]
  k <- dim(t)[2]
  K <- matrix(0, nrow=N, ncol=1)

  for (n in seq(1, N)) 
    K[n, 1] <- which.max(t[n,])
    
  return(K)
}


classify <- function(l, X) {
  
  if (is.data.frame(X))
    X <- data.matrix(X)
  
  else if (!is.matrix(X)) {
    print("Please provide a matrix for X")
    return(NULL);
  }
  p <- l$pi
  N <- dim(X)[1]
  D <- dim(X)[2]
  K <- length(p)
  t <- NULL

  #****   gamma stuff  ****/

  
  if (l$type == "norm") {
    t <- E_step("norm", X, N, K, D, l)
  }
  
  else if (l$type == "beta") {
    t <- E_step("beta", X, N, K, D, l)
  }
  
  else if (l$type == "gamma") {
    t <- E_step("gamma", X, N, K, D, l)
  }
  
  else {
    print("Object not recognized. Classification impossible")
    return;
  }
  #************************/
  
  result <- Matrix(0, nrow=N, ncol=K+1)
  result[, 1] = attributeClass(t)
  result[, seq(2, K+1)] = t
  
  names =  "attribution"
  for(k in 1:dim(t)[2])
    names = c(names, paste(c("classe ", k), collapse = " "))
  colnames(result) = names

  # http://edutechwiki.unige.ch/fr/R
  #buf <- matrix(t(t[, -1]), nrow=2, ncol=200)
  #rownames(buf) <- colnames(t)[-1]
  #barplot(buf, legend=rownames(buf), main="Diagramme empilé", xlab="n° échantillon")
  
  return(result)
};

plotEM <- function(listEM, X) {
  # For now we assume that the right set of data is provided
  
  D <- listEM$D
  K <- listEM$K
  N <- listEM$N
  
  class <- classify(listEM, X)
  lcolor = rainbow(K)
  
  if (listEM$type == "beta") {
    fpdf <- betaMixture_pdf
    param1 <- listEM$a
    param2 <- listEM$b
  }
  
  else if (listEM$type == "gamma") {
    fpdf <- gammaMixture_pdf
    param1 <- listEM$alpha
    param2 <- listEM$beta
  }
  
  else if (listEM$type == "norm") {
    fpdf <- cppGMixture_pdf#gaussianMixture_pdf
    param1 <- listEM$mu
    param2 = matrix(0, nrow=dim(X)[2], ncol=length(l$pi))
    for (k in seq(1, length(l$pi))){
      if (length(l$sigma[, , k]) == 1)
        param2[, k] = l$sigma[, , k]
      else
        param2[, k] = diag(l$sigma[, , k])
    }
  }
  
  else {
    print("Erreur")
  }
  
  par(mfrow=c(D, D))
  for (i in seq(1, D)) {
    for (j in seq(1, D)) {
      
      if (i == j) {
        t = seq(min(X[, i]), max(X[, i]), (max(X[, i]) - min(X[, i]))/1000)
        
        h <- hist(X[, i], breaks=25, main=paste("Histogram on dimension ", j))

        y <- fpdf(as.matrix(t, ncol=D), matrix(param1[i, ], ncol=K), matrix(param2[i, ], ncol=K), listEM$pi)

        lines(t, y * max(h$counts) / max(y), col="red")
      }
      
      else if (i < j) {
        plot(X[, i], X[, j], col="white", main=paste("dimension", i, " versus dimension ", j))
        for (k in seq(1, K))
          points(X[class[, 1]==k, i], X[class[, 1]==k, j], col=lcolor[k])
      }
      
      else {
        # Ne rien faire ici
        plot.new()
        if (i>1 && i==D && j == 1) {
          names = c("distribution estimée")
          cols =c("red")
          
          for(k in seq(1, K)) {
            names = c(names, paste("classe ", k))
            cols = c(cols, paste(lcolor[k]))
          }
          print(names)
          legend("topright", names, col=cols, lwd=2)
        }
      }
    }
  }
};