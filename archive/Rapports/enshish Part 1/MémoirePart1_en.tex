\documentclass[a4paper]{article}

%\usepackage[utf8]{inputenc}

%------------------------------------------------
\usepackage{amsfonts,amstext,amsmath,amssymb}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[english]{babel}
%\usepackage[T1]{fontenc}
%\usepackage[table]{xcolor}

\usepackage{algorithm}
\usepackage{algorithmic}

\usepackage{graphicx}
%------------------------------------------------
% Sets
\newcommand{\R}{\mathbb{R}}
\newcommand{\Rp}{{\mathbb{R}^p}}
\newcommand{\Rd}{{\mathbb{R}^d}}

% Lettre en gras
\newcommand{\ba}{\mathbf{a}}
\newcommand{\bb}{\mathbf{b}}
\newcommand{\bc}{\mathbf{c}}
\newcommand{\bg}{\mathbf{g}}
\newcommand{\be}{\mathbf{e}}
\newcommand{\bp}{\mathbf{p}}
\newcommand{\bu}{\mathbf{u}}
\newcommand{\bx}{\mathbf{x}}
\newcommand{\bX}{\mathbf{X}}
\newcommand{\bZ}{\mathbf{Z}}
\newcommand{\bz}{\mathbf{z}}
\newcommand{\bt}{\mathbf{t}}
\newcommand{\by}{\mathbf{y}}

\newcommand{\balpha}{\boldsymbol{\alpha}}
\newcommand{\bbeta}{\boldsymbol{\beta}}
\newcommand{\bsigma}{\boldsymbol{\sigma}}
\newcommand{\bDelta}{\boldsymbol{\Delta}}
\newcommand{\bepsilon}{\boldsymbol{\epsilon}}
\newcommand{\bGamma}{\boldsymbol{\Gamma}}
\newcommand{\blambda}{\boldsymbol{\lambda}}
\newcommand{\bmu}{\boldsymbol{\mu}}
\newcommand{\bpi}{\boldsymbol{\pi}}
\newcommand{\bphi}{\boldsymbol{\phi}}
\newcommand{\brho}{\boldsymbol{\rho}}
\newcommand{\btheta}{\boldsymbol{\theta}}
\newcommand{\bTheta}{\boldsymbol{\Theta}}
\newcommand{\bvarepsilon}{\boldsymbol{\varepsilon}}

%opening
\title{Part 1 : On the study of mixtures}
\author{Cl\'ement ELVIRA}

\begin{document}

\maketitle

\begin{abstract}
This document is the first part of an essay within the context of the masters degree at the university of Lille 1. It aims to introduce the expectation-maximization algorithm (EM), which is an iterative method to estimates parameters of a mixture, by maximizing the log-likelihood. The estimation of a few types of mixture will then be presented.
\end{abstract}


\section{A Short Course on Mixture Modeling}
Let ${\bx}=\{ {\bx}_1,...,{\bx}_n\}$ be $n$ independent vectors in $\mathbb{R}^d$ such that each
${\bx}_i$ arises from a probability distribution with density
\begin{equation}
  f({\bx}_i|\theta) = \sum_{k=1}^K p_k h({\bx}_{i}| \blambda_{k},\balpha)
\end{equation}
where the $p_k$'s are the mixing proportions ($0<p_k<1$ for all $k=1,...,K$ and
$p_1+...+p_K=1$), $h(\cdot| \blambda_{k},\balpha)$ denotes a $d$-dimensional distribution parameterized
by $\blambda_k$ and $\balpha$. The parameters $\balpha$ do not depend from $k$ and is common
to all the components of the mixture.
The vector parameter to be estimated is $\theta=(p_1,\ldots,p_K,\blambda_1,\ldots,\blambda_K, \balpha)$
and is chosen to maximize the observed log-likelihood
\begin{equation}
  \label{eq:vraisemblance}
  L(\theta|\bx_1,\ldots,\bx_n)=\sum_{i=1}^n \ln \left(\sum_{k=1}^K p_k h(\bx_i,\blambda_k, \balpha)\right).
\end{equation}

We are now going to introduce the indicator vectors or {\em labels}
${\bz}=\{ {\bz}_1,...,{\bz}_n\}$, with ${\bz}_i=(z_{i1},\ldots,z_{iK})$,

\[
z_{ik}=  \left\{
\begin{array}{rcl}

& 1 & \text{ if } {\bx}_i \; \text{is arising from the kth distribution} \\

 & 0 &  \text{else}\\

\end{array}
\right.
\]


Whereas ${\bz}$ is unknown in this context, we are going to use it to estimate these parameters, by the Expectation Maximization algorithm
\cite{Dempster97} or by a stochastic version of EM called SEM (see \cite{McLachlanPeel00}), or by a k-means like algorithm called CEM.


\section{The Algorithms}

\subsection{Construction}

The question is : If we assume that the $N$ points ${\bx}$ rise
from a mixture distribution of K components, how to estimate the parameters $(\theta_k)_{k \in \{1..K\}}$ and the proportion $\pi = (\pi_1..\pi_k)$? We will call in this section $\theta$ the set of all unknown parameters.\\

To perform that, the EM algorithm will try to estimated log-likelihood. Two steps are necessary :
\begin{itemize}
	\item An Expectation step, which computes an expected loke-likelihood from the current parameters.
	\item A Maximization step, which will try to compute new parameters to maximize the expected log-likelihood.  
\end{itemize} 

We use to that the second family of parameters, ${\bz}$, seen in section 1, called the missing values. So the density becomes :
\begin{center}
$f(x, z, \theta) = f(z\mid x, \theta)f(x, \theta)$ \\
and the log-likelihood : \\
$ln\left[f(x, \theta)]\right]=ln\left[f(x, z, \theta)\right] - ln\left[f(z\mid x, \theta)\right]$
\end{center}

In this equation, $\theta$ is still unknown. So we start from an initial value $\theta_0$, and we sum over $z\mid x, \theta $ :
\begin{center}
$E_{z \mid x, \theta_0}\left[ln\left(ln(x, \theta)\right)\right]=ln\left(ln(x, \theta)\right)$ \\
$E_{z\mid x, \theta}\left[ln(f(x, z, \theta)\right]=Q(\theta, \theta_0)$ \\
$E_{z\mid x, \theta}\left[ln(f(z,\mid z, \theta)\right]=H(\theta, \theta_0)$
\end{center}

\subparagraph*{}
Or $H(\theta, \theta_0)=\int ln(\left[f(z, \mid x, \theta)\right]f(z, \mid x, \theta_0)dz$ 

We define then the quantity :
\begin{equation}
-(H(\theta, \theta_0)-H(\theta_0, \theta_0))=-\int f(z\mid x, \theta)ln\frac{f(z\mid x, \theta)}{f(z\mid x, \theta_0)}dz
\end{equation}

This term is defined as the Kukkback Leibler divergence (\cite{kullback1951}). It can be seen as a "distance" between two distributions, and one can show that it is always positive.

\subparagraph*{}
We choose now $\theta_1$ such as $Q(\theta_1, \theta_0)>Q(\theta_0, \theta_0)$. Then :
\begin{eqnarray}
  ln\left[l(\theta_1\mid x)\right])-ln\left[l(\theta_0\mid x)\right]) & = & Q(\theta_1, \theta_0)-H(\theta_1, \theta_0)-(Q(\theta_0, \theta_0)-H(\theta_0, \theta_0)) \nonumber\\
   & = & \underbrace{Q(\theta_1, \theta_0)-Q(\theta_0, \theta_0)}_{\geq0 \quad \text{by the choice of } \theta_1}-\underbrace{(H(\theta_1, \theta_0)-H(\theta_0, \theta_0))}_{\geq 0 \quad \text{the divergence}} \nonumber
\end{eqnarray}

We obtain :
\begin{center}
$L(\theta_1\mid x) \geq L(\theta_0\mid x)$
\end{center}
Thus, we can construct iteratively a sequence of parameters which makes the log-likelihood increasing. Knowing that it is bounded, we are sure about its convergence to a maximum. However, this maximum can be local.

\paragraph*{}
We now introduce the algorithm :

\begin{algorithm}
\caption{Converging to a maximum L with the accuracy $\epsilon$}
\begin{algorithmic} 
\REQUIRE $\epsilon, \theta_{init}$
\STATE $\theta_{courant} \leftarrow \theta_{init}$
\STATE $L_{courant} = L(\theta_{courant}|x)$
\STATE $L_{ancien} = -1$ (convention)
\STATE $N_{iter} \leftarrow 0$
\WHILE{$L_{courant}-L_{ancien} > \epsilon$}
	\STATE $L_{ancien} = \theta_{courant}$
	\STATE $\theta_{courant} = {argmax}_{ \begin{array}{l} \theta \end{array}} \left( Q \left( \theta, \theta_{courant} \right) \right)$
	\STATE $N_{iter} \leftarrow N_{iter}+1$
\ENDWHILE
\RETURN $\theta$
\end{algorithmic}
\end{algorithm}

For the sake of simplicity, we call in this section $(k_i)$ the family defined by
\begin{center}
$\forall i \in \{1...N\}$, $k_i = {arg}_k (z_i^k \neq 0)$
\end{center}

We then have to a find an expression of Q. First :
\[
\begin{array}{r c l}
f(x, z, \theta) & = & \prod_{i=1}^N f(x_{i}, \theta_{k_i}) \\
 & = & \prod_{i=1}^N f(x_i | \theta_{k_i} ) f(\theta_{k_i}) \\
  & = & \prod_{i=1}^N p_k f_{k_i}(x_i, \theta_{k_i}) \\
\end{array}
\]
Thus, when using the expectation :
\[
\begin{array}{r c l}
Q(\theta, \theta_0)  & = & \sum_{i=1}^N E \left[ ln( \alpha_{k_i}) \right] + \sum_{i=1}^N E \left[ ln( f_{k_i}(\alpha_{k_i})) \right] \\
& & \\
& = & \sum_{i=1}^N \sum_{k=1}^K p(k|x_i, \theta_i^0)ln(p_k) + \sum_{i=1}^N \sum_{k=1}^K p(k|x_i, \theta_i^0)ln(f_k(x_i, \theta_k))
\end{array}
\]
\subsection{Final expression of the log-likelihood}

We obtain finally, if we use partial derivative over $p_k$, that whatever the distribution :
\[
\left.
\begin{array}{r c l}
% ------------------------------------------------------------------------------- %
 t_{i, k}^{iter+1} & = & \frac{p(x_i|k, \theta^{iter}) p(k|\theta^{iter})}{\sum_{l=1}^K p(x_i|l, \theta^{iter}) p(l|\theta^{iter})}\\
 & = & \frac{f_k(x_i, \theta_k^{iter}) p_k^{iter}}{\sum_{l=1}^K f_l(x_i, \theta_l^{iter}) p_l^{iter}}\\
 \end{array}
\right.
\]
and
\[
\left.
\begin{array}{r c l}
% ------------------------------------------------------------------------------- %
p_k^{iter+1} & = & \frac{1}{N} \sum_{i=1}^N p(k|x_i, \theta^{courant} \nonumber \\
% ------------------------------------------------------------------------------- %
 & = & \frac{1}{N} \sum_{i=1}^N t_{i, k}^{iter+1}
\end{array}
\right.
\]

Using the $t_{i, k}^m$. the log-likelihood becomes :
\begin{equation} \label{eq:finalLogVraisemblance}
L\left( \theta | x_1,...x_n, t^m \right) = \sum_{i=1}^n \sum_{k=1}^K t_{i, k}^m  ln \left[ p_kh\left( x_i, \lambda_k, \alpha \right) \right]
\end{equation}

\subsection{Final algorithm}

\begin{algorithm}
\caption{Converging to a maximum L with the accuracy $\epsilon$}
\begin{algorithmic} 
\REQUIRE $\epsilon, \theta_{init}$
\STATE $\theta_{courant} \leftarrow \theta_{init}$
\STATE $N_{iter} \leftarrow 0$
\WHILE{$L_{courant}-L_{ancien} > \epsilon$}
	\STATE \textbf{E step} : compute the $t_{i, k}$ using the current $\theta$
	\STATE \textbf{M step} : Estimate the new version of parameters $\theta$ using the conditional probabilities $t_{i, k}$, and update the proportion $\pi$
	\STATE $N_{iter} \leftarrow N_{iter}+1$
\ENDWHILE
\RETURN $\theta$
\end{algorithmic}
\end{algorithm}

\subsection{the SEM algorithm}

The SEM algorithm is a stochastic version of EM incorporating between the E and M steps a restoration of the unknown component labels $\bz_i$, $i=1,\ldots,n,$ by drawing them at random
from their current conditional distribution. Starting from an initial parameter $\theta^0$, an
iteration of SEM consists in three steps.
\begin{itemize}
\item {\bf E step:} The conditional probabilities $t^m_{ik}$ $(1 \leq i \leq n, 1 \leq k \leq
  K)$ are computed for the current value of $\theta$ as done in the E step of EM.
\item {\bf S step:} Generate labels ${\bz}^m=\{ {\bz}^m_1,...,{\bz}^m_n\}$
  by assigning each point ${\bx}_i$ at random to one of the mixture
  components according to the categorical distribution with parameter $(t^m_{ik}, 1 \leq k \leq K)$.
\item {\bf M step:} The m.l. estimate of $\theta$ is updated using the generated labels
  by maximizing
\begin{equation} \label{eq:mStepSEM}
  L(\theta| {\bx}_{1},\ldots,{\bx}_{n}, {\bt}^m)
    =\sum_{i=1}^{n}\sum_{k=1}^{K} z_{ik}^m \ln \left [p_{k} h({\bf x}_{i}|\blambda_{k},\balpha)\right],
\end{equation}
\end{itemize}

SEM does not converge point wise.  It generates a Markov chain whose stationary distribution is more or less concentrated around the m.l. parameter estimator. A natural way to estimate is to consider the parameter value leading to the highest likelihood in a SEM sequence.

\subsection{the CEM algorithm}

\section{Estimation of few distribution}

\subsection{Gaussian mixture}

For its convenience, we will study first a Gaussian density. The schema will always be the same : we begin first with the univariate model, and then we extend the results for multivariate distribution.

	\subsubsection{M step of the univariate model}

the univariate model need not to introduction the missing values, we can obtain directly, using the log likelihood :

\[
\begin{array}{r c l}
%---------------------------------------------------------------%
L \left( \theta | x_1...x_n \right) & = & \sum_{i=1}^n \ln{\frac{1}{\sqrt{2\pi \sigma^2}} \exp \left( -\frac{(x_i - \mu)^2}{2\sigma^2} \right)} \\
%---------------------------------------------------------------%
 & = &  - \frac{n}{2} \ln{\left( \pi \right)} - \frac{n}{2} \ln{\left( 2 \sigma^2 \right)} + \frac{1}{2 \sigma^2} \sum_{i=1}^n (x_i - \mu)^2
 %---------------------------------------------------------------%
\end{array}
\]

and the gradient :
\[
\left\{
\begin{array}{r c l}
%---------------------------------------------------------------%
\frac{\partial{L}}{\partial{\mu}} & = & \sum_{i=1}^{N} \frac{(x_i-\mu)}{{\sigma}^2} \\
%---------------------------------------------------------------%
\frac{\partial{L}}{\partial{\sigma}} & = & \sum_{i=1}^{N} \left[\frac{{(x_i-\mu)}^2}{{\sigma}^3} - \frac{1}{\sigma}\right] \\
%---------------------------------------------------------------%
\end{array}
\right.
\]

And we have directly, if we set the gradient to zero, without forgetting to verify that both of them correspond to a maximum.

\[
\left\{
\begin{array}{r c l}
\hat{\mu}_k & = & \frac{1}{N} \sum_{i=1}^{N}x_i \\

{\hat{\sigma}_k}^2 & = & \frac{1}{N} \sum_{i=1}^{N} {\left( x_i-\mu_k \right) }^2
\end{array}
\right.
\]



	\subsubsection{M step of the multivariate model}
	
We will considerer here multivariate and unidimensional model. The density becomes :

\begin{equation}
f(x) = \sum_{k=0}^{K} p_k \left[ \frac{1}{\sqrt{2\pi}\sigma_k} \exp{\left( -\frac{(x_i-\mu_k)^2}{2{\sigma_k}^2} \right)} \right]
\end{equation}  

Once more, we obtain, developing the log-likelihood :

\[
\begin{array}{r c l}
L = \sum_{i=1}^{n} \sum_{k=1}^{K} t^m_{ik} \left[ \ln{p_k} +  \left(-\frac{1}{2} \pi \ln{2} - \ln{\sigma_k} - \frac{\left(x_i - \mu_k\right)^2}{2{\sigma_k}^2}\right) \right]
\end{array}
\]

and the gradient, defined for each $k$  :

\[
\left\{
\begin{array}{r c l}
%---------------------------------------------------------------%
\frac{\partial{L}}{\partial{\mu_k}} & = & \sum_{i=1}^{N}t_{ik}^{m}\frac{(x_i-\mu_k)}{{\sigma_k}^2} \\
%---------------------------------------------------------------%
\frac{\partial{L}}{\partial{\sigma_k}} & = & \sum_{i=1}^{N} t_{ik}^{m}\left[\frac{{(x_i-\mu_k)}^2}{{\sigma_k}^3} - \frac{1}{\sigma_k}\right] \\
%---------------------------------------------------------------%
\end{array}
\right.
\]

Then

\[
\left\{
\begin{array}{r c l}
%---------------------------------------------------------------%
\frac{\partial{L}}{\partial{\mu_k}} = 0  & & \hat{\mu}_k =\left( \frac{1}{\sum_{i=1}^{N}t_{i,k}^{m}} \right) \sum_{i=1}^{N}t_{i,k}^{m} x_i\\
%---------------------------------------------------------------%
& \Leftrightarrow & \\
%---------------------------------------------------------------%
\frac{\partial{L}}{\partial{\sigma_k}} = 0  & & \hat{\sigma}_k =\left( \frac{1}{\sum_{i=1}^{N}t_{i,k}^{m}} \right) \sum_{i=1}^{N}t_{i,k}^{m} x_i\\
%---------------------------------------------------------------%
\end{array}
\right.
\]

We can see that, finally, these estimators correspond to estimators of an univariate model, pondered by the conditional probabilities.


	\subsubsection{Extension to multidimensional variable}

We can easily extend these results to mixture of Gaussian vectors, if we assume that within each vector, the dimensions are independent. Density is given by :

\begin{equation}
f(x) = \sum_{k=0}^{K} p_k \prod_{j=1}^{d} \left[ \frac{1}{\sqrt{2\pi}\sigma_k} \exp{\left( -\frac{(x_i^j-\mu_k)^2}{2{\sigma_k}^2} \right)} \right]
\end{equation} 

and the estimators :
\[
\left\{
\begin{array}{r c l}
%---------------------------------------------------------------%
\hat{\mu}_k^j & = & \left( \frac{1}{\sum_{i=1}^{N}t_{i,k}^{m}} \right) \sum_{i=1}^{N}t_{i,k}^{m} x_i^j \\
%---------------------------------------------------------------%
\\
%---------------------------------------------------------------%
\left( \hat{\sigma}_k^j \right) ^2 & = & \left( \frac{1}{\sum_{i=1}^{N}t_{i,k}^{m}} \right) \sum_{i=1}^{N}t_{i,k}^{m}{ \left(x_i^j-\mu_k^j \right)}^2
%---------------------------------------------------------------%
\end{array}
\right.{, \forall k \in \left[1, K \right] , \forall j \in \left[1, d \right]}
\]



\subsection{Beta mixture}

	\subsubsection{M step of the univariate model}
	
Once more, we begin with an unidimensional and univariate model.
	
	\subsubsection{M step of the multivariate model}	
	
Le us note the two families  $\alpha = (\alpha_1...\alpha_K)$ and $\beta = (\beta_1...\beta_K)$. The density is given by :

\begin{center}
$f(x_i, \theta) = \sum_{k=1}^K \left(\frac{(x_i)^{\alpha_k}\left(1-x_i\right)^{\beta_k-1}}{B(\alpha_k, \beta_k)} \right)$
\end{center}

and the log likelihood (\ref{eq:finalLogVraisemblance})
\begin{eqnarray} 
% ---------------------------------------------------------- 
L & = & \sum_{i=1}^{n} \sum_{k=1}^{K} \left( t^m_{ik}.\ln \left[ p_k . \frac{x_i^{\alpha_k-1}(1-{x_i}^{\beta_k-1})}{B(\alpha_k,\beta_k)}\right] \right) \nonumber %%\\
%------------------------------------------------------------ 
\end{eqnarray}

Then
\begin{equation} \label{eq:logVraisBeta2}
L = \sum_{i=1}^{n} \sum_{k=1}^{K} t_{i, k}^m \left( ln(p_k) + (\alpha_k-1)ln(x_i) + (\beta_k-1)ln(1-x_i)-lnB(\alpha_k, \beta_k) \right)
\end{equation}
\\
Using partial derivative over parameters, we have, for each k in $\lbrace 1...K\rbrace$ :

\[
\left\{
\begin{array}{r c l}
% ------------------------------------------------------------------------------- %
\frac{\partial{L}}{\partial{\alpha_k}} & = & \sum_{i=1}^nt_{i,k}^{m} \left( ln\left( x_i \right) -  \frac{1}{B(\alpha_k,\beta_k)} \frac{\partial{B(\alpha_k,\beta_k)}}{\partial{\alpha_k}} \right) \nonumber \\
% ------------------------------------------------------------------------------- %
\frac{\partial{L}}{\partial{\beta_k}} & = & \sum_{i=1}^nt_{i,k}^{m} \left( ln\left( 1-x_i \right) -  \frac{1}{B(\alpha_k,\beta_k)} \frac{\partial{B(\alpha_k,\beta_k)}}{\partial{\beta_k}} \right) \nonumber
% ------------------------------------------------------------------------------- %
\end{array}
\right.
\]

Once more we have, setting the gradient to 0 :

\[
\left\{
\begin{array}{r c l}
% ------------------------------------------------------------------------------- %
\frac{1}{B(\alpha_k,\beta_k)} \frac{\partial{B(\alpha_k,\beta_k)}}{\partial \alpha_k} & = & \frac{1}{\sum_{i=1}^n t_{i, k}^m} . \sum_{i=1}^n t_{i,k}^{m} ln \left( x_i \right) \nonumber\\
% ------------------------------------------------------------------------------- %
\frac{1}{B(\alpha_k,\beta_k)} \frac{\partial{B(\alpha_k,\beta_k)}}{\partial \beta_k} & = & \frac{1}{\sum_{i=1}^n t_{i, k}^m} . \sum_{i=1}^n t_{i,k}^{m} ln \left(1-x_i \right) \nonumber
% ------------------------------------------------------------------------------- %
\end{array}
\right.
\]

But this result is not numerically convenient. So we will use the relashionship between the derivative of the beta function and the digamma function (\cite{BetaLoi}) :
\begin{eqnarray}
\frac{\partial{B(x,y)}}{\partial{x}} = B(x,y)(\frac{\Gamma'(x)}{\Gamma(x)}-\frac{\Gamma'(x+y)}{\gamma(x+y)} = B(x,y)(\psi(x)-\psi(x+y)) \nonumber
\end{eqnarray}
Where $\psi$ is the digamma function. \\

Then :

\[
\left\{
\begin{array}{r c l}
% ------------------------------------------------------------------------------- %
\left( \psi(\alpha_k) - \psi(\alpha_k + \beta_k) \right) & = & \frac{1}{\sum_{i=1}^n t_{i, k}^m} . \sum_{i=1}^n t_{i,k}^{m} ln \left( x_i \right) \nonumber\\
% ------------------------------------------------------------------------------- %
\left( \psi(\beta_k) - \psi(\alpha_k + \beta_k) \right) & = & \frac{1}{\sum_{i=1}^n t_{i, k}^m} . \sum_{i=1}^n t_{i,k}^{m} ln \left( 1-x_i \right) \nonumber
% ------------------------------------------------------------------------------- %
\end{array}
\right.
\]

We now have a set non linear system of two equations and two unknown parameters. Because the digamma function is continuous over $\left[ 0,1 \right]$, we suggest using the the Newton method \cite{Dennis1996}. 

\paragraph*{}
Further, we have to be verify there estimators provide a maximum of likelihood and not a saddle point or a minimum Let us look a the second partial derivative, which is well defined : 

\[
\left\{
\begin{array}{r c l}
% ------------------------------------------------------------------------------- %
\frac{{\partial}^2 L}{\partial{\alpha_k}^2} & = & - \left( \sum_{i=1}^N t_{i,k} \right) \frac{{\partial}^2 \log B(\alpha_k, \beta_k)}{\partial {\alpha_k}^2} \nonumber \\
% ------------------------------------------------------------------------------- %
\frac{{\partial}^2 L}{\partial{\beta_k}^2} & = & - \left( \sum_{i=1}^N t_{i,k} \right) \frac{{\partial}^2 \log B(\alpha_k, \beta_k)}{\partial {\beta_k}^2}  \nonumber \\ \nonumber
% ------------------------------------------------------------------------------- %
\end{array}
\right.
\]

Using the previous relationship and noting $\psi_1$ the trigamma function, we have :

\[
\left\{
\begin{array}{r c l}
% ------------------------------------------------------------------------------- %
\frac{{\partial}^2L}{\partial{\alpha_k}^2} & = & - \left( \sum_{i=1}^N t_{i,k} \right) \left[ \psi_1(\alpha_k) - \psi_1(\alpha_k + \beta_k) \right] < 0 \\
% ------------------------------------------------------------------------------- %
\frac{{\partial}^2L}{\partial{\beta_k}^2} & = & - \left( \sum_{i=1}^N t_{i,k} \right) \left[ \psi_1(\beta_k) - \psi_1(\alpha_k + \beta_k) \right] < 0  \\
% ------------------------------------------------------------------------------- %
\end{array}
\right.
\]

Indeed, by the decreasing of the trigamma function, we have $\psi_1(\alpha) - \psi_1(\alpha + \beta) > 0$.

	\subsubsection{Extension to multidimensionality}

As for the Gaussian distribution, it is easy to extend theses results to the multidimensional beta distribution, if we assume that each dimension is independent :

\begin{equation}
L = \sum_{i=1}^{n} \sum_{k=1}^{K} \left( t^m_{ik}.\ln \left[ p_k . \prod_{j=1}^{d}\frac{x_i^{\alpha_k^j-1}(1-{x_i^j}^{\beta_k^j-1})}{B(\alpha_k^j,\beta_k^j)}\right] \right)
\end{equation}

\begin{equation}
L = \sum_{i=1}^{n} \sum_{k=1}^{K} t^m_{ik}.[\ln{p_k}+\sum_{j=1}^{d}+(\alpha_k^j-1)\ln{(x_i^j)} + (\beta_k^j-1)\ln{1-x_i^j}-ln{(B(\alpha_k^j,\beta_k^j)}]
\end{equation}

From the same reasoning follows :

\[
\left \{
\begin{array}{r c l}
% ------------------------------------------------------------------------------- %
\left( \psi(\alpha_k^j) - \psi(\alpha_k^j + \beta_k^j) \right) & = & \frac{1}{\sum_{i=1}^n t_{i, k}^m} . \sum_{i=1}^n t_{i,k}^{m} ln \left( x_i^j \right) \nonumber\\
% ------------------------------------------------------------------------------- %
\nonumber \\
% ------------------------------------------------------------------------------- %
\left( \psi(\beta_k^j) - \psi(\alpha_k^j + \beta_k^j) \right) & = & \frac{1}{\sum_{i=1}^n t_{i, k}^m} . \sum_{i=1}^n t_{i,k}^{m} ln \left( 1-x_i^j \right) \nonumber
% ------------------------------------------------------------------------------- %
\end{array}
\right.{, \forall k \in \left[1, K \right] , \forall j \in \left[1, d \right]}
\]




\subsection{Gamma mixture}

The gamma distribution is given by two parameters : the shape $k$ and  a scale $\beta$ : 
\begin{center}
$g \left( x, \alpha, \beta \right) = \frac{x^{\alpha-1} e^{-\frac{x}{\beta}}}{\Gamma(\alpha) \left( \beta \right)^{\alpha}}$
\end{center}


\subsubsection{M step of the univariate model}

\subsubsection{M step of the multivariate model}

\pagebreak

\section{Results}

\subsection{Gaussian mixture}

\paragraph*{}
The first results are available figure~(\ref{fig_gaussMix1}) page~\pageref{fig_gaussMix1}. 


\begin{figure}
	\center
	\includegraphics{img/2DSimple.png}
	\caption{\label{fig_gaussMix1} classification for simple gaussian distribution}
	\label{Référence}
\end{figure}


\paragraph*{}
The second problematic mentioned in this section lies in the representation of results for multi-dimensional distribution. To that, we have adopted the vision broached by [??] and used in the stk++ library. It consists on using a square matrix whose size is the dimension, where: 
\begin{itemize}
	\item on the $k^{th}$ diagonal $k$ we represent the posterior and estimated distribution for dimension $k$.
	\item on the case $i, j$ we plot dimension $i$ versus dimension $j$, using the estimated classes.
\end{itemize}

An example is available figure~\ref{fig_gaussMix2} page~\pageref{fig_gaussMix2}. \\

\begin{figure}
	\center
	\includegraphics[scale=0.5]{img/Gmix3D.png}
	\caption{\label{fig_gaussMix2} Results in 3 dimensions for a Gaussian distribution.}
	\label{Référence}
\end{figure}

Nevertheless, this representation can not allow us tu fully describe a distribution where dimensions are not independent . Hopefully, unless we mentioned it specifically, it will not be the case in this work.

\paragraph*{}
Finally, let's see the results on the Iris datasets, which can be seen as a five dimensions and three classes distribution. Results and confusion matrix are available figure [] and figure~\ref{fig_gaussMix3} page~\pageref{fig_gaussMix3}.
\begin{figure}
	\center
	\includegraphics[scale=1.5]{img/confusMatGIris.png}
	\caption{\label{fig_gaussMix3} Confusion matrix for the Iris dataset.}
	\label{Référence}
\end{figure}

\clearpage


\subsection{Beta mixture}

\paragraph*{}
We are using the same scheme : first with relatively simple dataset figure~\ref{fig_betaMix1} page~\pageref{fig_betaMix1}, then a real one (Iris).

\begin{figure}
	\center
	\includegraphics[scale=0.5]{img/bMix3D.png}
	\caption{\label{fig_betaMix1} results of estimations of a two components three dimensional beta mixture.}
	\label{Référence}
\end{figure}




\pagebreak

\section{La méthode de Brent}

d'après d'après Brent, R. P. (1973) \cite{Brent1973}.
\paragraph*{}
La méthode de Brent est un algorithme de recherche des zéros d'une fonctions.  Elle est issue de l'algorithme de Théoduros Dekker (1969), auquel Richard Brent a apporté une légère modification en 1973.

\paragraph*{}
Dans cette section, on cherchera à résoudre l'équation suivante, avec la précision $\epsilon$
\begin{eqnarray}
f(x) = 0 \nonumber
\end{eqnarray}
où $f$ est supposée être continue sur $\left[ a,b \right]$ tel que $f(a) f(b) <0$. Par hypothèse de continuité, on sait d'après le théorème des valeurs intermédiaires que la solution de cette équation existe.Si de plus $f$ est monotone sur cet intervalle, alors cette solution est unique. On fera cette hypothèse simplificatrice dans ce paragraphe.

\subsection{La méthode de Dekker}

\paragraph*{}
L'idée de l'algorithme est d'utiliser successivement les méthodes de la sécante et de dichotomie, suivant leur efficacité.

\paragraph*{}
A chaque itération, on dispose de trois points :
\begin{itemize}
	\item $b_n$ la solution courante
	\item $a_n$ le point tel que $f(a_n) f(b_n) <0$ et $|f(b_n)| < |f(a_n)|$
	\item $b_{n-1}$ la solution de l'itération précédente. Par convention $b_{-1} = a_{0}$ même si la deuxième condition n'est pas vérifiée.
\end{itemize}

\paragraph*{}
On calcule ensuite les deux candidats à la solution de l'itération suivante : \\

\begin{minipage}[c]{0.50\linewidth}
\begin{center}
$s = b_n - \frac{b_n-b_{n-1}}{f(b_n)-f(b_{n-1})} f(b_n)$ \\ \textbf{} \\
par la méthode de la sécante
\end{center}
\end{minipage} \hfill
\begin{minipage}[c]{0.50\linewidth}
\begin{center}
$\frac{a_n + b_n}{2}$ \\ \textbf{} \\
par dichotomie
\end{center}
\end{minipage}

\paragraph*{}
Si le résultat de la méthode de la sécante, $s$, tombe entre $b_n$ et $m$, alors il devient la nouvelle solution courante. Dans le cas contraire, on choisit le point issus de la dichotomie.

\paragraph*{}
Pour la mise à jour du nouveau contrepoint, on procède de la manière suivante :
\begin{itemize}
	\item Si $f(b_{n+1})$ et $f(a_n)$ sont de signe opposé, alors $a_{n+1} = a_n$
	\item Sinon $a_{n+1} = b_n$ 
\end{itemize}

\paragraph*{}
Ensuite, si $|f(a_{n+1})| < |f(b_{n+1})|$, alors $a_{n+1}$ est une meilleure approximation de la solution que $b_{n+1}$.On échange donc leur valeur.

\paragraph*{}
On répète enfin cette opération jusqu'à la précision souhaitée. On est assurée de la convergence puisque la suite des $b_n$ est encadrée par une suite construite à partir des résultats de la méthode de dichotomie, qui elle est convergente.

\subsection{Les modifications de Brent}

Il peut arriver que la méthode de Dekker converge particulièrement lentement, et en particulier nécessiter plus d'itération que la méthode de dichotomie. Brent propose alors une seconde condition à vérifier pour pour choisir $s$ lors de la nouvelle itération :
\begin{itemize}
	\item Si l'étape précédente utilisait la dichotomie alors on doit avoir $|s-b_n| < \frac{1}{2} |b_n-b_{n-1}|$
	\item Si l'étape précédente utilisait la méthode de la sécante, alors on doit avoir $|s-b_n| < \frac{1}{2} |b_{n-1}-b_{n-2}|$
\end{itemize}

\paragraph*{}
On doit alors retenir le résultats des trois itérations précédentes, mais on voit ainsi dans la deuxième proposition que la méthode de la sécante n'est plus retenue lorsqu'elle ne revient qu'à un rallongement de la dichotomie.\\
Brent a prouvé que dans ce cas, on converge au plus en $N^2$ itérations, où $N$ est le nombre d'itération nécessaire à la dichotomie.

\paragraph*{}
Prent propose également d'utiliser l'interpolation quadratique inverse plutôt que l'interpolation linéaire dans la méthode de la sécante si $f(a_n), f(b_n), f(b_{n-1)}$ sont distincts.

\paragraph*{}
Pour rappel, l'interpolation quadratique inverse consiste à approcher non plus la fonction par sa tangente, mais par une fonction quadratique par l'interpolation lagrangienne. Elle nécessite donc trois points distincts, et est définie ici par :
\begin{eqnarray}
b_{n+1} = \frac{f(a_{n})f(b_n)}{(f(b_{n-1})-f(a_{n}))(f(b_{n-1}-f(b_n))} b_{n-1} + \frac{f(b_{n-1})f(b_n)}{(f(a_{n})-f(b_{n-1}))(f(a_{n})-f(b_n))} a_{n} \nonumber
\end{eqnarray}

\paragraph*{}
De plus, dans le cas où l'interpolation quadratique inverse a été retenue à l'itération précédente, l'expression de $b_n$ est différente de celle issue de la l'interpolation linéaire. L'expression de la nouvelle condition change elle aussi :

\begin{itemize}
	\item Si l'étape précédente utilisait la dichotomie alors on doit avoir $|s-b_n| < \frac{1}{2} |b_n-b_{n-1}|$
	\item Si l'étape précédente utilisait la méthode de la sécante, alors on doit avoir $|s-b_n| < \frac{1}{4} |3a_{n}-b_{n}|$
\end{itemize}

\paragraph*{}
En effet, Il suffit de traduire le fait que

\subsection{Résolution d'un système d'équation}
On va maintenant chercher à résoudre le système non linéaire suivant :
\[
\left \{
\begin{array}{r c l}
% ------------------------------------------------------------------------------- %
f_1(x,y) = 0 \\
f_2(x, y) = 0
% ------------------------------------------------------------------------------- %
\end{array}
\right.{, \forall k \in \left[1, K \right] , \forall j \in \left[1, d \right]}
\]

L'algorithme proposé ici n'est pas propre à la méthode de Brent. Il consiste à maximiser le système successivement suivant chacune des variables :

\begin{algorithm}
\caption{calculer $x$ et $y$ avec la précision $\epsilon$}
\begin{algorithmic} 
\REQUIRE $\epsilon, x, y$
%\ENSURE $\psi(\alpha_k)- \psi}(\hat{\alpha_k) < \epsilon$
\STATE $x \leftarrow x_0$
\STATE $y \leftarrow y_0$
\WHILE{$|f_1(x, y)| > \epsilon$ and $|f_2(x, y)| > \epsilon$}
\STATE fixer $y$ et résoudre la première équation avec la méthode de Brent
\STATE fixer $x$ et résoudre la seconde équation avec la méthode de Brent
\ENDWHILE
\RETURN $x, y$
\end{algorithmic}
\end{algorithm}

\subsection{Implémentation numérique}

Plusieurs implémentation de la méthode de Brent existent déjà. Nous avons avons utilisé dans ce mémoire :
\begin{itemize}
	\item La fonction $fsolve$ de la toolBox optimisation de Matlab,
	\item La fonction $optimize$ sur R
	\item La fonction $?$ de la bibliothèque stk++.
\end{itemize}

\bibliographystyle{unsrt}
\bibliography{../memoirebib}

\end{document}