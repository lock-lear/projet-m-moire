verifyShape_gaussien <- function(X, N, D, K, l) {
  
  if(!is.matrix(l$mu)) {
    print("Please provide a D*K matrix for mu_hat")
    return(FALSE)
  }
  else if (dim(l$mu)[1] != D || dim(l$mu)[2] != K) {
    print("Please provide a D*K matrix for mu_hat")
    return(FALSE)
  }
  
  
  
  if(!is.array(l$sigma)) {
    print("Please provide a D*D*K array for sigma_hat")
    return(FALSE)
  }
  else if (dim(l$sigma)[1] != D || dim(l$sigma)[2] != D || dim(l$sigma)[3] != K) {
    print("Please provide a D*D*K matrix for sigma_hat")
    return(FALSE)
  }
  
  
  
  if(!is.vector(l$pi) || length(l$pi) != K) {
    print("Please provide a K vector for p_hat")
    return(FALSE)
  }
  
  
  return(TRUE)
};