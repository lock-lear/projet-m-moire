#include <Rcpp.h>
using namespace Rcpp;

// Below is a simple example of exporting a C++ function to R. You can
// source this function into an R session using the Rcpp::sourceCpp 
// function (or via the Source button on the editor toolbar)

// For more on using Rcpp click the Help button on the editor toolbar


// [[Rcpp::export]]
NumericMatrix cS_step(NumericMatrix t, NumericMatrix s) {

  int N = t.nrow();
  int K = t.ncol();
  NumericMatrix sumt(N, 1);
  NumericMatrix result(N, K);
  
  for (int n=0; n<N; n++) {
    for (int k=0; k < K; k++) {
      if ( s(n, 0) > sumt(n, 0) && s(n, 0) <= sumt(n, 0) + t(n, k) )
        result(n, k) = 1;
    
      sumt(n, 0) = sumt(n, 0) + t(n, k);
    }
  }
  
  return result;
}


// [[Rcpp::export]]
List cppMstepGauss(NumericMatrix X, NumericMatrix t) {
   
   int N = t.nrow();
   int K = t.ncol();
   int D = X.ncol();
   
   NumericMatrix mu(D, K);
    // En attendant de trouver comment faire des array en C++, une colone = 1 diagonale
   NumericMatrix sigma(D, K); 
   NumericMatrix sumt(1, K);
   
   // Calcul de la moyenne N*D*(K)
   for(int i=0; i<N; i++) {
     for (int d=0; d<D; d++) {
      mu(d, _) = mu(d, _) + t(i, _) * X(i, d);
      }
      sumt(0, _) = sumt(0, _) + t(i, _);
   }
    for (int d=0; d<D; d++) {
      mu(d, _) = mu(d, _) / sumt(0, _);
    }
   
   // Calcul de la variance N*D*(K) again
   for (int d=0; d<D; d++) {
    for (int i=0; i<N; i++) {  
       sigma(d, _) = sigma(d, _) + t(i, _) * pow(X(i, d) - mu(d, _), 2);
     }
     
     sigma(d, _) = sqrt(sigma(d, _) / sumt(0, _));
   }
   
   
   return List::create( Named("mu")=mu,
                        Named("sigma")=sigma,
                        Named("pi")=sumt);
}


// [[Rcpp::export]]
NumericMatrix cppGMixture_pdf(NumericMatrix X, NumericMatrix mu, NumericMatrix sigma, const NumericVector pi) {
   // Trick : sigma doit être une matrice où les colonnnes sont les diagonales
   
   int N = X.nrow();
   int K = sigma.ncol();
   int D = X.ncol();
   
   NumericMatrix result(N, 1);
   NumericMatrix buf(1, K);
   
   for (int i=0; i<N; i++) {
     // D'abord on initialise à 1
     for (int k=0; k<K; k++) {
       buf(0, k) = 1;
     }
     
     // Ensuite on calcul pour composante du mélange
     for(int d=0; d<D; d++) {
        buf(0, _) = buf(0, _) * exp( - pow((X(i, d)-mu(d, _)), 2) / (2 * pow(sigma(d, _), 2)) ) / (sigma(d, _) * sqrt(2*3.14159265359));
     }
     
     // On somme le tout
     for (int k=0; k<K; k++) {
       result(i, 0) = result(i, 0) + pi[k] * buf(0, k);
     }
   }
   
   return result;
}



// [[Rcpp::export]]
NumericMatrix cppGEM_t(NumericMatrix X, NumericMatrix mu, NumericMatrix sigma, const NumericVector pi) {
   // Trick : sigma doit être une matrice où les colonnnes sont les diagonales
   
   int N = X.nrow();
   int K = sigma.ncol();
   int D = X.ncol();
   
   NumericMatrix result(N, K);
   
   for (int i=0; i<N; i++) {
     // D'abord on initialise à 1
     for (int k=0; k<K; k++)
      result(i, k) = 1;
     
     // Ensuite on calcul pour composante du mélange
     for(int d=0; d<D; d++) {
        result(i, _) = result(i, _) * exp( - pow((X(i, d)-mu(d, _)), 2) / (2 * pow(sigma(d, _), 2)) ) / (sigma(d, _) * sqrt(2*3.14159265359));
     }
     
   }
   
   return result;
}