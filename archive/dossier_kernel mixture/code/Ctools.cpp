#include <Rcpp.h>
using namespace Rcpp;

// Below is a simple example of exporting a C++ function to R. You can
// source this function into an R session using the Rcpp::sourceCpp 
// function (or via the Source button on the editor toolbar)

// For more on using Rcpp click the Help button on the editor toolbar

// [[Rcpp::export]]
double ckGauss(const NumericVector x, const NumericVector y, double sigma) {
  if (x.size() != y.size())
    return -1;
  
  double res = 0;
  
  for (int i=0; i < x.size(); i++) {
    res += pow(x[i] - y[i], 2);
  }
  
  return exp(-res / sigma);
}

double cLinear(const NumericVector x, const NumericVector y, double sigma) {
  if (x.size() != y.size())
    return -1;
  
  return y[0];
}


// [[Rcpp::export]]
NumericMatrix cComputeY(NumericMatrix X, NumericMatrix t, double sigma) {
  // Pas de vérification sur la configuration des matrices pour l'instant

  int N = t.nrow();
  int K = t.ncol();
  
  // Calcul de la matrice de noyau
  NumericMatrix kmatrix(N, N);
  for (int i=0; i < N; i++) {
    for (int j=0; j < N; j++) {
      if (i <= j)
        kmatrix(i, j) = ckGauss(X(i, _), X(j, _), sigma);
      else
        kmatrix(i, j) = kmatrix(j, i);
    }
  }
 
  // Calcul de la somme sur les lignes de t
  NumericMatrix sumt(1, K);
  for (int i=0; i < N; i++) {
    sumt(0, _) = sumt(0 ,_) + t(i, _);
  }
  
  // Début du calcul de Y
  NumericMatrix Y(N, K);
  NumericMatrix buf_simpleSum(N, K);
  
  for (int i=0; i < N; i++) {
    // Ajout du terme constant
    for (int k=0; k<K; k++)
      Y(i, k) = kmatrix(i, i);
    
    // Calcul puis Ajout du terme somme simple
    for (int j=0; j < N; j++) {
      buf_simpleSum(i, _) = buf_simpleSum(i, _) + t(j, _) * kmatrix(i, j);
    }
    Y(i, _) = Y(i, _) - 2 * buf_simpleSum(i, _) / sumt(0, _);
  }
  
  // Calcul puis Ajout du terme somme double
  NumericMatrix buf_doubleSum(1, K);
  for (int j=0; j < N; j++) {
    buf_doubleSum(0, _) = buf_doubleSum(0, _) + t(j, _) * buf_simpleSum(j, _);
  }
  for (int i=0; i < N; i++) {
    Y(i, _) = Y(i, _) + buf_doubleSum(0, _) / (pow(sumt(0, _), 2));
  }
  
   return Y;
}



// [[Rcpp::export]]
NumericMatrix cComputeYprot(NumericMatrix D, NumericMatrix t, double sigma) {
  // Pas de vérification sur la configuration des matrices pour l'instant

  int N = t.nrow();
  int K = t.ncol();
  
  // Calcul de la matrice de noyau
  NumericMatrix kmatrix(N, N);
  for (int i=0; i < N; i++) {
    for (int j=0; j < N; j++) {
      if (i <= j)
        kmatrix(i, j) = exp(- pow(D(i, j) / sigma, 2) );
      else
        kmatrix(i, j) = kmatrix(j, i);
    }
  }
 
  // Calcul de la somme sur les lignes de t
  NumericMatrix sumt(1, K);
  for (int i=0; i < N; i++) {
    sumt(0, _) = sumt(0 ,_) + t(i, _);
  }
  
  // Début du calcul de Y
  NumericMatrix Y(N, K);
  NumericMatrix buf_simpleSum(N, K);
  
  for (int i=0; i < N; i++) {
    // Ajout du terme constant
    for (int k=0; k<K; k++)
      Y(i, k) = kmatrix(i, i);
    
    // Calcul puis Ajout du terme somme simple
    for (int j=0; j < N; j++) {
      buf_simpleSum(i, _) = buf_simpleSum(i, _) + t(j, _) * kmatrix(i, j);
    }
    Y(i, _) = Y(i, _) - 2 * buf_simpleSum(i, _) / sumt(0, _);
  }
  
  // Calcul puis Ajout du terme somme double
  NumericMatrix buf_doubleSum(1, K);
  for (int j=0; j < N; j++) {
    buf_doubleSum(0, _) = buf_doubleSum(0, _) + t(j, _) * buf_simpleSum(j, _);
  }
  for (int i=0; i < N; i++) {
    Y(i, _) = Y(i, _) + buf_doubleSum(0, _) / (pow(sumt(0, _), 2));
  }
  
   return Y;
}